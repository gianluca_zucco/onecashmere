env SERVER_NAME;
env S3_HOST;

user root;
worker_processes 4;
daemon off;
error_log /dev/stdout info;
pid /var/run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    perl_modules perl/lib;
    perl_require validator.pm;

    perl_set $server_name_from_env 'sub { return $ENV{"SERVER_NAME"}; }';
    perl_set $s3_host_from_env 'sub { return $ENV{"S3_HOST"}; }';

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    #access_log off;
    access_log /dev/stdout;
    server_tokens off;

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;

    keepalive_timeout 65;

    gzip on;
    gzip_http_version 1.0;
    gzip_comp_level 2;
    gzip_proxied any;
    gzip_vary off;
    #gzip_buffers
    gzip_types text/plain text/css application/x-javascript text/xml application/xml application/rss+xml application/atom+xml text/javascript application/javascript application/json text/mathml;
    gzip_min_length  1000;
    gzip_disable     MSIE [1-6]\.;

    server_names_hash_bucket_size 64;
    types_hash_max_size 2048;
    types_hash_bucket_size 64;
    #proxy_read_timeout
    #client_body_buffer_size
    #client_max_body_size

    limit_req_zone $binary_remote_addr zone=default:10m rate=1r/s;

    proxy_cache_path /var/www/nginx/cache levels=1:2 keys_zone=cache-space:4m max_size=50m inactive=120m;
    proxy_temp_path /var/www/nginx/tmp;

    server {
      listen 80;
      server_name $server_name_from_env;

      location ~ ^/(.+)$ {
        proxy_pass http://localhost:8080;
        proxy_cache cache-space;
        proxy_cache_valid 200 60m;
      }
    }

    server {
      listen 8080;
      server_name $server_name_from_env;
      root /var/www/html/htdocs;
      resolver 8.8.8.8;
      small_light on;
      location @empty {
        empty_gif;
      }

      # Image processing for images in local file
      location ~ ^/small_light[^/]*/(.+)$ {
        set $small_light_maximum_size 3072;
        perl validator::handler;
      }
    }
}
