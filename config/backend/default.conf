server {
    listen       80;
    server_name  dev.onecashmere.com;
    root /var/www/html/htdocs;
    index index.php;

    gzip  on;
    gzip_vary on;
    gzip_comp_level 6;
    gzip_proxied any;
    gzip_types text/html text/css application/json application/javascript application/x-javascript text/javascript text/xml application/xml application/rss+xml application/atom+xml application/rdf+xml image/svg+xml;

    # make sure gzip does not lose large gzipped js or css files
    # see http://blog.leetsoft.com/2007/07/25/nginx-gzip-ssl.html
    gzip_buffers 16 8k;

    # Disable gzip for certain browsers.
    gzip_disable “MSIE [1-6].(?!.*SV1)”;

    location ~* \.(png|jpg|jpeg|gif)$ {
        expires modified 365d;
        add_header Pragma public;
        add_header Cache-Control "public";
        proxy_read_timeout 90;
        proxy_connect_timeout 90;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_pass http://imageserver:80;
    }

    location ~* \.(ico|asf|asx|wax|wmv|wmx|avi|bmp|class|divx|doc|docx|eot|exe|gz|gzip|ico|mdb|mid|midi|mov|qt|mp3|m4a|mp4|m4v|mpeg|mpg|mpe|mpp|odb|odc|odf|odg|odp|ods|odt|ogg|ogv|otf|pdf|pot|pps|ppt|pptx|ra|ram|svg|svgz|swf|tar|t?gz|tif|tiff|ttf|wav|webm|wma|woff|wri|xla|xls|xlsx|xlt|xlw|zip)$ {
        access_log off;
        log_not_found off;
        expires modified 365d;
        add_header Pragma public;
        add_header Cache-Control "public";
    }

    location ~* (.+)\.(\d+)\.(js|css)$ {
        try_files $uri $1.$3;
        expires max;
        add_header Pragma public;
        add_header Cache-Control "public";
    }

    location ~* \.(js|css)$ {
        expires modified 365d;
        add_header Pragma public;
        add_header Cache-Control "public";
    }

    location / {
        try_files /maintenance.html $uri $uri/ @handler; ## If missing pass the URI to Magento's front handler
        expires 30d; ## Assume all files are cachable
    }

    location @handler { ## Magento uses a common front handler
        rewrite / /index.php;
    }
    
    location ~ .php/ { ## Forward paths like /js/index.php/x.js to relevant handler
        rewrite ^(.*.php)/ $1 last;
    }

    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_pass php:9000;
        fastcgi_index  index.php;
        fastcgi_param  SERVER_PORT 80;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_param HTTPS on;
        fastcgi_param MAGE_IS_DEVELOPER_MODE on;
    }
}
