<?php

namespace Deployer;

task('nginx:reload', function ()   {
    within('{{release_path}}', function ()  {
        $container = "{{application}}{{stage}}_backend_1";
        $config = json_decode(run("docker inspect $container"));
        $ipAddress = current(current($config)->{'NetworkSettings'}->{'Networks'})->{'IPAddress'};
        if ($ipAddress) {
            run("sh -c \"cat config/frontend/{{stage}}.conf | sed -e 's/backend:80/$ipAddress:80/g' > /etc/nginx/conf.d/{{stage}}.conf\"", ['tty' => true]);
            run("sudo /etc/init.d/nginx reload", ['tty' => true]);
        } else {
            writeln("<error>$container has no IP Address</error>");
        }
    });
});
