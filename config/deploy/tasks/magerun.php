<?php

namespace Deployer;

task('index:reindex', function ()   {
    run("{{bin/magerun}} index:reindex:all", ['tty' => true]);
});

task('cache:flush', function ()   {
    run("{{bin/magerun}} cache:flush", ['tty' => true]);
});

task('maintenance:on', function ()   {
    run("{{bin/magerun}} sys:maintenance --on", ['tty' => true]);
});

task('maintenance:off', function ()   {
    run("{{bin/magerun}} sys:maintenance --off", ['tty' => true]);
});
