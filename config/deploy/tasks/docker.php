<?php

namespace Deployer;

task('docker:up', function ()   {
    within('{{release_path}}', function ()  {
        run("{{docker}} -f docker-compose-{{stage}}.yml down -v --remove-orphans", ['tty' => true]);
        run("{{docker}} -f docker-compose-{{stage}}.yml up -d", ['tty' => true]);
    });
});

task('docker:down', function ()   {
    within('{{release_path}}', function ()  {
        run("{{docker}} -f docker-compose-{{stage}}.yml down -v --remove-orphans", ['tty' => true]);
    });
});

task('docker:build', function ()   {
    within('{{release_path}}', function ()  {
        run("{{docker}} -f docker-compose-{{stage}}.yml up -d --build", ['tty' => true]);
    });
});

task('php:restart', function ()   {
    within('{{release_path}}', function ()  {
        run("{{docker}} -f docker-compose-{{stage}}.yml restart php", ['tty' => true]);
    });
});
