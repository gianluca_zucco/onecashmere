<?php

namespace Deployer;

task('crontab:install', function () {
    within('{{release_path}}', function () {
        $crontabTemplate = parse("{{release_path}}/config/crontab/{{stage}}");
        $tmp = "$crontabTemplate.tmp";
        run('echo "' . parse(run("cat $crontabTemplate")) . "\" > $tmp");
        run("crontab -u {{http_user}} $tmp", ['tty' => true]);
        run("rm $tmp", ['tty' => true]);
    });
})->onStage(['production', 'test']);
