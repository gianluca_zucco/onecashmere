let gulp = require('gulp'),
    less = require('gulp-less'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    merge = require('ordered-merge-stream'),
    stripJsComment = require('gulp-strip-comments'),
    stripCssComment = require('gulp-strip-css-comments'),
    imagemin = require('gulp-imagemin'),
    clean = require('gulp-clean');

let modules = 'node_modules';
let skin = 'htdocs/skin/frontend/onecashmere/default';
let baseSkin = 'htdocs/skin/frontend/base/default';
let parentSkin = 'htdocs/skin/frontend/magma/default';

// CSS
let cssFiles = [
    `${modules}/font-awesome/css/font-awesome.css`,
    `${modules}/normalize.css/normalize.css`,
    `${modules}/boxicons/css/boxicons.css`,
    `${baseSkin}/css/widgets.css`,
    `${parentSkin}/css/magestore/productfile.css`,
    `${parentSkin}/css/cookienotice.css`,
    `${baseSkin}/css/smile/elasticsearch.css`
];

let lessFiles = [`${skin}/src/application.less`];

// JS
let libJs = 'htdocs/js';
let coreJs = [
    `${libJs}/prototype/prototype.js`,
    `${libJs}/lib/ccard.js`,
    `${libJs}/prototype/validation.js`,
    `${libJs}/scriptaculous/builder.js`,
    `${libJs}/scriptaculous/effects.js`,
    `${libJs}/scriptaculous/dragdrop.js`,
    `${libJs}/scriptaculous/controls.js`,
    `${libJs}/scriptaculous/slider.js`,
    `${libJs}/varien/js.js`,
    `${libJs}/varien/form.js`,
    `${libJs}/mage/translate.js`,
    `${libJs}/mage/cookies.js`
];
let libScripts = [
    `${modules}/jquery/dist/jquery.js`,
    `${modules}/jquery-noconflict/index.js`
];
let themeJs = [
    `${modules}/jquery-ui/ui/widget.js`,
    `${modules}/knockout/build/output/knockout-latest.js`,
    `${modules}/slick-carousel/slick/slick.js`,
    `${modules}/popper.js/dist/umd/popper.js`,
    `${modules}/bootstrap/dist/js/bootstrap.js`,
    `${modules}/waypoints/lib/jquery.waypoints.js`,
    `${modules}/lazysizes/lazysizes.js`,
    `${libJs}/lazysizes/plugin/attrchange.js`,
    `${libJs}/bootstrap/fix.js`,
    `${libJs}/w3themes/backtotop.js`,
    `${libJs}/ebizmarts/mailchimp/campaignCatcher.js`,
    `${parentSkin}/js/lib.js`,
    `${libJs}/smile/elasticsearch/search.js`,
    `${libJs}/onecashmere/{waypoint,optinMonster,slick,layeredNav,navigation,facebookPixel,library}.js`
];

let libJsFiles = coreJs.concat(libScripts);

let productPageScript = [
    `${libJs}/varien/product.js`,
    `${libJs}/varien/product_options.js`,
    `${libJs}/varien/configurable.js`,
    `${libJs}/onecashmere/jquery.zoom.js`,
    `${libJs}/onecashmere/product/view/*.js`
];

gulp.task('css', function () {
    return merge([gulp.src(cssFiles), gulp.src(lessFiles).pipe(less())])
        .pipe(concat('application.css'))
        .pipe(stripCssComment())
        .pipe(gulp.dest(`${skin}/css`))
        .pipe(minifycss())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(`${skin}/css`));
});

gulp.task('lib_js', function () {
    return gulp.src(libJsFiles)
        .pipe(concat('lib.js'))
        .pipe(stripJsComment())
        .pipe(gulp.dest(`${libJs}`))
        .pipe(uglify({mangle: {reserved: ['$super']}}))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(`${libJs}`));
});

gulp.task('app_js', function () {
    return gulp.src(themeJs)
        .pipe(concat('application.js'))
        .pipe(stripJsComment())
        .pipe(gulp.dest(`${libJs}`))
        .pipe(uglify({mangle: {reserved: ['$super']}}))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(`${libJs}`));
});

gulp.task('product_js', function () {
    return gulp.src(productPageScript)
        .pipe(concat('product.js'))
        .pipe(stripJsComment())
        .pipe(gulp.dest(`${libJs}`))
        .pipe(uglify({mangle: {reserved: ['$super']}}))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(`${libJs}`));
});

gulp.task('js', gulp.parallel(['lib_js', 'app_js', 'product_js']));

// Images
gulp.task('clean_images', function() {
    return gulp.src(`${skin}/images/*`).pipe(clean({
        read: false,
        force: true
    }));
});

gulp.task('process_images', function () {
    return gulp
        .src([
            `${skin}/src/images/**/*`,
            `${modules}/payment-icons/min/flat/{visa,mastercard,amex,paypal}.svg`,
            `${modules}/simple-icons/icons/{facebook,twitter,pinterest}.svg`,
            `${modules}/flag-svg-collection/flags/4x3/{it,gb}.svg`,
            `${modules}/boxicons/svg/regular/{bx-menu,bx-x,bx-chevron-left,bx-chevron-right}.svg`
        ])
        .pipe(imagemin([], {
            verbose: false
        }))
        .pipe(gulp.dest(`${skin}/images`));
});

gulp.task('images', gulp.series(['clean_images', 'process_images']));

// Fonts
gulp.task('fonts', function () {
    let modules = 'node_modules';
    let fonts = [
        `${modules}/font-awesome/fonts/**/*`,
        `${modules}/boxicons/fonts/*`,
        `${modules}/slick-carousel/slick/fonts/*`,
        `${modules}/typeface-yanone-kaffeesatz/files/*`,
    ];
    return gulp.src(fonts).pipe(gulp.dest(`${skin}/fonts`));
});

// Default task
gulp.task('default', gulp.parallel(['css', 'js', 'images', 'fonts']));
gulp.task('watch', function() {
    gulp.watch(cssFiles.concat([`${skin}/src/*.less`, `${skin}/src/*/*.less`, `${skin}/src/*/*/*.less`]), gulp.series(['css']));
    gulp.watch(libJsFiles.concat(themeJs).concat(productPageScript), gulp.series(['js']));
});
