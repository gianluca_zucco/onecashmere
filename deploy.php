<?php
namespace Deployer;
require_once 'recipe/common.php';

foreach (glob(__DIR__ . '/config/deploy/tasks/*.php') as $task) {
    require_once $task;
}

inventory('config/deploy/config.yml');

after('deploy:symlink', 'php:restart');
after('php:restart', 'cache:flush');
after('deploy:writable', 'docker:up');
after('docker:up', 'nginx:reload');

desc('Deploy your project');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
