namespace :deploy do
    after :finished, 'app:cron:deploy'
end

namespace :app do
    namespace :cache do
        desc 'Flush caches'
          task :flush do
            on roles(:web) do
                execute "docker exec -u www-data #{fetch(:application)}#{fetch(:stage)}_php_1 magerun cache:flush";
            end
        end
        desc 'Clean caches'
          task :clean do
            on roles(:web) do
                execute "docker exec -u www-data #{fetch(:application)}#{fetch(:stage)}_php_1 magerun cache:clean";
            end
        end
    end
    namespace :reindex do
        desc 'Reindex'
        task :all do
            on roles(:web) do
                execute "docker exec -u www-data #{fetch(:application)}#{fetch(:stage)}_php_1 magerun index:reindex:all";
            end
        end
    end
    namespace :cron do
        desc 'Deploy crontab'
        task :deploy do
            on roles(:web) do
                within current_path do
                    execute "crontab -u onecashmere #{current_path}/config/#{fetch(:stage)}/cron"
                end
            end
        end
    end
end
