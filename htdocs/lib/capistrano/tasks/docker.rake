namespace :deploy do
    after :updated, 'docker:down'
    after :updated, 'docker:up'
end

namespace :docker do

    desc 'Restart application containers'
    task :restart do
        on roles(:web) do
            within current_path do
                execute "docker-compose -p #{fetch(:application)}#{fetch(:stage)} -f #{release_path}/docker-compose-#{fetch(:stage)}.yml restart";
                invoke "docker:nginx:reload"
            end
        end
    end

    namespace :nginx do
        desc 'Reload server\'s NGINX with new configuration'
        task :reload do
            on roles(:web) do
                within release_path do
                    execute "cat #{release_path}/config/frontend/#{fetch(:stage)}.conf | sed s/{backend}/$(docker inspect --format={{.NetworkSettings.Networks.#{fetch(:application)}#{fetch(:stage)}_default.IPAddress}} #{fetch(:application)}#{fetch(:stage)}_backend_1)/g >> #{release_path}/#{fetch(:stage)}.#{fetch(:application)}.com.conf"
                    execute "sudo mv #{release_path}/#{fetch(:stage)}.#{fetch(:application)}.com.conf /etc/nginx/conf.d/"
                    execute "sudo service nginx reload"
                end
            end
        end
    end

    desc 'Build application'
    task :build do
        on roles(:web) do
            within current_path do
                execute "docker-compose -p #{fetch(:application)}#{fetch(:stage)} -f #{current_path}/docker-compose-#{fetch(:stage)}.yml up -d --build";
            end
        end
    end

    desc 'Stop docker containers'
    task :down do
        on roles(:web) do
            within release_path do
                execute "docker-compose -p #{fetch(:application)}#{fetch(:stage)} -f #{release_path}/docker-compose-#{fetch(:stage)}.yml down -v";
                execute "docker rm -f $(docker ps -a | grep Dead | awk '{print $1}')", raise_on_non_zero_exit: false;
            end
        end
    end

    desc 'Start docker containers'
    task :up do
        on roles(:web) do
            within release_path do
                execute "docker-compose -p #{fetch(:application)}#{fetch(:stage)} -f #{release_path}/docker-compose-#{fetch(:stage)}.yml up -d";
                invoke "docker:nginx:reload"
            end
        end
    end

    desc 'Restart FPM'
    task :fpm_restart do
        on roles(:web) do
            within release_path do
                execute "docker-compose -p #{fetch(:application)}#{fetch(:stage)} -f #{release_path}/docker-compose-#{fetch(:stage)}.yml restart php";
            end
        end
    end
end
