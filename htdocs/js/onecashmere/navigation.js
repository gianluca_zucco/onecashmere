(function($) {
    $('[data-toggle="offcanvas"]').on('click', function () {
        var id = $(this).data('target');
        $('#'+id+'.offcanvas-collapse').toggleClass('open');
        $(this).toggleClass('open');
    });

    var dropdownBehavior = {
        onHover: function($isHover, event) {
            $(event.currentTarget).toggleClass('show', $isHover);
            $(event.currentTarget).find('.dropdown-menu').toggleClass('show', $isHover);
            $(this).find('[data-toggle="dropdown"]').attr('aria-expanded', $isHover ? 'true' : 'false');
        }
    };
    $('[data-toggle="dropdown"]:not([data-hover])').parent().hover(
        dropdownBehavior.onHover.bind($(this), true),
        dropdownBehavior.onHover.bind($(this), false)
    );
    $('a.nav-link[data-toggle="dropdown"]').click(function() {
        var parent = $(this).closest('.offcanvas-collapse.open');
        if (!parent.length) {
            location.href = $(this).attr('href');
        }

    });
})(jQuery);
