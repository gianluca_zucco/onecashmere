(function($) {
    $(document).ready(function() {
        $('#top-navbar').waypoint(function(direction) {
            $('body').toggleClass('is-nav-sticky', direction === 'down');
            $(this.element).toggleClass('is-sticky', direction === 'down');
        });
    });
})(jQuery);
