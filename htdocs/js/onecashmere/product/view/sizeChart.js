(function($) {
    $.widget('ui.sizeChart', {
        _create: function() {
            var slider = this.element.find('.size-media-slider');
            this.element.find('.chart-nav > *').each(function(slideIndex) {
                $(this).attr('data-index', slideIndex);
            });
            slider.slick({
                autoPlay: false,
                arrows: false,
                dots: true,
                slidesToScroll: 1,
                slidesToShow: 1
            });
            this._on({
                'click .chart-nav > *': function(Event) {
                    this.element.find('.size-media-slider').slick('slickGoTo', $(Event.target).data('index'));
                }
            });
        }
    });
    $(document).ready(function() {
        $('[data-widget="sizeChart"]').sizeChart();
    });
})(jQuery);
