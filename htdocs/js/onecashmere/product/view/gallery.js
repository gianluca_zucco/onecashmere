(function($) {
    $.widget('ui.gallery', {
        options: {
            mainContainerId: '',
            targetId: '',
            items: {}
        },
        initZoom: function (url) {
            $(this.options.mainContainerId).trigger('zoom:destroy');
            $(this.options.targetId).empty();
            $(this.options.mainContainerId).zoom({
                target: $(this.options.targetId),
                url: url,
                touch: false,
                on: 'mouseover'
            });
        },
        _create: function() {
            this.items = ko.observableArray(this.options.items);
            if (window.configurableOptions) {
                configurableOptions.gallery.subscribe(function(gallery) {
                    this.items(gallery);
                }, this);
            }
            this.display = ko.observable(this.options.items[0] || {});
            this.display.subscribe(function(object) {
                this.initZoom(object.zoomUrl);
            }, this);
            this.items.subscribe(function(gallery) {
                this.display(gallery[0] || {})
            }, this);
            this.initZoom(this.display().zoomUrl);
            window.gallery = this;
            ko.applyBindings(gallery, this.element[0]);
        }
    });

    $(document).ready(function() {
        $('[data-gallery]').each(function() {
            $(this).gallery($(this).data('config'));
        });
    });
})(jQuery);
