(function($) {
    $.widget('ui.addToCartQty', {
        options: {
            maxQty: 5,
            minQty: 1
        },
        _create: function() {
            this._on({
                'click .minus': this.onClick.bind(this,  -1),
                'click .plus': this.onClick.bind(this,  +1)
            });
        },
        onClick: function(qty) {
            var target = this.element.find('input[name="qty"]');
            var newValue = parseInt(target.val()) + qty;
            newValue = newValue < this.options.minQty ? this.options.minQty :
                (newValue > this.options.maxQty ? this.options.maxQty : newValue);
            target.val(newValue);
        }
    });

    $(document).ready(function() {
        $('[data-widget="addToCartQty"]').addToCartQty();
    });
})(jQuery);
