(function ($) {
    $.widget('ui.configurableOptions', {
        options: {},
        _create: function () {
            $.extend(this, this.options);
            this.allowedProducts = ko.observableArray([]);
            this.selected = ko.observable({});
            this.initCurrentProduct();
            this.gallery = ko.observableArray(this.optionsConfig.config.images[this.optionsConfig.config.productId]);
            this.initAttributes();
            window.configurableOptions = this;
            this._on($(document), {
                'click .btn-cart': this.onAddToCart
            });
            ko.applyBindings(configurableOptions, this.element.parent()[0]);
        },
        initCurrentProduct: function () {
            var productId = this.optionsConfig.config.productId;
            this.product = ko.observable($.extend({
                'productId': productId,
            }, this.optionsConfig.config.prices[productId] || {}));
        },
        initAttributes: function () {
            var _this = this;
            this.attributes = ko.observableArray($.map(this.optionsConfig.settings, function (element) {
                var attribute = this.optionsConfig.config.attributes[element.id.replace('attribute', '')];
                return $.extend(attribute, {
                    error: ko.observable(),
                    options: $.map(attribute.options, function (option) {
                        return $.extend(option, {
                            isAvailable: ko.computed(function (option) {
                                return !!option.allowedProducts || _this.hasAllowedProduct(option);
                            }.bind(_this, option, attribute))
                        });
                    })
                });
            }.bind(this)));
        },
        onOptionClick: function (attribute, option) {
            if (option.allowedProducts) {
                this.allowedProducts(option.allowedProducts || []);
            }
            this.updateOption(option, attribute);
            var productId = this.getProductId();
            this.product($.extend(this.product(), this.optionsConfig.config.prices[productId] || {}));
            if (!option.allowedProducts || this.isInit) {
                this.gallery(this.optionsConfig.config.images[productId] || {});
            }
        },
        getProductId: function() {
            var products = this.optionsConfig.config.productAttributes;
            var product = Object.keys(this.selected()).length === this.attributes().length ?
                Object.keys(products).filter(function(productId) {
                    return Object.keys(this.selected()).reduce(function(valid, attributeId) {
                        return this.selected()[attributeId].id === products[productId][attributeId] ? valid : false;
                    }.bind(this), true);
                }.bind(this)) : {};
            return product.length === 1 ? product[0] : this.optionsConfig.config.productId;
        },
        hasAllowedProduct: function (option) {
            return option.products.reduce(function (hasProduct, product) {
                return hasProduct ? hasProduct : (this.allowedProducts().indexOf(product) !== -1 ? true : hasProduct);
            }.bind(this), false);
        },
        updateOption: function (option, attribute) {
            var selected = this.selected();
            selected[attribute.id] = option;

            //Update selection
            $.each(selected, function(attributeId, option) {
                if (!option.isAvailable()) {
                    delete selected[attributeId];
                }
            });
            //Reset errors
            attribute.error('');
            this.selected(selected);
        },
        isSelected: function (option, attribute) {
            return this.getSelected(attribute, 'id') === option.id;
        },
        getSelected: function (attribute, property) {
            return this.selected()[attribute.id] ? this.selected()[attribute.id][property] : '';
        },
        onAddToCart: function() {
            $.each(this.attributes(), function(i, attribute) {
                if (!this.selected()[attribute.id]) {
                    attribute.error(Translator.translate('This is a required field.'));
                }
            }.bind(this));
        },
        toDisplayPrice: function(price) {
            return this.optionsConfig.priceTemplate.evaluate({price: parseFloat(price).toFixed(2)});
        },
        initOptionSelection: function(renderer, attribute) {
            if (attribute.options.length > 0) {
                this.onOptionClick(attribute, attribute.options[0]);
            }
        }
    });
    $(document).ready(function () {
        if ($('#configurableOptions').length) {
            $('#configurableOptions').configurableOptions({
                optionsConfig: spConfig,
                optionsPrice: optionsPrice,
            });
        }
    });
})(jQuery);
