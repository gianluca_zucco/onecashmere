(function ($) {
    $.widget('ui.slickWidget', {
        options: {
            config: {}
        },
        _create: function () {
            this.element.slick($.extend(this.element.data('slick') || {}, this.options.config));
        }
    });

    $(document).ready(function() {
        $('[data-slick]').slickWidget();
    });
})(jQuery);
