(function ($) {
    $.widget('ui.layeredNav', {
        options: {
            childSelector: '.attr-filter label'
        },
        _create: function () {
            this._on({
                'click .attr-filter label': this.toggleClassToLabel,
                'click button.apply': this.applyFilter,
                'click button.clear': this.clearFilter
            });
        },
        toggleClassToLabel: function (Event) {
            $(Event.currentTarget).toggleClass('selected');
        },
        applyFilter: function (Event) {
            var form = $('#' + $(Event.currentTarget).data('form'));
            var name = form.find('input').attr('name');
            this.redirect(this.buildUrl(form.serialize(), name));
        },
        clearFilter: function (Event) {
            var form = $('#' + $(Event.currentTarget).data('form'));
            var name = form.find('input').attr('name');
            this.redirect(this.buildUrl('', name));
        },
        buildUrl: function (additional, name) {
            var baseUrl = window.location.href.split('?')[0];
            var query = this.prepareQueryString(name);
            return (!query && !additional) ? baseUrl : baseUrl + '?' + query + (additional ? (query ? '&' : '') + additional : '');
        },
        prepareQueryString: function(name) {
            var query = decodeURI(window.location.search).replace(/^\?&?/, '').split('&');
            return $.grep(query, function(element) {
                return !(element.indexOf(name) === 0);
            }).join('&');
        },
        redirect: function(url) {
            window.location.href = url;
        }
    });
    $(document).ready(function () {
        $('.block-layered-nav').layeredNav();
    });
})(jQuery);
