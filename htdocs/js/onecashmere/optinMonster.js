(function($) {
    $(document).on('OptinMonsterBeforeShow', function(event, props, object) {
        object.setProp('email_error', Translator.translate('Per favore, scrivi un indirizzo e-Mail corretto'));
    });
})(jQuery);
