(function($) {
    $.widget('ui.toggler', {
        _create: function() {
            this._on({
                'click': function() {
                    $(this.element.data('target')).toggle({
                        complete: function () {
                            if ($(this).is(':visible')) {
                                $(this).find('input').first().trigger('focus');
                            }
                        }
                    });
                }
            });
        }
    });

    $(document).ready(function() {
        $('[data-toggle="toggle"]').toggler();
    });
})(jQuery);
