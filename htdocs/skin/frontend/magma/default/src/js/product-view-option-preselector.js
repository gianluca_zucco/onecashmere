(function($) {
    $.widget('ui.sizePreSelector', {
        options: {
            selector: 'img.product-option'
        },
        _create: function() {
            this.element.find(this.options.selector).first().trigger('click');
        }
    });
    $(document).ready(function() {
        $('#product-options-wrapper').sizePreSelector();
    });
})(jQuery);
