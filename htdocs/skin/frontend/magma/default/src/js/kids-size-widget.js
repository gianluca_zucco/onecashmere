(function($) {
    $.widget('ui.kidsSize', {
        options: {
            template: new Template('<tr><td>#{ages}</td><td class="price">#{price}</td></tr>')
        },
        _create: function() {
            this.sizes = spConfig.config.attributes[155].options;
            this.prices = confData.optionProducts;
            this.buildPriceTable();
        },
        getLabels: function () {
            var _this = this;
            return this.sizes.reduce(function (labels, option) {
                if (_this._isOption(option)) {
                    $.each(_this.prices, function (key) {
                        if (key.toString().indexOf(option.id + ",") !== -1) {
                            var price = this.price;
                            if (!labels.hasOwnProperty(price)) {
                                labels[price] = [];
                            }
                            _this.prices[option.id] = $.extend(_this.prices[option.id], {
                                price: price,
                                price_html: this.price_html,
                                price_clone_html: this.price_clone_html
                            });
                            var label = option.label.split(' ').shift();
                            if ($.inArray(label, labels[price]) === -1) {
                                labels[price].push(label);
                            }
                        }
                    });
                }
                return labels;
            }, {});
        },
        buildPriceTable: function() {
            var content = '';
            var _this = this;
            $.each(this.getLabels(), function(price) {
                content += _this.options.template.evaluate({
                    ages: [this.join(' - '), Translator.translate('years')].join(' '),
                    price: spConfig.formatPrice(price, false)
                });
            });
            this.element.find('tbody').html(content);
            this.element.show();
        },
        _isOption: function(option) {
            return typeof option === 'object' && option.hasOwnProperty('label');
        }
    });
    $(document).ready(function() {
        $('#children-size-prices-table').kidsSize();
    });
})(jQuery);
