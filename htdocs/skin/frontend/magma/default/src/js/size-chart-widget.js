(function($) {
    $.widget('ui.sizeChartSlider', {
        options: {
            slickConfig: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false,
                adaptativeHeight: false,
                autoPlay: false,
                variableWidth: false

            }
        },
        _create: function() {
            this.element.slick(this.options.slickConfig);
            this._on('a[data-slide-index]', {
                click: this.onChartHandlerClick
            });
        },
        onChartHandlerClick: function(Event) {
            var index = $(Event.target).data('slide-index');
            this.element.slick('slickGoTo', parseInt(index));

        }
    });
    $(document).ready(function() {
        $('.size-media-slider').sizeChartSlider();
    });
})(jQuery);
