// extension Code

AmConfigurableData = Class.create();
AmConfigurableData.prototype =
{
    textNotAvailable: "",

    mediaUrlMain: "",

    currentIsMain: "",

    optionProducts: null,

    optionDefault: new Array(),

    oneAttributeReload: false,

    isResetButton: false,

    imageContainer: '.product-img-box',

    initialize: function (optionProducts) {
        this.optionProducts = optionProducts;
    },
    //special for simple price
    reloadOptions: function () {
        if ('undefined' != typeof(spConfig) && spConfig.settings) {
            spConfig.settings.each(function (select) {
                if (select.enable) {
                    spConfig.reloadOptionLabels(select);
                }
            });
        }
    },

    hasKey: function (key) {
        return ('undefined' != typeof(this.optionProducts[key]));
    },

    getData: function (key, param) {
        if (this.hasKey(key) && 'undefined' != typeof(this.optionProducts[key][param])) {
            return this.optionProducts[key][param];
        }
        return false;
    },

    saveDefault: function (param, data) {
        this.optionDefault['set'] = true;
        this.optionDefault[param] = data;
    },

    getDefault: function (param) {
        if ('undefined' != typeof(this.optionDefault[param])) {
            return this.optionDefault[param];
        }
        return false;
    }
}
// extension Code End

Product.Config.prototype.amOrig_resetChildren = Product.Config.prototype.resetChildren;
Product.Config.prototype.resetChildren = function (element) {
    this.amOrig_resetChildren(element);

    this.processEmpty();
}

Product.Config.prototype.overrideSelect = function(attributeId, holderDiv, _currentOption, key, isTheOne)    {
    // extension Code
    if (this.config.attributes[attributeId].use_image) {
        var imgContainer = document.createElement('div');
        imgContainer = $(imgContainer); // fix for IE
        imgContainer.addClassName('amconf-image-container');
        imgContainer.addClassName('amconf-image-container-'+attributeId);
        imgContainer.id = 'amconf-images-container-' + attributeId;
        imgContainer.writeAttribute('data-attribute', attributeId);
        imgContainer.style.float = 'left';
        holderDiv.appendChild(imgContainer);
        if (_currentOption.color) {
            var width = 50;
            var height = 50;
            if (this.config.attributes[attributeId].config.small_width) {
                width = this.config.attributes[attributeId].config.small_width;
            }
            if (this.config.attributes[attributeId].config.small_height) {
                height = this.config.attributes[attributeId].config.small_height;
            }
            var div = document.createElement('div');
            div.setStyle({
                width: width + 'px',
                height: height + 'px',
                background: '#' + _currentOption.color
            });
            imgContainer.appendChild(div);
            div.id = 'amconf-image-' + _currentOption.id;
            div.observe('click', this.configureImage.bind(this));
            if (this.config.attributes[attributeId].config && this.config.attributes[attributeId].config.use_tooltip != "0" && 'undefined' != typeof(jQuery)) {
                var amcontent = "";
                width = this.config.attributes[attributeId].config.big_width;
                height = this.config.attributes[attributeId].config.big_height;
                switch (this.config.attributes[attributeId].config.use_tooltip) {
                    case "1":
                        amcontent = '<div class="amtooltip-label">' + _currentOption.label + '</div>';
                        break;
                    case "2":
                        amcontent = '<div class="amtooltip-img"><div style="width: ' + width + 'px; height:' + height + 'px; background: #' + _currentOption.color + '"></div></div>';
                        break;
                    case "3":
                        amcontent = '<div class="amtooltip-img"><div style="width: ' + width + 'px; height:' + height + 'px; background: #' + _currentOption.color + '"></div><div class="amtooltip-label">' + _currentOption.label + '</div>';
                        break;
                }
                try {
                    jQuery(div).tooltipster({
                        content: jQuery(amcontent),
                        theme: 'tooltipster-light',
                        animation: 'grow',
                        touchDevices: false,
                        position: "top"
                    });
                }
                catch (exc) {
                    console.debug(exc);
                }
            }
        } else {
            var image = document.createElement('img');
            image = $(image); // fix for IE
            image.id = 'amconf-image-' + _currentOption.id;
            image.src = _currentOption.image;
            image.writeAttribute('data-attribute', attributeId);
            image.addClassName('product-option');
            // for out of stock options
            var keyOpt = key + _currentOption.id;
            image.alt = _currentOption.label;

            image.title = _currentOption.label;

            if (isTheOne) image.addClassName('first-selectable-option');

            //Check if option has dependant option with different price
            var optionPrice = null;
            var hasDiscount = false;
            for (m in confData.optionProducts) {

                //Excluding non-object items
                if(typeof confData.optionProducts[m] != 'object') continue;

                if (m.indexOf(',') === -1 || attributeId != 155) continue;

                if (m.split(',')[0] != _currentOption.id) continue;

                //save option price
                if (optionPrice == null) {
                    optionPrice = confData.optionProducts[m].price;
                    continue;
                }			
								
                if (confData.optionProducts[m].price != optionPrice) {
                    //console.debug($(image).id);
                    //console.debug(m +": "+ confData.optionProducts[m].price +" / "+ optionPrice)
                    hasDiscount = true;
                    optionPrice = null;
                    break;
                }
            }

            if (hasDiscount) {
                $(imgContainer).addClassName('has-discount');
            }

            if (typeof confData != 'undefined' && confData.getData(keyOpt, 'not_is_in_stock')) {
                image.addClassName('amconf-image-outofstock');
            } else {

                image.observe('click', this.configureImage.bind(this));
            }


            if (this.config.attributes[attributeId].config && this.config.attributes[attributeId].config.use_tooltip != "0" && 'undefined' != typeof(jQuery)) {
                var amcontent = "";
                switch (this.config.attributes[attributeId].config.use_tooltip) {
                    case "1":
                        amcontent = '<div class="amtooltip-label">' + _currentOption.label + '</div>';
                        break;
                    case "2":
                        amcontent = '<div class="amtooltip-img"><img src="' + _currentOption.bigimage + '"/></div>';
                        break;
                    case "3":
                        amcontent = '<div class="amtooltip-img"><img src="' + _currentOption.bigimage + '"/></div><div class="amtooltip-label">' + _currentOption.label + '</div>';
                        break;
                }
                try {
                    jQuery(image).tooltipster({
                        content: jQuery(amcontent),
                        theme: 'tooltipster-light',
                        animation: 'grow',
                        touchDevices: false,
                        position: "top"
                    });
                }
                catch (exc) {
                    console.debug(exc);
                }
            }

            imgContainer.appendChild(image);
            if (this.config.attributes[attributeId].config && this.config.attributes[attributeId].config.use_title != "0") {
                var amImgTitle = document.createElement('div');
                amImgTitle = $(amImgTitle); // fix for IE
                amImgTitle.addClassName('amconf-image-title');
                amImgTitle.id = 'amconf-images-title-' + _currentOption.id;
                amImgTitle.setStyle({
                    fontWeight: 600,
                    textAlign: 'center',
                    paddingTop: '7px'
                });
                amImgTitle.innerHTML = _currentOption.label;
                imgContainer.appendChild(amImgTitle);
            }
            image.onload = function () {
                var optId = this.id.replace(/[a-z-]*/, '');
                var maxW = this.getWidth();
                var maxH = this.getHeight();
                if (optId) {
                    var title = $('amconf-images-title-' + optId);
                    if (title && title.getWidth() && title.getWidth() > maxW) {
                        maxW = title.getWidth();
                    }

                }
                if (this.parentNode) {
                    this.parentNode.style.width = maxW + 'px';
                }
                if (this.parentNode.getElementsByTagName('hr')[0]) {
                    this.parentNode.getElementsByTagName('hr')[0].style.width = Math.sqrt(maxW * maxW + maxH * maxH) + 1 + 'px';
                    this.parentNode.getElementsByTagName('hr')[0].style.top = maxH / 2 + 1 + 'px';
                    this.parentNode.getElementsByTagName('hr')[0].style.left = -(Math.sqrt(maxW * maxW + maxH * maxH) - maxH) / 2 + 2 + 'px';
                }
                if (this.parentNode.childElements()[1] && !this.parentNode.childElements()[1].hasClassName('amconf-image')) {
                    this.parentNode.childElements()[1].style.width = maxW + 'px';
                }
            };
        }
    }
    // extension Code End
}

Product.Config.prototype.fillSelect = function (element) {

    var attributeId = element.id.replace(/[a-z]*/, '');
    var options = this.getAttributeOptions(attributeId);
    this.clearSelect(element);
    element.options[0] = new Option(this.config.chooseText, '');

    if ('undefined' != typeof(AmTooltipsterObject)) {
        AmTooltipsterObject.load();
    }

    var prevConfig = false;
    if (element.prevSetting) {
        prevConfig = element.prevSetting.options[element.prevSetting.selectedIndex];
    }

    if (options) {
        if ($('amconf-images-' + attributeId)) {
            $('amconf-images-' + attributeId).parentNode.removeChild($('amconf-images-' + attributeId));
        }

        if (this.config.attributes[attributeId].use_image) {
            holder = element.parentNode;
            holderDiv = document.createElement('div');
            holderDiv = $(holderDiv); // fix for IE
            holderDiv.addClassName('amconf-images-container');
            holderDiv.id = 'amconf-images-' + attributeId;
            holder.insertBefore(holderDiv, element);
        }

        var key = $F($('attribute155'));
        var priceGroups = [];
        var differentPrices = 0;
        var relatedOptions = [];
        if (attributeId == 92 && key != null)  {
            var _options = this.getAttributeOptions(92);
            for (var l in _options) {
                if (typeof _options[l] != 'object') continue;

                var doubleKey = key+','+_options[l].id;

                if (!confData.optionProducts.hasOwnProperty(doubleKey)) continue;

                var combinationPrice = confData.getData(doubleKey, 'price');

                if (!combinationPrice) continue;

                relatedOptions.push(_options[l]);

                if (!priceGroups.hasOwnProperty(combinationPrice)) {
                    differentPrices++;
                    priceGroups[combinationPrice] = [];
                }

                priceGroups[combinationPrice].push(_options[l]);
            }
        }

        var index = 1;
        var key = '';

        this.settings.each(function (select, ch) {
            // will check if we need to reload product information when the first attribute selected
            if (parseInt(select.value)) {
                key += select.value + ',';
            }
        });

        if (differentPrices > 1) {
            for(var price in priceGroups)   {
                var _optionList = priceGroups[price];
                if (typeof _optionList != 'object') continue;
                $('amconf-image-'+key.split(',')[0]).addClassName('has-discount');
                index += this.loopConfigurableOptions(_optionList, attributeId, element, index, key, price);
            }
        } else {
            if (attributeId == 92) {
                index = this.loopConfigurableOptions(relatedOptions, attributeId, element, index, key);
            } else {
                index = this.loopConfigurableOptions(options, attributeId, element, index, key);
            }
        }

        if (this.config.attributes[attributeId].use_image) {
            var lastContainer = document.createElement('div');
            lastContainer = $(lastContainer); // fix for IE
            lastContainer.setStyle({clear: 'both'});
            holderDiv.appendChild(lastContainer);
        }

    }
}

/**
 * Creates option divs
 * Function takes into account different price than parent, and if there is one, it creates a different html on
 * frontend.
 *
 */
Product.Config.prototype.loopConfigurableOptions = function(options, attributeId, element, index, key, price)  {
    var cntn = document.createElement('div');

    if (price)  {
        var inStock = false;
        for (var m in options) {
            var _currentOption = options[m];
            if (typeof _currentOption != 'object') continue;
            if (!_currentOption.id) continue;
            var keyOpt = key+_currentOption.id;
            if(!confData.getData(keyOpt, 'not_is_in_stock')) {
                inStock = true;
                break;
            }
        }
        if (inStock)    {
            title = document.createElement('h3');
            $(title).update(this.formatPrice(price));
            cntn.appendChild(title);
        }
    }

    for (var m in options) {
        var _currentOption = options[m];
        //Exclude prototyped functions
        if (typeof _currentOption != 'object') continue;
        var allowedProducts = _currentOption.products.clone();
        if (allowedProducts.size() > 0) {
            var isTheOne = false;
            //preselect first option of first attribute
            if (this.settings[0].id == element.id && m == 0) isTheOne = true;
            this.overrideSelect(attributeId, cntn, _currentOption, key, isTheOne);

            _currentOption.allowedProducts = allowedProducts;
            element.options[index] = new Option(this.getOptionLabel(_currentOption, _currentOption.price), _currentOption.id);
            element.options[index].config = _currentOption;
            index++;
        }
    }

    if (price)  {
        clear = document.createElement('div');
        clear.style = 'clear:both';
        cntn.appendChild(clear);
    }

    holderDiv.appendChild(cntn);
    return index;
}

Product.Config.prototype.configureElement = function (element) {
    // extension Code
    optionId = element.value;
    if ($('amconf-image-' + optionId)) {
        this.selectImage($('amconf-image-' + optionId));
    } else {
        attributeId = element.id.replace(/[a-z-]*/, '');
        if ($('amconf-images-' + attributeId)) {
            $('amconf-images-' + attributeId).childElements().each(function (child) {
                child.childElements().each(function (children) {
                    children.removeClassName('amconf-image-selected');
                });
            });
        }
    }
    // extension Code End

    this.reloadOptionLabels(element);

    //Here renders following options
    if (element.value) {
        this.state[element.config.id] = element.value;
        if (element.nextSetting) {
            element.nextSetting.disabled = false;
            this.fillSelect(element.nextSetting);
            this.resetChildren(element.nextSetting);
        }
    }
    else {
        // extension Code
        if (element.childSettings) {
            for (var i = 0; i < element.childSettings.length; i++) {
                attributeId = element.childSettings[i].id.replace(/[a-z-]*/, '');
                if ($('amconf-images-' + attributeId)) {
                    $('amconf-images-' + attributeId).parentNode.removeChild($('amconf-images-' + attributeId));
                }
            }
        }
        // extension Code End

        this.resetChildren(element);

        // extension Code
        if (this.settings[0].hasClassName('no-display')) {
            this.processEmpty();
        }
        // extension Code End
    }
    // extension Code
    var key = '';
    var stock = 1;
    this.settings.each(function (select, ch) {
        // will check if we need to reload product information when the first attribute selected
        if (parseInt(select.value)) {
            key += select.value + ',';
            if (confData.getData(key.substr(0, key.length - 1), 'not_is_in_stock')) {
                stock = 0;
            }
        }
    });
    if (typeof confData != 'undefined') {
        confData.isResetButton = false;
    }
    key = key.substr(0, key.length - 1);
    this.updateData(key, element.id.split('attribute').pop());
    //<---- ---->
    if (typeof confData != 'undefined' && confData.useSimplePrice == "1") {
        this.reloadSimplePrice(key); // replace price values with the selected simple product price
    }
    else {
        this.reloadPrice(); // default behaviour
    }
    if (stock === 0) {
        $$('.add-to-cart').each(function (elem) {
            elem.hide();
        });
    }
    else {
        $$('.add-to-cart').each(function (elem) {
            elem.show();
        });
    }
    // for compatibility with custom stock status extension:
    if ('undefined' != typeof(stStatus) && 'function' == typeof(stStatus.onConfigure)) {
        var keySt = '';
        this.settings.each(function (select, ch) {
            if (parseInt(select.value) || (!select.value && (!select.options[1] || !select.options[1].value))) {
                keySt += select.value + ',';
            }
            else {
                keySt += select.options[1].value + ',';
            }
        });
        keySt = keySt.substr(0, keySt.length - 1);
        stStatus.onConfigure(keySt, this.settings, key);
    }
    //Amasty code for Automatically select attributes that have one single value
    if (('undefined' != typeof(amConfAutoSelectAttribute) && amConfAutoSelectAttribute) || ('undefined' != typeof(amStAutoSelectAttribute) && amStAutoSelectAttribute)) {
        var nextSet = element.nextSetting;
        if (nextSet && nextSet.options.length == 2 && !nextSet.options[1].selected && element && !element.options[0].selected) {
            nextSet.options[1].selected = true;
            this.configureElement(nextSet);
        }
    }
    if ('undefined' != typeof(preorderState))
        preorderState.update()


    var label = "";
    element.config.options.each(function (option) {
        if (option.id == element.value) label = option.label;
    });
    if (label) label = " - " + label;
    var parent = element.parentNode.parentNode.previousElementSibling;
    if (typeof(parent) != 'undefined' && parent != null && parent.nodeName == "DT" && (conteiner = parent.select("label")[0])) {
        if (tmp = conteiner.select('span.amconf-label')[0]) {
            tmp.innerHTML = label;
        }
        else {
            var tmp = document.createElement('span');
            tmp.addClassName('amconf-label');
            conteiner.appendChild(tmp);
            tmp.innerHTML = label;
        }
    }
    // extension Code End
}

Product.Config.prototype.amPreselectOneOptionAttribute = function () {
    if (('undefined' != typeof(amConfAutoSelectAttribute) && amConfAutoSelectAttribute) || ('undefined' != typeof(amStAutoSelectAttribute) && amStAutoSelectAttribute)) {
        var select = this.settings[0];
        if (select && select.options.length == 2 && !select.options[1].selected) {
            select.options[1].selected = true;
            this.configureElement(select);
        }
    }
}

// these are new methods introduced by the extension
// extension Code
Product.Config.prototype.configureHr = function (event) {
    var element = Event.element(event);
    element.nextSibling.click();
}


Product.Config.prototype.configureImage = function (event) {
    var element = Event.element(event);

    var attributeId = element.readAttribute('data-attribute');
    var optionId = element.id.replace(/[a-z-]*/, '');

    this.selectImage(element);

    var originalSelect = $('attribute' + attributeId);
    originalSelect.value = optionId;
    originalSelect.writeAttribute('data-attribute', attributeId);

    this.configureElement(originalSelect);
    /* fix for sm ajax cart*/
    if ($$('body.sm_market').length > 0) {
        jQuery('#attribute' + attributeId).trigger("change");
    }
}

/**
 * It select an image, deselecting others
 */
Product.Config.prototype.selectImage = function (element) {
    attributeId = element.parentNode.id.replace(/[a-z-]*/, '');

    $$('.amconf-image-container-'+attributeId+' img').each(function(item){
        $(item).removeClassName('amconf-image-selected');
    });

    element.addClassName('amconf-image-selected');
}

Product.Config.prototype.processEmpty = function () {
    $$('.super-attribute-select').each(function (select) {
        var attributeId = select.id.replace(/[a-z]*/, '');
        if (select.disabled) {
            if ($('amconf-images-' + attributeId)) {
                $('amconf-images-' + attributeId).parentNode.removeChild($('amconf-images-' + attributeId));
            }
            holder = select.parentNode;
            holderDiv = document.createElement('div');
            holderDiv.addClassName('amconf-images-container');
            holderDiv.id = 'amconf-images-' + attributeId;
            if ('undefined' != typeof(confData)) {
                holderDiv.innerHTML = confData.textNotAvailable;
            } else {
                holderDiv.innerHTML = "";
            }
            holder.insertBefore(holderDiv, select);
        } else if (!select.disabled && !$(select).hasClassName("no-display")) {
            var element = $(select.parentNode).select('#amconf-images-' + attributeId)[0];
            if (typeof confData != 'undefined' && typeof element != 'undefined' && element.innerHTML == confData.textNotAvailable) {
                element.parentNode.removeChild(element);
            }
        }
    }.bind(this));
}

Product.Config.prototype.clearConfig = function () {
    this.settings[0].value = "";
    if (typeof confData != 'undefined')
        confData.isResetButton = true;
    this.configureElement(this.settings[0]);
    $$('span.amconf-label').each(function (el) {
        el.remove();
    })
    return false;
}


//start code for reload simple price

Product.Config.prototype.reloadSimplePrice = function (key) {

    if ('undefined' == typeof(confData)) {
        return false;
    }

    var container;
    var result = false;
    if (confData.hasKey(key)) {
        // convert div.price-box into price info container
        // top price box
        if (confData.getData(key, 'price_html')) {
            $$('.product-shop .price-box, #product_addtocart_form .price-box').each(function (container) {
                if (!confData.getDefault('price_html')) {
                    confData.saveDefault('price_html', container.innerHTML);
                }
                container.addClassName('amconf_price_container');
            }.bind(this));


            $$('.product-shop .tax-details, .product-shop .tier-prices').each(function (container) {
                container.remove();
            }.bind(this));

            $$('.amconf_price_container').each(function (container) {
                container.outerHTML = confData.getData(key, 'price_html');
            }.bind(this));
        }

        // bottom price box
        if (confData.getData(key, 'price_clone_html')) {
            $$('.product-options-bottom .price-box').each(function (container) {
                if (!confData.getDefault('price_clone_html')) {
                    confData.saveDefault('price_clone_html', container.innerHTML);
                }
                container.addClassName('amconf_price_clone_container');
            }.bind(this));

            $$('.product-options-bottom .tax-details, .product-options-bottom .tier-prices').each(function (container) {
                container.remove();
            }.bind(this));

            $$('.amconf_price_clone_container').each(function (container) {
                container.outerHTML = confData.getData(key, 'price_clone_html');
            }.bind(this));

        }

        // function return value
        if (confData.getData(key, 'price')) {
            result = confData.getData(key, 'price');
        }
    }
    else {
        // setting values of default product
        if (true == confData.getDefault('set')) {
            // restore price info containers into default price-boxes
            if (confData.getDefault('price_html')) {
                $$('.product-shop .price-box, #product_addtocart_form .price-box').each(function (container) {
                    container.addClassName('amconf_price_container');
                }.bind(this));
                $$('.product-shop .tax-details, .product-shop .tier-prices').each(function (container) {
                    container.remove();
                }.bind(this));

                $$('.amconf_price_container').each(function (container) {
                    container.innerHTML = confData.getDefault('price_html');
                    container.removeClassName('amconf_price_container');
                }.bind(this));
            }

            if (confData.getDefault('price_clone_html')) {
                $$('.product-options-bottom .price-box').each(function (container) {
                    container.addClassName('amconf_price_clone_container');
                }.bind(this));

                $$('.amconf_price_clone_container').each(function (container) {
                    container.innerHTML = confData.getDefault('price_clone_html');
                    container.removeClassName('amconf_price_clone_container');
                }.bind(this));

            }

            // function return value
            if (confData.getDefault('price')) {
                result = confData.getDefault('price');
            }
        }
    }

    return result; // actually the return value is never used
}

Product.Config.prototype.getOptionLabel = function (option, price) {
    var price = parseFloat(price);
    if (this.taxConfig.includeTax) {
        var tax = price / (100 + this.taxConfig.defaultTax) * this.taxConfig.defaultTax;
        var excl = price - tax;
        var incl = excl * (1 + (this.taxConfig.currentTax / 100));
    } else {
        var tax = price * (this.taxConfig.currentTax / 100);
        var excl = price;
        var incl = excl + tax;
    }
    if (this.taxConfig.showIncludeTax || this.taxConfig.showBothPrices) {
        price = incl;
    } else {
        price = excl;
    }
    var str = option.label;

    if (price) {
        if ('undefined' != typeof(confData) && confData.useSimplePrice == "1" && confData['optionProducts'] && confData['optionProducts'][option.id] && confData['optionProducts'][option.id]['price']) {
            str += ' ' + this.formatPrice(confData['optionProducts'][option.id]['price'], true);
            pos = str.indexOf("+");
            str = str.substr(0, pos) + str.substr(pos + 1, str.length);
        }
        else {
            if (this.taxConfig.showBothPrices) {
                str += ' ' + this.formatPrice(excl, true) + ' (' + this.formatPrice(price, true) + ' ' + this.taxConfig.inclTaxTitle + ')';
            } else {
                str += ' ' + this.formatPrice(price, true);
            }
        }
    }
    else {
        if ('undefined' != typeof(confData) && confData.useSimplePrice == "1" && confData['optionProducts'] && confData['optionProducts'][option.id] && confData['optionProducts'][option.id]['price']) {
            str += ' ' + this.formatPrice(confData['optionProducts'][option.id]['price'], true);
            pos = str.indexOf("+");
            str = str.substr(0, pos) + str.substr(pos + 1, str.length);
        }
    }
    return str;
}

Event.observe(window, 'load', function () {
    if ('undefined' != typeof(confData) && confData.useSimplePrice == "1") {
        confData.reloadOptions();
    }
    if ('undefined' != typeof(spConfig)) {
        spConfig.amPreselectOneOptionAttribute();
    }
});

Product.Config.prototype.updateData = function (key, attributeId) {

    if ('undefined' == typeof(confData)) {
        return false;
    }

    if (confData.hasKey(key)) {
        // getting values of selected configuration
        this.updateSimpleData('name', '.product-name h1', key);
        this.updateSimpleData('short_description', '.short-description div', key);
        this.updateSimpleData('description', '.box-description div', key);
        this.updateSimpleData('attributes', '#product-attribute-specs-table', key);
        if (confData.getData(key, 'media_url') && attributeId != 155) {
            // should reload images
            var tmpContainer = $$(confData.imageContainer)[0];
            if (!tmpContainer) {
                console.debug("Please set correctly CSS selector at module configuration!");
            }
            else {
                new Ajax.Updater(tmpContainer, confData.getData(key, 'media_url'), {
                    evalScripts: true,
                    onCreate: function () {
                        confData.saveDefault('media', tmpContainer.innerHTML);
                        confData.currentIsMain = false;
                    },
                    onComplete: function () {
                        if ('undefined' != typeof(AmZoomerObj)) {
                            if ($$('.zoomContainer')[0]) $$('.zoomContainer')[0].remove();
                            AmZoomerObj.loadZoom();
                        }
                        jQuery('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
                    }
                });
            }
        }
    } else {
        // setting values of default product
        if (true == confData.getDefault('set')) {
            this.getDefaultSimpleData('name', '.product-name h1');
            this.getDefaultSimpleData('short_description', '.short-description div');
            this.getDefaultSimpleData('description', '.box-description div');
            this.getDefaultSimpleData('attributes', '#product-attribute-specs-table');

            if (confData.getDefault('media') && !confData.currentIsMain) {
                var tmpContainer = $$(confData.imageContainer)[0];
                new Ajax.Updater(tmpContainer, confData.mediaUrlMain, {
                    evalScripts: true,
                    onSuccess: function (transport) {
                        confData.saveDefault('media', tmpContainer.innerHTML);
                        confData.currentIsMain = true;
                    },
                    onComplete: function () {
                        if ('undefined' != typeof(AmZoomerObj)) {
                            if ($$('.zoomContainer')[0]) $$('.zoomContainer')[0].remove();
                            AmZoomerObj.loadZoom();
                        }
                        jQuery('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
                    }
                });
            }
        }
    }
}

Product.Config.prototype.updateSimpleData = function (type, selector, key) {
    if (confData.getData(key, type)) {
        $$(selector).each(function (container) {
            if (!confData.getDefault(type)) {
                confData.saveDefault(type, container.innerHTML);
            }
            if (confData.getData(key, type) != "") container.innerHTML = confData.getData(key, type);
        }.bind(this));
    }
}

Product.Config.prototype.getDefaultSimpleData = function (type, selector) {
    if (confData.getDefault(type)) {
        $$(selector).each(function (container) {
            container.innerHTML = confData.getDefault(type);
        }.bind(this));
    }
}

// extension Code End
