<?php

require_once 'abstract.php';

class GoogleShoppingFeed extends Mage_Shell_Abstract
{
    public function run()
    {
        ini_set('memory_limit', '1,5G');
        Mage::getModel('onecashmere_googleshopping/cron')->generateFeeds();
    }
}

$shell = new GoogleShoppingFeed();
$shell->run();
