<?php

require_once 'abstract.php';

class Onecashmere_Shell_StockIndexer extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        try {
            $flag = (new Onecashmere_Catalog_Model_Stock_Flag())->loadSelf();
            if ($flag->getState()) {
                $this->getStockIndexer()->reindexAll();
                $flag->delete();
            }
        } catch (Exception $e) {
            //No exception in case indexer is already running
        }
    }

    /**
     * @return false|Mage_Index_Model_Process
     */
    private function getStockIndexer()
    {
        return Mage::getModel('index/indexer')->getProcessByCode('cataloginventory_stock');
    }
}
(new Onecashmere_Shell_StockIndexer())->run();
