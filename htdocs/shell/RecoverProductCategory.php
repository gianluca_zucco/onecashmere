<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

require_once 'abstract.php';

class Onecashmere_Shell_RecoverProductCategory extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        Mage::getResourceModel('catalog/product_collection')
            ->addFieldToFilter('type_id', Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE)
            ->walk(function(Mage_Catalog_Model_Product $product) {
                $categoryIds = $product->getCategoryIds();
                /** @var Mage_Catalog_Model_Product_Type_Configurable $instance */
                $instance = $product->getTypeInstance();
                $instance->getUsedProductCollection($product)->addAttributeToSelect('*')->walk(function(Mage_Catalog_Model_Product $child) use ($categoryIds) {
                    $child->setCategoryIds($categoryIds);
                    $child->setData('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
                    $child->save();
                });
                fwrite(STDOUT, $product->getSku() . PHP_EOL);
            });
    }
}
$class = new Onecashmere_Shell_RecoverProductCategory();
$class->run();
