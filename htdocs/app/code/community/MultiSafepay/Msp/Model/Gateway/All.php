<?php

/**
 *
 * @category MultiSafepay
 * @package  MultiSafepay_Msp
 */
class MultiSafepay_Msp_Model_Gateway_All extends MultiSafepay_Msp_Model_Gateway_Abstract 
{
    protected $_code = "msp_all";
    public $_model = "all";
    public $_gateway = "ALL";

}
