<?php

class KS_Alternate_Model_Observer
{
    public function alternateLinks()
    {
        $headBlock = Mage::app()->getLayout()->getBlock('head');

        $stores = Mage::app()->getStores();
        $product = Mage::registry('current_product');
        $category = Mage::registry('current_category');
        $url = '';

        if ($headBlock) {
            foreach ($stores as $store) {
                if ($store->getCode() !=Mage::app()->getStore()->getCode()) {
                    if ($product) {
                        $category ? $categoryId = $category->getId() : $categoryId = null;
                        $url = $store->getBaseUrl() . Mage::helper('ks_alternate')->rewrittenProductUrl($product->getId(),
                                $categoryId, $store->getId());
                    } else {
                        if ($category != null) {
                            $url = $store->getBaseUrl() . Mage::getModel('catalog/category')->setStoreId($store->getId())->load($category->getId())->getUrlPath();
                        } elseif (Mage::getSingleton('cms/page')->getIdentifier() == 'home') {
                            $url = $store->getBaseUrl();
                        }
                    }
                    $storeCode = substr($store->getConfig('general/locale/code'), 0, 2);
                    $headBlock->addLinkRel('alternate"' . ' hreflang="' . $storeCode, $url);
                }
            }
        }
        return $this;
    }

}
