<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @var $this Mage_Core_Model_Resource_Setup
 * @var $block Mage_Cms_Model_Block
 */

$identifiers = ['logo_static'];

foreach (Mage::app()->getStores() as $store) {
    foreach ($identifiers as $identifier) {
        $block = Mage::getModel('cms/block')->setStoreId($store->getId())->load($identifier);
        $path = Mage::getModuleDir('data', 'Onecashmere_Core');
        $str = $block->getIdentifier() ?? $identifier;
        $path = "$path/cms_block_setup/{$str}_{$store->getCode()}.html";
        if (is_file($path)) {
            $block
                ->setData('stores', [$store->getId()])
                ->setIdentifier($str)
                ->setTitle($block->getTitle() ?? "{$str} ({$store->getCode()})")
                ->setContent(file_get_contents($path))
                ->save();
        }
    }
}
