<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @var $this Mage_Core_Model_Resource_Setup
 * @var $block Mage_Cms_Model_Block
 */

$identifiers = ['footer_static', 'footer_payment', 'footer_social'];

foreach (Mage::app()->getStores() as $store) {
    foreach ($identifiers as $identifier) {
        $block = Mage::getModel('cms/block')->setStoreId($store->getId())->load($identifier);
        $path = Mage::getModuleDir('data', 'Onecashmere_Core');
        $path = "$path/cms_block_setup/{$block->getIdentifier()}_{$store->getCode()}.html";
        if (is_file($path)) {
            $block->setContent(file_get_contents($path))->save();
        }
    }
}
