<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 * @var $this Mage_Core_Model_Resource_Setup
 */

$installer = $this;
$table = $installer->getTable('admin/permission_block');
$installer->getConnection()->insertMultiple($table, array(
array(
    'block_id' => new Zend_Db_Expr('NULL'),
    'block_name' => 'newproductslider/newproductslider',
    'is_allowed' => 1
),
array(
    'block_id' => new Zend_Db_Expr('NULL'),
    'block_name' => 'featuredproduct/featuredproduct',
    'is_allowed' => 1
),
array(
    'block_id' => new Zend_Db_Expr('NULL'),
    'block_name' => 'newsletter/subscribe',
    'is_allowed' => 1
)
));
