<?php

class Onecashmere_Core_Block_Widget_DoubleImage extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    protected $_template = 'onecashmere/core/widget/double_image.phtml';

    public function getTitle(string $type): string
    {
        return $this->escapeHtml((string) $this->getData("image_{$type}_title"));
    }

    public function getAlt(string $type): string
    {
        return $this->escapeHtml((string) $this->getData("image_{$type}_alt"));
    }

    public function getLinkUrl(string $type): string
    {
        try {
            $data = $this->getData("image_{$type}_link");
            return Mage::helper('cms')->getBlockTemplateProcessor()->filter((string) $data);
        } catch (Exception $e) {
            return '';
        }
    }

    public function getImageUrl(string $type, string $device): string
    {
        $baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $data = $this->getData("image_{$type}_{$device}");
        return $data ? "$baseUrl{$data}" : '';
    }
}
