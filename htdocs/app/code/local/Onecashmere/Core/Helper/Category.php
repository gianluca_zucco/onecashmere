<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

class Onecashmere_Core_Helper_Category extends Mage_Core_Helper_Abstract
{
    /**
     * @return string
     */
    public function getProductListCacheKey()
    {
        return $this->_generateCacheKey(array(
            Mage::app()->getStore()->getCode(),
            "frontend/magma/default/template/catalog/product/list.phtml",
            Mage::registry('current_category')->getId()
        ) + $this->_getRequest()->getParams());
    }

    /**
     * @param array $info
     * @return string
     */
    protected function _generateCacheKey(array $info)
    {
        return sha1(implode('|', array_values($info)));
    }
}
