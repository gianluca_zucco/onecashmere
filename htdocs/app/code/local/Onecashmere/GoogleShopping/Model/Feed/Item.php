<?php

use Mage_Catalog_Model_Category as Category;
use Mage_Catalog_Model_Product as Product;
use Mage_Catalog_Model_Resource_Category_Collection as Categories;

class Onecashmere_GoogleShopping_Model_Feed_Item
{
    /** @var Product */
    private $product;

    /** @var Product */
    private $parent;
    /**
     * @var Mage_Catalog_Model_Resource_Category_Collection
     */
    private $categories;

    public function __construct(Product $product, Product $parent = null, Categories $categories)
    {
        $this->product = $product;
        $this->parent = $parent;
        $this->categories = $categories;
    }

    private function getMainProduct(): Product
    {
        return $this->parent ?? $this->product;
    }

    public function asArray(): array
    {
        $productCategories = $this->getProductCategories();
        $data = [
            'g:id' => $this->product->getSku(),
            'title' => Mage::helper('core')->stripTags($this->getTitle()),
            'description' => Mage::helper('core')->stripTags($this->getDescription()),
            'link' => $this->getMainProduct()->getProductUrl(),
            'g:image_link' => $this->getImage(),
            'g:price' => $this->getPrice(),
            'g:condition' => 'new',
            'g:availability' => $this->getAvailability(),
            'g:color' => $this->product->getAttributeText('color'),
            'g:size' => $this->product->getAttributeText('size'),
            'g:brand' => 'Onecashmere',
            'g:product_type' => $productCategories[count($productCategories)-1] ?? '',
            'g:mpn' =>  $this->product->getSku(),
            'g:shipping_weight' => $this->product->getData('weight'),
            'g:shipping' => [
                'g:country' => 'IT',
                'g:price' => $this->toPrice(8)
            ],
            'g:google_product_category' => $this->getGoogleProductCategory(),
        ];

        if ($this->parent) {
            $data['g:item_group_id'] = $this->getMainProduct()->getSku();
        }

        return ['item' => $data];
    }

    private function getImage(): string
    {
        return (string) Mage::helper('catalog/image')->init($this->product, 'thumbnail');
    }

    private function getPrice(): string
    {
        return $this->toPrice($this->product->getPrice());
    }

    private function toPrice(float $price): string
    {
        return Mage::helper('core')->formatCurrency($price, false);
    }

    private function getAvailability(): string
    {
        return $this->product->isSalable() ? 'in stock' : 'out of stock';
    }

    private function getProductCategories(): array
    {
        $categories = array_filter($this->categories->getItems(), function (Category $category) {
            return in_array($category->getId(), $this->getMainProduct()->getCategoryIds());
        });
        usort($categories, function (Category $a, Category $b) {
            return $a->getLevel() <=> $b->getLevel();
        });
        return array_map(function (Category $category) {
            return $category->getName();
        }, $categories);
    }

    private function getTitle(): string
    {
        return $this->getMainProduct()->getData('google_shopping_title');
    }

    private function getDescription(): string
    {
        return $this->getMainProduct()->getData('google_shopping_description');
    }

    private function getGoogleProductCategory(): string
    {
        return htmlentities($this->getMainProduct()->getData('google_product_category'));
    }
}
