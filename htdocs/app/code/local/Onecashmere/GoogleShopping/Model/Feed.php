<?php

use Onecashmere_GoogleShopping_Model_Feed_Item as FeedItem;

class Onecashmere_GoogleShopping_Model_Feed
{
    /** @var Mage_Eav_Model_Entity_Collection_Abstract  */
    private $categories;

    private $pageSize = 500;

    /**
     * Onecashmere_GoogleShopping_Model_Feed constructor.
     *
     * @throws Mage_Core_Exception
     */
    public function __construct()
    {
        $this->categories = $this->getCategories();
    }


    public function getXml(): DOMDocument
    {
        $xml = new Mage_Xml_Generator();
        $dom = $xml->getDom();
        $dom->encoding = 'UTF-8';
        $dom->formatOutput = true;

        $items = [];
        foreach ($this->getItems() as $item) {
            $items[] = $item->asArray();
        }
        $xml->arrayToXml([
            'rss' => [
                '_value' => ['channel' => $items],
                '_attribute' => [
                    'version' => '2.0',
                    'xmlns:g' => 'http://base.google.com/ns/1.0',
                    'magento' => Mage::getEdition() . ' ' . Mage::getVersion(),
                ],
            ],
        ]);
        return $dom;
    }

    private function getItems(): \Generator
    {
        $page = 1;
        while ($products = $this->getCollection($page)->getItems()) {
            /** @var Mage_Catalog_Model_Product $product */
            foreach ($products as $product) {
                /** @var Mage_Catalog_Model_Product_Type_Configurable $instance */
                if (($instance = $product->getTypeInstance()) instanceof Mage_Catalog_Model_Product_Type_Configurable) {
                    yield new FeedItem($product, null, $this->categories);
                    foreach ($instance->getUsedProducts(null, $product) as $simple) {
                        $item = new FeedItem($simple, $product, $this->categories);
                        yield $item;
                    }
                } else {
                    $item = new FeedItem($product, null, $this->categories);
                    yield $item;
                }
            }
            $page++;
        }
    }

    protected function getCollection(int $page): Mage_Catalog_Model_Resource_Product_Collection
    {
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection
            ->addAttributeToSelect('*')
            ->addFieldToFilter('visibility', ['neq' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE])
            ->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addFieldToFilter('google_shopping_title', [
                'notnull' => true,
                'neq' => ''
            ])
            ->addFieldToFilter('google_shopping_description', [
                'notnull' => true,
                'neq' => ''
            ]);
        $collection->addCategoryIds();
        $collection->getSelect()->limitPage($page, $this->pageSize);
        return $collection;
    }

    /**
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     * @throws Mage_Core_Exception
     */
    private function getCategories(): Mage_Eav_Model_Entity_Collection_Abstract
    {
        return Mage::getResourceSingleton('catalog/category_collection')
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToSelect('name')
            ->load();
    }
}
