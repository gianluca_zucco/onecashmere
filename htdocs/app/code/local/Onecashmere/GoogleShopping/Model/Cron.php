<?php

class Onecashmere_GoogleShopping_Model_Cron
{
    /** @var Onecashmere_GoogleShopping_Model_Feed  */
    private $feed;

    /** @var Varien_Io_File */
    private $io;

    /** @var string */
    private $path;

    public function __construct()
    {
        $this->path = implode(DS, [Mage::getBaseDir('media'), 'feeds']);
        $this->io = new Varien_Io_File();
        $this->feed = Mage::getModel('onecashmere_googleshopping/feed');
    }

    /**
     * @throws Exception
     */
    public function generateFeeds()
    {
        array_map([$this, 'generateFeed'], Mage::app()->getStores());
    }

    /**
     * @param Mage_Core_Model_Store $store
     * @throws Exception
     */
    private function generateFeed(Mage_Core_Model_Store $store)
    {
        $info = Mage::getModel('core/app_emulation')->startEnvironmentEmulation($store->getId());
        $filename = sprintf("google_shopping_feed_%s.xml", $store->getCode());
        $this->io->checkAndCreateFolder($this->path);
        $this->feed->getXml()->save(implode(DS, [$this->path, $filename]));
        Mage::getModel('core/app_emulation')->stopEnvironmentEmulation($info);
    }
}
