<?php
/**
 * @var $this Mage_Catalog_Model_Resource_Setup
 */

use Mage_Catalog_Model_Product as Product;
use Varien_Db_Ddl_Table as DDL;
use Mage_Catalog_Model_Resource_Eav_Attribute as CatalogAttribute;

$this->addAttribute(Product::ENTITY, 'google_product_category', [
    'type' => 'varchar',
    'lenght' => DDL::DEFAULT_TEXT_SIZE,
    'label' => 'Google Product Category',
    'group' => 'Google Shopping',
    'required' => false,
    'configurable' => false,
    'note' => 'Google Shopping Feed Category Value',
    'global' => CatalogAttribute::SCOPE_STORE,
]);
