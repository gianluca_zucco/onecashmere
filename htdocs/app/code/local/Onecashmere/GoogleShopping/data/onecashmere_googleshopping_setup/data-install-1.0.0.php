<?php
/**
 * @var $this Mage_Catalog_Model_Resource_Setup
 */

use Mage_Catalog_Model_Product as Product;
use Varien_Db_Ddl_Table as DDL;
use Mage_Catalog_Model_Resource_Eav_Attribute as CatalogAttribute;

$this->addAttribute(Product::ENTITY, 'google_shopping_title', [
    'type' => 'varchar',
    'lenght' => DDL::DEFAULT_TEXT_SIZE,
    'label' => 'Title',
    'group' => 'Google Shopping',
    'required' => false,
    'configurable' => false,
    'note' => 'Title for Google Shopping Feed',
    'global' => CatalogAttribute::SCOPE_STORE,
]);

$this->addAttribute(Product::ENTITY, 'google_shopping_description', [
    'type' => 'text',
    'lenght' => DDL::MAX_TEXT_SIZE,
    'label' => 'Description',
    'group' => 'Google Shopping',
    'required' => false,
    'configurable' => false,
    'note' => 'Description for Google Shopping Feed',
    'global' => CatalogAttribute::SCOPE_STORE,
]);
