<?php
/**
 * @var $this Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;
$installer->addAttribute('catalog_product', 'size_chart_cms_block_desc', [
    'label' => 'Blocco CMS Descrizione',
    'type' => 'int',
    'input' => 'select',
    'source' => 'catalog/category_attribute_source_page',
    'group' => 'Onecashmere',
    'note'  =>  'Blocco statico immagini',
    'product_type' => Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE,
    'required' => false,
]);
