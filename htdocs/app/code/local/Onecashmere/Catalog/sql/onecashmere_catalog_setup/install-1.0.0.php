<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 * @var $this Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;
$installer->startSetup();
$installer->addAttribute('catalog_product', 'size_file', [
    'label' => 'File delle taglie',
    'type' => 'varchar',
    'input' => 'file',
    'backend' => 'onecashmere_catalog/resource_product_attribute_backend_sizeFile',
    'group' => 'Onecashmere',
    'note'  =>  'File CSV',
    'product_type' => Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE,
    'required' => false,
]);
$installer->addAttribute('catalog_product', 'size_chart_cms_block', [
    'label' => 'Blocco CMS',
    'type' => 'int',
    'input' => 'select',
    'source' => 'catalog/category_attribute_source_page',
    'group' => 'Onecashmere',
    'note'  =>  'Blocco statico immagini',
    'product_type' => Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE,
    'required' => false,
]);
$installer->endSetup();
