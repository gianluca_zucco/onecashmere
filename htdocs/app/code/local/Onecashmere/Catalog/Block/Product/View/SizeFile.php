<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

use Mage_Catalog_Model_Product as Product;

class Onecashmere_Catalog_Block_Product_View_SizeFile extends Mage_Core_Block_Template
{
    private $data;

    public function getProduct(): Product
    {
        return Mage::registry('current_product');
    }

    private function getPath(Product $product): string
    {
        return !Mage::helper('onecashmere_catalog')->hasSizeChart($product) ? '' :
            Mage::getBaseDir('media') . "/catalog/product/size_files/{$product->getId()}/{$product->getData('size_file')}";
    }

    public function getChart(): array
    {
        try {
            if (!isset($this->data)) {
                $handler = new Varien_File_Csv();
                $this->data = $handler->getData($this->getPath($this->getProduct()));
            }
            return is_array($this->data) ? $this->data : [];
        } catch (Exception $e) {
            return [];
        }
    }

    public function getFirstLine(): array
    {
        return array_map([$this, '__'], $this->getChart()[0]);
    }

    public function getLines(): \Generator
    {
        $data = $this->getChart();
        array_shift($data);
        foreach ($data as $line) {
            yield array_map([$this, '__'], $line);
        }
    }
}
