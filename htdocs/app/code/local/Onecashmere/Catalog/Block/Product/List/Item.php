<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

use Mage_Catalog_Model_Product as Product;

class Onecashmere_Catalog_Block_Product_List_Item extends Mage_Catalog_Block_Product_View_Abstract
{
    protected $_template = 'onecashmere/catalog/product/list/item.phtml';

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $child = $this->getLayout()->createBlock('core/template', 'product.label')->setTemplate('onecashmere/catalog/product/list/item/labels.phtml');
        $this->append($child);
        return $this;
    }


    public function getProduct(): Product
    {
        return $this->getData('product');
    }

    public function getCacheLifetime()
    {
        return null;
    }

    public function getLabelsHtml(): string
    {
        return $this->getChild('product.label')->setProduct($this->getProduct())->toHtml();
    }
}
