<?php
/**
 * @var $this Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'labels', [
    'label' => 'Label Prodotto',
    'type' => 'varchar',
    'input' => 'text',
    'group' => 'Onecashmere',
    'note'  =>  'Inserire valori separati da virgola',
    'required' => false,
    'backend' => 'onecashmere_catalog/product_attribute_backend_labels',
    'used_in_product_listing' => true,
]);
