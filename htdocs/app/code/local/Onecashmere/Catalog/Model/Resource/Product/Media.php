<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

class Onecashmere_Catalog_Model_Resource_Product_Media extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('catalog/product_attribute_media_gallery', 'value_id');
    }

    public function getAllImages(array $productIds = [])
    {
        $select = $this->getReadConnection()->select()->from(['m' => $this->getMainTable()], ['entity_id', 'value']);
        $select->joinInner(['v' => $this->getTable('catalog/product_attribute_media_gallery_value')], implode(' AND ', [
            'm.value_id = v.value_id',
            'v.store_id = 0',
        ]), []);
        if ($productIds) {
            $select->where('entity_id IN (?)', $productIds);
        }
        $select->order('v.position ASC');
        return $this->getReadConnection()->fetchAll($select);
    }
}
