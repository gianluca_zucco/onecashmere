<?php


class Onecashmere_Catalog_Model_Resource_CatalogInventory_Index extends Mage_CatalogInventory_Model_Resource_Stock_Item
{
    public function afterReindexProcessCataloginventoryStock()
    {
        $select = $this->_getReadAdapter()->select()->from(['p' => $this->getTable('catalog/product')], [
            'product_id' => 'entity_id',
            'is_in_stock' => new Zend_Db_Expr('SUM(i.qty) > 0'),
            'i.stock_id'
        ])
            ->joinInner(['l' => $this->getTable('catalog/product_super_link')], 'p.entity_id = l.parent_id', [])
            ->joinInner(['i' => $this->getTable('cataloginventory/stock_item')], 'l.product_id = i.product_id', [])
            ->where('p.type_id = ?', Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE)
            ->group('p.entity_id');

        $data = $this->_getReadAdapter()->fetchAll($select);
        $this->_getWriteAdapter()->insertOnDuplicate($this->getMainTable(), $data, ['is_in_stock']);
        $data = array_map(function(array $row) {
            return [
                'product_id' => $row['product_id'],
                'stock_id' => $row['stock_id'],
                'website_id' => 1,
                'stock_status' => $row['is_in_stock'],
            ];
        }, $data);
        $this->_getWriteAdapter()->insertOnDuplicate($this->getTable('cataloginventory/stock_status'), $data, ['stock_status']);
    }

    public function flagNeedReindex()
    {
        (new Onecashmere_Catalog_Model_Stock_Flag())->setState(1)->save();
    }
}
