<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

class Onecashmere_Catalog_Model_Resource_AmLegacy_OptionAddition extends Mage_Catalog_Model_Resource_Product
{
    public function getAllImages(): array
    {
        $select = $this->getReadConnection()->select()
            ->from(['e' => 'eav_attribute_option_value'], ['IFNULL(store_values.value, e.value) as label', 'e.option_id'])
            ->joinLeft(['store_values' => 'eav_attribute_option_value'], implode(' AND ', [
                'store_values.option_id = e.option_id',
                'store_values.store_id = ' . Mage::app()->getStore()->getId()
            ]), [])->where('e.store_id = 0');
        return $this->getReadConnection()->fetchPairs($select);
    }

    public function getAllImagesById(): array
    {
        $select = $this->getReadConnection()->select()->from(['e' => 'am_shopby_value'], ['option_id', 'img_small']);
        return $this->getReadConnection()->fetchPairs($select);
    }
}
