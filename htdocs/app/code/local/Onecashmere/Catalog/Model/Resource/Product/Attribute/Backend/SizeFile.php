<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */
use Mage_Core_Model_File_Uploader as Uploader;
use Mage_Eav_Model_Entity_Attribute_Backend_Abstract as Backend;
use Mage_Catalog_Model_Product as Product;

class Onecashmere_Catalog_Model_Resource_Product_Attribute_Backend_SizeFile extends Backend
{
    /**
     * @param Varien_Object|Mage_Catalog_Model_Product $object
     * @throws Exception
     */
    public function afterSave($object)
    {
        $value = $object->getData($this->getCode());

        if (is_array($value) && !empty($value['delete'])) {
            $object->setData($this->getCode(), '');
            $this->getAttribute()->getEntity()->saveAttribute($object, $this->getCode());
        } else {
            try {
                $uploader = $this->getUploader();
                $uploader->save($this->getPath($object));
                if (($fileName = $uploader->getUploadedFileName())) {
                    $object->setData($this->getCode(), $fileName);
                    $this->getAttribute()->getEntity()->saveAttribute($object, $this->getCode());
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addException($e, $e->getMessage());
            }
        }
    }

    /**
     * @return string
     */
    private function getCode(): string
    {
        return $this->getAttribute()->getAttributeCode();
    }

    /**
     * @return Uploader
     * @throws Exception
     */
    private function getUploader(): Uploader
    {
        $uploader = new Uploader("product[{$this->getCode()}]");
        $uploader->setAllowedExtensions(['csv', 'CSV']);
        $uploader->setAllowRenameFiles(false);
        $uploader->setFilesDispersion(false);
        $uploader->setAllowCreateFolders(true);
        return $uploader;
    }

    private function getPath(Product $product): string
    {
        return Mage::getBaseDir('media') . "/catalog/product/size_files/{$product->getId()}";
    }
}
