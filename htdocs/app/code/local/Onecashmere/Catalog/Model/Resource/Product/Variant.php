<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

use Mage_Catalog_Model_Resource_Product_Collection as ProductCollection;

class Onecashmere_Catalog_Model_Resource_Product_Variant extends Mage_Core_Model_Resource_Db_Abstract
{
    /** @inheritDoc */
    protected function _construct()
    {
        $this->_init('catalog/product_super_link', 'link_id');
    }

    public function getColorVariantCount(array $ids): array
    {
        if ($ids) {
            $select = $this->_getReadAdapter()->select()
                ->from(['s' => $this->getMainTable()], ['parent_id', 'product_id', new Zend_Db_Expr('1 as amount')])
                ->joinInner(['c' => $this->getTable(['catalog/product', 'int'])], implode(' AND ', [
                    'c.entity_id = s.product_id',
                    'c.store_id = 0',
                    'c.attribute_id = 92'
                ]), ['value'])
                ->where('parent_id IN (?)', $ids)
                ->group('c.value')
                ->group('s.parent_id');

            $superSelect = $this->getReadConnection()->select()
                ->from(['sub' => new Zend_Db_Expr("({$select})")], ['parent_id', new Zend_Db_Expr('SUM(amount)')])
                ->group('parent_id');

            return $this->getReadConnection()->fetchPairs($superSelect);
        }
        return [];
    }
}
