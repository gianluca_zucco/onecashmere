<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

use Varien_Event_Observer as Event;
use Mage_Catalog_Model_Resource_Product_Collection as ProductCollection;
use Mage_Catalog_Model_Category as Category;
use Mage_Catalog_Model_Product as Product;
use Mage_Sales_Model_Quote_Item as QuoteItem;

class Onecashmere_Catalog_Model_Observer
{
    /** @var QuoteItem */
    private $lastAddedQuoteItem;

    /** @var Product */
    private $lastAddedProduct;

    /**
     * Listen to
     * - catalog_product_collection_load_after
     *
     * @param Varien_Event_Observer $event
     * @throws Varien_Exception
     */
    public function bindReviewSummary(Event $event)
    {
        /** @var ProductCollection $collection */
        $collection = $event->getCollection();
        Mage::getModel('review/review')->appendSummary($collection);
    }

    public function addKidProductLayoutUpdate(Event $observer)
    {
        $category = Mage::registry('current_category');
        /** @var Mage_Core_Model_Layout $layout */
        $layout = $observer->getData('layout');
        /** @var Mage_Core_Controller_Front_Action $action */
        $action = $observer->getData('action');
        $path = $category instanceof Category ? $category->getUrlPath() : '';
        $isKidUrl = strpos($path, 'bambini') !== false || strpos($path, 'children') !== false;
        if ($action->getFullActionName() === 'catalog_product_view' && $isKidUrl) {
            $layout->getUpdate()->addHandle('PRODUCT_TYPE_CHILDREN');
        }
    }

    public function filterInStockProduct(Event $event)
    {
        try {
            /** @var ProductCollection $collection */
            $collection = $event->getData('collection');
            Mage::getModel('cataloginventory/stock')->addInStockFilterToCollection($collection);
        } catch (Exception $e) { //intentionally void
        }
    }

    public function setLastAddedQuoteItem(Event $event)
    {
        $this->lastAddedQuoteItem = $event->getData('quote_item');
        $this->lastAddedProduct = $event->getData('product');
    }

    public function setQuoteItemCustomPrice()
    {
        $quoteItem = $this->lastAddedQuoteItem ?? new QuoteItem();
        if ($quoteItem->getParentItemId() && $this->lastAddedProduct) {
            $price = $this->lastAddedProduct->getFinalPrice($quoteItem->getQty());
            $quoteItem->setCustomPrice($price);
            $quoteItem->setOriginalCustomPrice($price);
            $quoteItem->getProduct()->setIsSuperMode(true);
        }
    }

    public function addColorVariantsCount(Event $event)
    {
        /** @var ProductCollection $collection */
        $collection = $event->getData('collection');
        if ($collection->isLoaded()) {
            $variantCount = Mage::getResourceModel('onecashmere_catalog/product_variant')->getColorVariantCount(array_keys($collection->getItems()));
            foreach ($variantCount as $productId => $count) {
                $collection->getItemById($productId)->setData('variant_count', $count);
            }
        }
    }
}
