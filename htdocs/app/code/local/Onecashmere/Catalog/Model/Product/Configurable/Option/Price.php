<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

use Mage_Catalog_Model_Product as Product;

class Onecashmere_Catalog_Model_Product_Configurable_Option_Price
{
    /** @var Mage_Catalog_Model_Product */
    private $product;

    /** @var Mage_Tax_Helper_Data */
    private $taxHelper;

    public function __construct(Product $product)
    {
        $this->product = $product;
        $this->taxHelper = Mage::helper('tax');
    }

    public function toArray()
    {
        $price = (float) $this->product->getPrice();
        $specialPrice = $this->product->getFinalPrice() < $price ? $this->product->getFinalPrice() : 0;
        return [
            'price' => $this->toPrice($price, true),
            'specialPrice' => $this->toPrice($specialPrice, true),
            'priceExcludingTax' => $this->toPrice($price, false),
            'specialPriceExcludingTax' => $this->toPrice($specialPrice, false),
            'showBothPrices' => $this->taxHelper->displayBothPrices(),
            'excludingTaxLabel' => $this->taxHelper->__('Excl. Tax:')
        ];
    }

    private function toPrice(float $price, bool $includingTax = true): float
    {
        return $this->taxHelper->getPrice($this->product, $price, $includingTax);
    }
}
