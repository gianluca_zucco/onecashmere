<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

class Onecashmere_Catalog_Model_Product_Attribute_Backend_Labels extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    /**
     * @param Mage_Catalog_Model_Product $object
     * @return $this
     */
    public function beforeSave($object)
    {
        $object->setData($this->getCode(), $this->serialize($object));
        return $this;
    }

    private function getCode(): string
    {
        return $this->getAttribute()->getAttributeCode();
    }

    private function serialize(Mage_Catalog_Model_Product $object): string
    {
        return implode(',', array_filter(array_map('trim', (array) $object->getData($this->getCode()))));
    }
}
