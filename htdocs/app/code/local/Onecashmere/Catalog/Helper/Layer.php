<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

class Onecashmere_Catalog_Helper_Layer extends Mage_Core_Helper_Abstract
{
    /** @var string[] */
    private $amShopByValues = [];

    public function __construct()
    {
        $this->amShopByValues = $this->initAmShopByValues();
    }

    public function getLayerFilterImage(string $label): string
    {
        return $this->amShopByValues[$label] ?? '';
    }

    private function initAmShopByValues(): array
    {
        $resource = Mage::getResourceModel('onecashmere_catalog/amLegacy_optionAddition');
        return array_map([$this, 'toValue'], $resource->getAllImages());
    }

    private function toValue(string $optionId): string
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . "small_light(dw=28,progressive=y)/media/amconf/images/$optionId.jpg";
    }
}
