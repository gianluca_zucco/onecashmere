<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

use Mage_Catalog_Model_Product as Product;
use Onecashmere_Catalog_Model_Product_Configurable_Option_Price as OptionPrice;

class Onecashmere_Catalog_Helper_Configurable extends Mage_Core_Helper_Abstract
{
    /** @var int[] */
    private $productIds;

    /** @var Product[] */
    private $items;

    public function extendConfigurableJson(string $configuration)
    {
        $decoded = Mage::helper('core')->jsonDecode($configuration);
        $images = Mage::getResourceSingleton('onecashmere_catalog/amLegacy_optionAddition')->getAllImagesById();
        foreach ($decoded['attributes'] as $attributeId => $options) {
            $options = $options['options'];
            array_walk($options, [$this, 'extendOption'], $images);
            $decoded['attributes'][$attributeId]['options'] = $options;
        }
        $decoded = $this->addMedia($decoded);
        $decoded = $this->addPrices($decoded);
        $decoded = $this->addAttributes($decoded);
        return Mage::helper('core')->jsonEncode($decoded);
    }

    private function addAttributes(array $configuration) {

        $configuration['productAttributes'] = array_reduce($this->getItems($configuration), [$this, 'toAttributeSet'], []);
        return $configuration;
    }

    private function extendOption(array &$option, int $index, array $images)
    {
        $baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $mediaPath = Mage::getBaseDir('media');
        $image = $images[$option['id']] ?? '';
        if (is_file("{$mediaPath}/amconf/images/{$option['id']}.jpg")) {
            $option['media_url'] = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . "small_light(dw=35,progressive=y)/media/amconf/images/{$option['id']}.jpg";
        } else {
            $option['media_url'] = "{$baseUrl}amshopby/$image";
        }
    }

    private function addPrices(array $configuration): array
    {
        $configuration['prices'] = array_reduce($this->getItems($configuration), [$this, 'toPrice'], []);
        return $configuration;
    }

    private function addMedia(array $configuration): array
    {
        $product = Mage::registry('current_product') ?? Mage::getModel('catalog/product')->load($configuration['productId']);
        $products = $this->getProductIds($configuration);
        $images = Mage::getResourceSingleton('onecashmere_catalog/product_media')->getAllImages($products);
        $configuration['images'] = array_reduce($images, function(array $images, array $current) use ($product)  {
            $images[$current['entity_id']][] = $this->toImage($product, $current['value'], 500);
            return $images;
        }, []);
        return $configuration;
    }

    private function toPrice(array $prices, Product $product): array
    {
        $prices[$product->getEntityId()] = (new OptionPrice($product))->toArray();
        return $prices;
    }

    private function toAttributeSet(array $attributes, Product $product): array
    {
        $attributes[$product->getEntityId()] = [
            92 => $product->getData('color'),
            155 => $product->getData('size'),
            'productId' => $product->getEntityId(),
        ];
        return $attributes;
    }

    private function toImage(Product $product, string $file, $resize): array
    {
        return [
            'source' => [
                [
                    'media' => '(max-width: 1024px)',
                    'srcset' => "{$this->toUrl($product, 'thumbnail', $resize / 2, $file)} 1x, {$this->toUrl($product, 'thumbnail', $resize, $file)} 2x",
                ],
                [
                    'media' => '(min-width: 1024px)',
                    'srcset' => "{$this->toUrl($product, 'thumbnail', $resize, $file)} 1x, {$this->toUrl($product, 'thumbnail', $resize * 2, $file)} 2x",
                ]
            ],
            'zoomUrl' => $this->toUrl($product, 'thumbnail', 0, $file),
        ];
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param string $image
     * @param int $width
     * @param string|null $file
     * @return string
     */
    public function toUrl(Product $product, string $image, int $width = 0, string $file = null): string
    {
        $image = (new Onecashmere_Catalog_Helper_Image())->init($product, $image, $file);
        return (string) ($width ? $image->resize($width) : $image);
    }

    /**
     * @param array $configuration
     * @return int[]
     */
    private function getProductIds(array $configuration)
    {
        if (!isset($this->productIds)) {
            $products = [$configuration['productId']];
            foreach ($configuration['attributes'] as $attributeId => $attribute) {
                $products += array_reduce($attribute['options'], function (array $return, array $option) {
                    return array_merge($return, $option['products']);
                }, $products);
            }
            $this->productIds = array_unique($products);
        }
        return $this->productIds;
    }

    /**
     * @param array $configuration
     * @return array
     */
    private function getItems(array $configuration): array
    {
        if (!isset($this->items)) {
            $this->items = Mage::getResourceSingleton('catalog/product_collection')
                ->addFinalPrice()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('entity_id', ['in' => $this->getProductIds($configuration)])
                ->getItems();
        }
        return $this->items;
    }
}
