<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */
use Mage_Catalog_Model_Product as Product;
class Onecashmere_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getSizeCartCmsBlockId(): string
    {
        $data = $this->getRegistryProduct()->getData('size_chart_cms_block');
        return $data ? Mage::getModel('cms/block')->load($data)->getIdentifier() : '';
    }

    public function getSizeChartCmsBlockDescriptionId(): string
    {
        $data = $this->getRegistryProduct()->getData('size_chart_cms_block_desc');
        return $data ? Mage::getModel('cms/block')->load($data)->getIdentifier() : '';
    }

    private function getRegistryProduct(): Product
    {
        return Mage::registry('current_product');
    }

    public function hasSizeChart(Product $product): bool
    {
        return (bool) $product->getData('size_file');
    }

    public function getSizeTemplate(string $sizeChart, string $download): string
    {
        return $this->hasSizeChart($this->getRegistryProduct()) ? $sizeChart : $download;
    }
}
