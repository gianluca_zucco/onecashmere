<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

use Mage_Catalog_Model_Product as Product;

class Onecashmere_Catalog_Helper_Image extends Mage_Catalog_Helper_Image
{
    public function init(Mage_Catalog_Model_Product $product, $attributeName, $imageFile = null)
    {
        try {
            return parent::init($product, $attributeName, $imageFile);
        } catch (Exception $e) {
            Mage::logException($e);
        } finally {
            return $this;
        }
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param string $image
     * @param int $width
     * @param string|null $file
     * @return string
     */
    public function toUrl(Product $product, string $image, int $width = 0, string $file = null): string
    {
        $image = $this->init($product, $image, $file)->setQuality(100);
        return (string) ($width ? $image->resize($width) : $image);
    }

    public function getGalleryConfiguration(Product $product, array $pictures, $resize = 0): string
    {
        $mapped = array_values(array_map($this->toImage($product, $resize), $pictures));
        return Mage::helper('core')->jsonEncode($mapped);
    }

    private function toImage(Product $product, $resize): Closure
    {
        return function (Varien_Object $picture) use ($product, $resize) {
            return [
                'source' => [
                    [
                        'media' => '(max-width: 399px)',
                        'srcset' => "{$this->toUrl($product, 'thumbnail', 220, $picture->getFile())} 1x, {$this->toUrl($product, 'thumbnail', 640, $picture->getFile())} 2x",
                    ],
                    [
                        'media' => '(max-width: 767px)',
                        'srcset' => "{$this->toUrl($product, 'thumbnail', 230, $picture->getFile())} 1x, {$this->toUrl($product, 'thumbnail', 560, $picture->getFile())} 2x",
                    ],
                    [
                        'media' => '',
                        'srcset' => "{$this->toUrl($product, 'thumbnail', $resize, $picture->getFile())} 1x, {$this->toUrl($product, 'thumbnail', $resize * 2, $picture->getFile())} 2x",
                    ]
                ],
                'zoomUrl' => $this->toUrl($product, 'thumbnail', 0, $picture->getFile()),
            ];
        };
    }

    public function __toString()
    {
        try {
            $model = $this->_getModel();

            if ($this->getImageFile()) {
                $model->setBaseFile($this->getImageFile());
            } else {
                $model->setBaseFile($this->getProduct()->getData($model->getDestinationSubdir()));
            }

            return $this->generateMediaUrl($model);
        } catch (Exception $e) {
            return Mage::getDesign()->getSkinUrl($this->getPlaceholder());
        }
    }

    private function getSmallLightConfiguration(Mage_Catalog_Model_Product_Image $image): string
    {
        $params = ["dw={$image->getWidth()}"];

        if ($image->getHeight()) {
            $params[] = "dh={$image->getHeight()}";
            $params[] = "da=n"; //No preserve ratio
        }

        if (strpos($image->getBaseFile(), '.jp') !== -1) {
            $params[]="progressive=y";
        }

        return implode(',', $params);
    }

    /**
     * @param Mage_Catalog_Model_Product_Image $model
     * @return string
     * @throws Exception
     */
    private function generateMediaUrl(Mage_Catalog_Model_Product_Image $model)
    {
        $url = [Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)];
        if ($this->_scheduleResize) {
            $url[] = "small_light({$this->getSmallLightConfiguration($model)})";
        }
        $url[] = $model->getBaseFile();
        $url = array_map(function(string $value) {
            if (strpos($value, Mage::getBaseDir()) === 0) {
                $value = str_replace(Mage::getBaseDir(), '', $value);
            }
            return trim($value, ...['/']);
        }, $url);
        return implode('/', $url);
    }
}
