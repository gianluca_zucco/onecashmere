<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

class Onecashmere_Review_Model_System_Config_Source_Widget_Review_Carousel_Sort
{
    const BY_DATE = 1;
    const RANDOM = 2;
    protected $_options;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        if (!$this->_options) {
            $this->_options = [
                [
                    'label' => Mage::helper('onecashmere_review')->__('By Date (Newest to Oldest)'),
                    'value' => self::BY_DATE
                ],
                [
                    'label' => Mage::helper('onecashmere_review')->__('Random'),
                    'value' => self::RANDOM
                ]
            ];
        }
        return $this->_options;
    }
}
