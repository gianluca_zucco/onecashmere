<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

use Mage_Review_Model_Review as Review;
use Mage_Catalog_Model_Product as Product;
use Mage_Rating_Model_Resource_Rating_Option_Vote_Collection as RatingCollection;

class Onecashmere_Review_Model_Review
{
    /** @var Review */
    private $review;

    /** @var Mage_Catalog_Model_Product */
    private $product;

    public function __construct(Review $review, Product $product)
    {
        $this->review = $review;
        $this->product = $product;
    }

    /**
     * @return Mage_Rating_Model_Rating[]
     */
    public function getRatingVotes(): array
    {
        $votes = $this->review->getRatingVotes();
        return $votes instanceof RatingCollection ? $votes->getItems() : [];
    }

    public function getRatingVotesAggregated(): float
    {
        $percent = array_sum(array_map(function(Mage_Rating_Model_Rating_Option_Vote $rating) {
            return $rating->getData('percent');
        }, $this->getRatingVotes()));
        return ceil($percent / max(count($this->getRatingVotes()), 1));
    }

    /**
     * @return Review
     */
    public function getReview(): Review
    {
        return $this->review;
    }

    public function getTitle(): string
    {
        return (string) $this->review->getTitle();
    }

    public function getNickname(): string
    {
        return (string) $this->review->getNickname();
    }

    public function getDetail(): string
    {
        return (string) $this->review->getDetail();
    }
}
