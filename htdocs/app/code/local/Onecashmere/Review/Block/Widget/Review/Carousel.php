<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */
use Onecashmere_Review_Model_System_Config_Source_Widget_Review_Carousel_Sort as Sorting;
use Mage_Review_Model_Resource_Review_Collection as ReviewCollection;
use Mage_Catalog_Model_Product as Product;
use Mage_Review_Model_Review as Review;

class Onecashmere_Review_Block_Widget_Review_Carousel extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    const DEFAULT_LIMIT = 4;
    /** @var  ReviewCollection */
    protected $_collection;

    /**
     * @return ReviewCollection
     */
    public function getCollection(): ReviewCollection
    {
        if (!isset($this->_collection)) {
            $collection = Mage::getResourceModel('review/review_collection')
                ->addStoreFilter(Mage::app()->getStore()->getId())
                ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED);
            $collection->setPageSize($this->getLimit());

            switch ($this->getSortBy()) {
                case Sorting::RANDOM:
                    $collection->getSelect()->order(new Zend_Db_Expr('RAND()'));
                break;
                case Sorting::BY_DATE:
                default:
                    $collection->setOrder('created_at');
                break;
            }

            $this->_collection = $collection->load();
            $_productCollection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductCollectionAttributes())
                ->addFieldToFilter('entity_id', ['in' => $collection->getColumnValues('entity_pk_value')]);

            $collection->walk(function(Review $review) use ($_productCollection) {
                $review->setProduct($_productCollection->getItemById($review->getEntityPkValue()) ?? new Product());
            });
        }
        return $this->_collection;
    }
    
    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->getData('limit') ? (int) $this->getData('limit') : self::DEFAULT_LIMIT;
    }

    /**
     * @return int
     */
    public function getSortBy(): int
    {
        return $this->getData('sort_by');
    }
}
