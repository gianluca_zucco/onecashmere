<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

class Onecashmere_Review_Block_Helper extends Mage_Review_Block_Helper
{
    public function getReviewsUrl()
    {
        return "{$this->getProduct()->getProductUrl()}#reviews";
    }
}
