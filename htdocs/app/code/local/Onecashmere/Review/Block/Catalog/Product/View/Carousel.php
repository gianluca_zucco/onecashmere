<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

use Mage_Review_Model_Resource_Review_Collection as ReviewCollection;
use Mage_Review_Model_Review as Review;

class Onecashmere_Review_Block_Catalog_Product_View_Carousel extends Mage_Catalog_Block_Product_View_Abstract
{
    public function getCollection(): ReviewCollection
    {
        if (!isset($this->_collection)) {
            $collection = Mage::getResourceModel('review/review_collection')
                ->addFieldToFilter('entity_pk_value', $this->getProduct()->getId())
                ->addStoreFilter(Mage::app()->getStore()->getId())
                ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED);
            $collection->setOrder('created_at');
            $collection->addRateVotes();
            $this->_collection = $collection->load();
            $collection->walk(function (Review $review) {
                $review->setProduct($this->getProduct());
            });
        }
        return $this->_collection;
    }
}
