<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

use Mage_Catalog_Model_Product as Product;

class Onecashmere_Review_Block_Catalog_Product_View_Form extends Mage_Review_Block_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->unsetData('template');
    }

    public function getProductInfo()
    {
        return Mage::registry('current_product') ?? new Product();
    }

    public function getAction()
    {
        return $this->getUrl('review/product/post', ['id' => $this->getProductInfo()->getId()]);
    }
}
