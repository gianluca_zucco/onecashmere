<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */
use Mage_Sales_Model_Order as Order;

class Onecashmere_Sales_Adminhtml_Onecashmere_Sales_Order_EmailController extends Mage_Adminhtml_Controller_Action
{
    public function sendWarehouseAction()
    {
        try {
            Mage::getResourceModel('sales/order_collection')
                ->addFieldToFilter('entity_id', ['in' => $this->getRequest()->getParam('order_ids', [])])
                ->walk(function (Order $order) {
                    Mage::getModel('onecashmere_sales/warehouse_order')->sendEmail($order);
                });
            $this->_getSession()->addSuccess(Mage::helper('onecashmere_sales')->__('Warehouse email were sent'));
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $e->getMessage());
        }
        $this->_redirectReferer();
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view');
    }

    /**
     * @return Mage_Adminhtml_Model_Session|Mage_Core_Model_Abstract
     */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }
}
