<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

class Onecashmere_Sales_Adminhtml_Onecashmere_Sales_OrderController extends Mage_Adminhtml_Controller_Action
{
    public function printMultiplePreviewAction()
    {
        try {
            $this->loadLayout(false);
            $this->renderLayout();
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $e->getMessage());
            $this->_redirectReferer();
        }
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view');
    }
}
