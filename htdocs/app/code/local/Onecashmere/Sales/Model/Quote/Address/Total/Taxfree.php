<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

use Mage_Sales_Model_Quote_Address as Address;

class Onecashmere_Sales_Model_Quote_Address_Total_Taxfree extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    protected $_code = 'extra_ue_tax_free';

    /**
     * @param Address $address
     * @return $this
     */
    public function collect(Address $address)
    {
        if ($this->_isValidAddress($address)) {
            $address->addTotalAmount($this->getCode(), -$address->getTaxAmount());
            $address->addBaseTotalAmount($this->getCode(), -$address->getBaseTaxAmount());
            $address->setGrandTotal($address->getGrandTotal() - $address->getTaxAmount());
            $address->setBaseGrandTotal($address->getBaseGrandTotal() - $address->getBaseTaxAmount());
        }
        return $this;
    }

    /**
     * @param Address $address
     * @return $this
     */
    public function fetch(Address $address)
    {
        if ($address->getTotalAmount($this->getCode())) {
            $address->addTotal([
                'code' => $this->getCode(),
                'title' => Mage::helper('onecashmere_sales')->__('Extra UE Tax Free'),
                'full_info' => [],
                'value' => $address->getTotalAmount($this->getCode())
            ]);
        }
        return $this;
    }

    protected function _isValidAddress(Address $address): bool
    {
        return !in_array($address->getCountryId(), explode(',', Mage::getStoreConfig('general/country/ue')))
            && $address->getSubtotalWithDiscount() >= Mage::getStoreConfig('tax/discount/minimal_subtotal')
            && Mage::getStoreConfigFlag('onecashmere/extra_ue_tax_free/active');
    }


}
