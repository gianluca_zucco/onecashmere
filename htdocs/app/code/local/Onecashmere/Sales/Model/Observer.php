<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */
use Varien_Event_Observer as Event;
use Mage_Adminhtml_Block_Widget_Grid_Massaction as Massaction;

class Onecashmere_Sales_Model_Observer
{
    /**
     * Listen to
     * - adminhtml_block_html_before
     * @param Varien_Event_Observer $observer
     */
    public function addCustomActions(Event $observer)
    {
        if ($this->isOrderGrid() && (($block = $observer->getData('block')) instanceof Massaction)) {
            /** @var $block Massaction */
            $block->addItem('print_warehouse_order', [
                'label' => Mage::helper('onecashmere_sales')->__('Send executive email'),
                'url' => $block->getUrl('*/onecashmere_sales_order_email/sendwarehouse'),
            ]);
            $block->addItem('print_multiple_preview_orders', [
                'label' => Mage::helper('onecashmere_sales')->__('Print Order Preview'),
                'url' => $block->getUrl('*/onecashmere_sales_order/printMultiplePreview'),
            ]);
        }
    }

    private function getLayoutHandles(): array
    {
        return Mage::getSingleton('core/layout')->getUpdate()->getHandles();
    }

    private function isOrderGrid(): bool
    {
        return in_array('adminhtml_sales_order_index', $this->getLayoutHandles())
            || in_array('adminhtml_sales_order_grid', $this->getLayoutHandles());
    }
}
