<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

use Mage_Sales_Model_Order as Order;

class Onecashmere_Sales_Model_Warehouse_Order extends Varien_Object
{
    const XML_PATH_EMAIL_TEMPLATE = 'sales_email/warehouse_order/template';
    const XML_PATH_EMAIL_EMAIL = 'trans_email/ident_sales/email';
    const XML_PATH_EMAIL_IDENTITY = 'trans_email/ident_sales/name';

    /**
     * Sends out email
     * @param Mage_Sales_Model_Order $order
     */
    public function sendEmail(Order $order)
    {

        /* @var $emailModel Mage_Core_Model_Email_Template */
        $templateVars = [
            'order' => $order,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $order->getPayment()->getMethodInstance()->getConfigData('title')
        ];

        Mage::getModel('core/email_template')->sendTransactional(
            Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
            'general',
            Mage::getStoreConfig(self::XML_PATH_EMAIL_EMAIL),
            Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY),
            $templateVars,
            Mage_Core_Model_App::ADMIN_STORE_ID);
    }
}
