<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 * @method setOrder(Mage_Sales_Model_Order $order)
 * @method Mage_Sales_Model_Order|null getOrder()
 */

class Onecashmere_Sales_Block_Adminhtml_Order_Preview_List_Item extends Mage_Core_Block_Template
{
    protected $_template = 'onecashmere/sales/order/preview/list/item.phtml';
}
