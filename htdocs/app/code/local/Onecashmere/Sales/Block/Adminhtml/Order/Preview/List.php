<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project onecashmere
 */

use Dompdf\Dompdf;
use Mage_Sales_Model_Order as Order;

class Onecashmere_Sales_Block_Adminhtml_Order_Preview_List extends Mage_Core_Block_Text_List
{
    /**
     * @return Mage_Core_Block_Abstract|void
     * @throws Exception
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        foreach ($this->getOrders() as $order) {
            /** @var Onecashmere_Sales_Block_Adminhtml_Order_Preview_List_Item $renderer */
            $renderer = $this->getLayout()->createBlock('onecashmere_sales/adminhtml_order_preview_list_item');
            $renderer->setOrder($order);
            $this->append($renderer);
        }
    }

    /**
     * @return Order[]
     * @throws Exception
     */
    private function getOrders(): array
    {
        if (!($ids = $this->getRequest()->getParam('order_ids'))) {
            throw new \Exception('Order Ids are not set');
        }
        return Mage::getResourceModel('sales/order_collection')->addFieldToFilter('entity_id',
            ['in' => $ids])->getItems();
    }

    protected function _toHtml()
    {
        $pdf = new Dompdf();

        $pdf->loadHtml(parent::_toHtml());

        $pdf->setPaper('A4', 'landscape');

        $pdf->render();

        $pdf->stream();
    }
}
