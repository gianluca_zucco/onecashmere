<?php
/**
 * Customer account navigation sidebar
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Customer_Block_Account_Navigation extends Mage_Core_Block_Template
{

    protected $_links = array();

    protected $_activeLink = false;

    public function addLink($name, $path, $label, $urlParams = [])
    {
        $this->_links[$name] = new Varien_Object([
            'name' => $name,
            'path' => $path,
            'label' => $label,
            'url' => $this->getUrl($path, $urlParams),
        ]);
        return $this;
    }

    public function removeLink(string $key)
    {
        unset($this->_links[$key]);
        return $this;
    }

    public function removeLinks(array $keys)
    {
        array_walk($keys, function($key) {
            unset($this->_links[$key]);
        });
        return $this;
    }

    public function setActive($path)
    {
        $this->_activeLink = $this->_completePath($path);
        return $this;
    }

    public function getLinks()
    {
        return $this->_links;
    }

    public function isActive($link)
    {
        if (empty($this->_activeLink)) {
            $this->_activeLink = $this->getAction()->getFullActionName('/');
        }
        return $this->_completePath($link->getPath()) == $this->_activeLink;
    }

    protected function _completePath($path)
    {
        $path = rtrim($path, '/');
        switch (sizeof(explode('/', $path))) {
            case 1:
                $path .= '/index';
                // no break

            case 2:
                $path .= '/index';
        }
        return $path;
    }

}
