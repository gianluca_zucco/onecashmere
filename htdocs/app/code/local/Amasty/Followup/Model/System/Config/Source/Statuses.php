<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */

class Amasty_Followup_Model_System_Config_Source_Statuses
{
    protected $_options;
        
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options = Mage::getResourceModel('sales/order_status_collection')
            ->toOptionArray();
        }
        return $this->_options;
    }
}
