<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */

    class Amasty_Followup_Model_Event_Cancel_Customer_Loggedin extends Amasty_Followup_Model_Event_Basic
    {
        function validate($history){
            $customer = Mage::getModel('customer/customer')->load($history->getCustomerId());
            $logCustomer = Mage::getModel('log/customer')->loadByCustomer($customer);
            
            return strtotime($logCustomer->getLoginAt()) > strtotime($history->getCreatedAt());
            
        }
    }
?>