<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */

    class Amasty_Followup_Model_Event_Customer_Wishlist extends Amasty_Followup_Model_Event_Basic
    {
        function validate($customer){
            $wishlist = Mage::getModel("wishlist/wishlist")->loadByCustomer($customer);
            
            return $wishlist->getItemsCount() > 0 &&
                    $this->_validateBasic($customer->getStoreId(), $customer->getEmail(), $customer->getGroupId());
        }
        
        protected function _initCollection(){
        
            $resource = Mage::getSingleton('core/resource');
            
            $collection = Mage::getModel('customer/customer')->getCollection();
            
            $collection->addNameToSelect();
            
            $collection->getSelect()->joinInner(
                array('wishlist' => $resource->getTableName('wishlist/wishlist')), 
                'e.entity_id = wishlist.customer_id',
                array()
            );

            $collection->getSelect()->where("wishlist.updated_at > '" . $this->date($this->getLastExecuted()) . "'");
            $collection->getSelect()->where("wishlist.updated_at < '" . $this->date($this->getCurrentExecution()) . "'");
            $collection->getSelect()->group("e.entity_id");

            return $collection;
        }
    }
?>