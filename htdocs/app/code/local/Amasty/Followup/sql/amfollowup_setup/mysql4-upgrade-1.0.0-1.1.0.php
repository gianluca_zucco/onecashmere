<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */

function createTemplate($templateCode, $templateLabel){
    

    $locale = 'en_US';

    $template = Mage::getModel('adminhtml/email_template');

    $template->loadDefault($templateCode, $locale);
    $template->setData('orig_template_code', $templateCode);
    $template->setData('template_variables', Zend_Json::encode($template->getVariablesOptionArray(true)));

    $template->setData('template_code', $templateLabel);

    $template->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

    $template->setId(NULL);

    $template->save();
}

createTemplate('amfollowup_order_new', 'Amasty Follow Up Email: Order Created');
createTemplate('amfollowup_order_ship', 'Amasty Follow Up Email: Order Shipped');
createTemplate('amfollowup_order_invoice', 'Amasty Follow Up Email: Order Invoiced');
createTemplate('amfollowup_order_complete', 'Amasty Follow Up Email: Order Completed');
createTemplate('amfollowup_order_cancel', 'Amasty Follow Up Email: Order Cancelled');
createTemplate('amfollowup_customer_group', 'Amasty Follow Up Email: Customer Changed Group');
createTemplate('amfollowup_customer_birthday', 'Amasty Follow Up Email: Customer Birthday');
createTemplate('amfollowup_customer_new', 'Amasty Follow Up Email: Customer Registration');
createTemplate('amfollowup_customer_subscription', 'Amasty Follow Up Email: Customer Subscribed to Newsletter');
createTemplate('amfollowup_customer_activity', 'Amasty Follow Up Email: Customer No Activity');
createTemplate('amfollowup_customer_wishlist', 'Amasty Follow Up Email: Customer Wish List Product Added');
createTemplate('amfollowup_customer_wishlist_shared', 'Amasty Follow Up Email: Customer Wish List Shared');

?>