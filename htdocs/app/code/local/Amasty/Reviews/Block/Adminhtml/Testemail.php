<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
 

class Amasty_Reviews_Block_Adminhtml_Testemail extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /*
     *  Set template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('amasty/amreviews/system/config/button.phtml');
    }
 
    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->_toHtml();
    }
 
    /**
     * Return ajax url for button
     *
     * @return string
     */
    public function getAjaxCheckUrl()
    {
        return Mage::helper('adminhtml')->getUrl('amasty/amreviews/adminhtml_ajax/sendTestEmail');
    }
 
    /**
     * Generate button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
            'id'        => 'amreviews_button',
            'label'     => $this->helper('adminhtml')->__('Send Test Email'),
            'onclick'   => 'send_test_email();'
        ));
 
        return $button->toHtml();
    }

}