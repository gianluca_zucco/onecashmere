<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */   
class Amasty_Reviews_Block_Adminhtml_History extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_history';
        $this->_blockGroup = 'amreviews';
        $this->_headerText = Mage::helper('amreviews')->__('Sent Emails');
        parent::__construct();
		$this->_removeButton('add'); 
    }
}