<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Block_Adminhtml_History_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('historyGrid');
      $this->setDefaultSort('invite_id','DESC');
  }

    public function getRowUrl($row)
    {
        return null;
    }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('amreviews/invite')->getCollection();
      $collection->addFieldToFilter('sent', 1);

      $this->setCollection($collection);
      parent::_prepareCollection();

      foreach ($collection as &$item){
          if (strtotime($item->getVisitedAt())<0){
              $item->unsVisitedAt();
          }
      }

      return $this;
  }

  protected function _prepareColumns()
  {
      $hlp = Mage::helper('amreviews');
    $this->addColumn('invite_id', array(
      'header'    => $hlp->__('ID'),
      'align'     => 'right',
      'width'     => '50px',
      'index'     => 'invite_id',
    ));

      $this->addColumn('increment_id', array(
          'header' => $hlp->__('Order #'),
      'align'     => 'right',
      'width'     => '50px',
      'index'     => 'increment_id',
    ));

	$this->addColumn('email', array(
        'header'    => $hlp->__('Email'),
        'index'     => 'email',
    ));

    $this->addColumn('name', array(
        'header'    => $hlp->__('Customer Name'),
        'index'     => 'name',
    ));


	$this->addColumn('sent_at', array(
		'header'    => $hlp->__('Sent At'),
		'index'     => 'sent_at',
        'type' => 'datetime',
		'width'     => '150px',
		'gmtoffset' => true,
		'default'	=> ' ---- ',
	));

      $this->addColumn('visited_at', array(
		'header'    => $hlp->__('Visited At'),
		'index'     => 'visited_at',
        'type' => 'datetime',
		'width'     => '150px',
		'gmtoffset' => true,
		'default'	=> ' ---- ',
      )
      );

    return parent::_prepareColumns();
  }
  

}