<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Block_Adminhtml_Queue_Renderer_Products extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input
{
    public function render(Varien_Object $row)
    {
        $productNames =  Mage::registry('amreviews_queue_grid_data');
        $products = str_replace(array_keys($productNames),array_values($productNames),trim($row->getProductIds(),','));
        $products = '"'.str_replace(',','" , "',$products).'"';

        return $products;
    }
}