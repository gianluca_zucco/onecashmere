<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */   
class Amasty_Reviews_Block_Adminhtml_Queue extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_queue';
        $this->_blockGroup = 'amreviews';
        $this->_headerText     = Mage::helper('amreviews')->__('Pending Emails');
		$this->_addButtonLabel = Mage::helper('amreviews')->__('Load');  
        parent::__construct();
    }
}