<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Block_Adminhtml_Queue_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('queueGrid');
        $this->setDefaultSort('invite_id');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('amreviews/invite')->getCollection();
        $collection->addFieldToFilter('sent', 0);

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $hlp = Mage::helper('amreviews');
        $this->addColumn('invite_id', array(
            'header' => $hlp->__('ID'),
            'align'  => 'right',
            'width'  => '50px',
            'index'  => 'invite_id',
        )
        );

        $this->addColumn('increment_id', array(
                'header' => $hlp->__('Order #'),
            'align'  => 'right',
            'width'  => '50px',
            'index'  => 'increment_id',
        )
        );

        $this->addColumn('purchased_at', array(
            'header'    => $hlp->__('Purchased At'),
            'index'     => 'purchased_at',
            'type'      => 'datetime',
            'width'     => '150px',
            'gmtoffset' => true,
            'default'   => ' ---- ',
        )
        );

        $this->addColumn('email', array(
            'header' => $hlp->__('Email'),
            'index'  => 'email',
        )
        );

        $this->addColumn('name', array(
            'header' => $hlp->__('Customer Name'),
            'index'  => 'name',
        )
        );

        $this->addColumn('product', array(
            'header'   => $hlp->__('Products'),
            'index'    => 'product_ids',
            'renderer' => 'amreviews/adminhtml_queue_renderer_products',
        )
        );

        $this->addColumn('action', array(
            'header'    => Mage::helper('catalog')->__('Action'),
            'width'     => '50px',
            'type'      => 'action',
            'actions'   => array(
                array(
                    'caption' => Mage::helper('amreviews')->__('Send'),
                    'url'     => array('base' => 'amreviews/adminhtml_queue/send'),
                    'field'   => 'invite_id'
                )
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'invite_id',
            'is_system' => true,
        )
        );

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('invite_id');
        $this->getMassactionBlock()->setFormFieldName('ids');

        $actions = array(
            'massDelete' => 'Delete',
            'massSend' => 'Send',
        );
        foreach ($actions as $code => $label) {
            $this->getMassactionBlock()->addItem($code, array(
                'label'   => Mage::helper('amreviews')->__($label),
                'url'     => $this->getUrl('*/*/' . $code),
                'confirm' => ($code == 'massDelete' ? Mage::helper('amreviews')->__('Are you sure?') : null),
            )
            );
        }

        return $this;
    }

    protected function _afterLoadCollection()
    {
        $ids        = array();
        $collection = $this->getCollection();
        foreach ($collection as $item) {
            $ids = array_merge($ids, explode(',', trim($item->getProductIds(), ',')));
        }

        $productNames = array();
        $ids          = array_unique($ids);
        $products     = Mage::getModel('catalog/product')->getCollection()->addIdFilter($ids)->addAttributeToSelect('name');

        foreach ($products as $item) {
            $productNames[$item->getEntityId()] = $item->getName();
        }

        Mage::register('amreviews_queue_grid_data', $productNames);

        return $this;
    }
}