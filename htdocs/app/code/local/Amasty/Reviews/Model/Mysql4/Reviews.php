<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Model_Mysql4_Reviews extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('amreviews/reviews', 'id');
    }
}