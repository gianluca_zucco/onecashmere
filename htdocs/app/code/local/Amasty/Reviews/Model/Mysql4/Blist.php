<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Model_Mysql4_Blist extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        $this->_init('amreviews/blist', 'blist_id');
    }
	
	public function uploadAndImport($cfg)
	{
		if (empty($_FILES['groups']['tmp_name']['email']['fields']['file']['value'])) {
            return false;
        }

        $fileName = $_FILES['groups']['tmp_name']['email']['fields']['file']['value'];
        $ext = pathinfo($_FILES['groups']['name']['email']['fields']['file']['value'], PATHINFO_EXTENSION);
		if (!in_array($ext,array('txt','csv'))){
            Mage::getModel('adminhtml/session')->addWarning(__('Blacklist was not uploaded. File extension incorrect. Please, upload only CSV or TXT files, containing one email per line.'));
            return false;
        }


        ini_set('auto_detect_line_endings', 1); 

        $n = 0;
        $total = 0;
        $emails = @fopen($fileName, "r");
        $sql = 'INSERT IGNORE INTO `'. $this->getMainTable() .'` (email) VALUES ';
        if ($emails) {
            // read line by line and add to query
            while (($line = fgets($emails, 4096)) !== false) {
                $line = str_replace(array("\r","\n","\t",'"', "'", ',', ';', ' '), '', $line);
                if ($line){
                    $sql .= "('$line'),";
                    $n++;
                }
                // if query is too long - commit this part and open new query
                if ($n>5000) {
                    $n   = 0;
                    $this->_getWriteAdapter()->raw_query(substr($sql, 0, -1));
                    $sql = 'INSERT IGNORE INTO `'. $this->getMainTable() .'` (email) VALUES ';
                }
                $total++;
            }
            if (!feof($emails)) {
                echo "Error: unexpected fgets() fail\n";
            }

            // final commit
            $this->_getWriteAdapter()->raw_query(substr($sql, 0, -1));
            fclose($emails);
        }

        Mage::getModel('adminhtml/session')->addNotice(__('Blacklist successfully imported. Added rows: ').$total);

		return true;
	}

    public function addEmail($email){
        $sql = 'INSERT IGNORE INTO `'. $this->getMainTable() .'` (email) VALUES (?)';
        return $this->_getWriteAdapter()->query($sql,trim($email));
    }
}