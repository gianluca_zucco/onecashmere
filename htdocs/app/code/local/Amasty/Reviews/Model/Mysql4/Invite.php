<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Model_Mysql4_Invite extends Mage_Core_Model_Mysql4_Abstract
{
    const ONCE_BY_CUSTOMER = 0;
    const ONCE_BY_PRODUCT  = 1;
    const ONCE_BY_ORDER    = 2;

    public function _construct()
    {
        $this->_init('amreviews/invite', 'invite_id');
    }

    public function massDelete($ids)
    {
        $db = $this->_getWriteAdapter();

        $ids[] = 0;
        $cond  = $db->quoteInto('invite_id IN(?)', $ids);
        $db->delete($this->getMainTable(), $cond);

        return true;
    }

    public function generate($dateFrom, $dateTo)
    {
        if (!$orders = $this->findOrders($dateFrom, $dateTo)) {
            return false;
        }
        $orders = $this->filterByBlacklist($orders);
        if (!$orders) return false;

        list($products, $orders_to_products) = $this->findProductsByOrders($orders);

        if (Mage::getStoreConfig('amreviews/conditions/product_category')) {
            $orders = $this->filterByProductCategory($orders, $orders_to_products);
            if (!$orders) return false;
        }

        if (intval(Mage::getStoreConfig('amreviews/frequency/once_by')) == self::ONCE_BY_CUSTOMER) {
            $orders = $this->filterByHistoryOrders($orders);
            $orders = $this->filterByHistoryEmails($orders);
        } elseif (intval(Mage::getStoreConfig('amreviews/frequency/once_by')) == self::ONCE_BY_PRODUCT) {
            list($orders, $orders_to_products) = $this->filterByProduct($products, $orders, $orders_to_products);
        } elseif (intval(Mage::getStoreConfig('amreviews/frequency/once_by')) == self::ONCE_BY_ORDER) {
            $orders = $this->filterByHistoryOrders($orders);
        }

        if (!$orders) return false;

        foreach ($orders as $k => &$item) {
            $item['order_id']    = $item['entity_id'];
            $item['product_ids'] = ',' . implode(',', array_keys($orders_to_products[$k])) . ',';
            $item['customer_id'] = isset($item['customer_id']) ? $item['customer_id'] : 0;
        }

        return $this->saveAll($orders);
    }

    private function findOrders($dateFrom, $dateTo)
    {
        $db         = $this->_getReadAdapter();
        $orders     = array();
        $orderStore = array();;

        $orderStatus = trim(Mage::getStoreConfig('amreviews/conditions/order_status'), ',');
        $orderStatus = str_replace(',', '","', $orderStatus);
        $orderStatus = '"' . $orderStatus . '"';

        $orderWebsites = explode(',', trim(Mage::getStoreConfig('amreviews/conditions/order_store')));
        if (is_array($orderWebsites) && !empty($orderWebsites)) {
            foreach ($orderWebsites as $websiteId) {
                if ($websiteId) {
                    $storeId    = Mage::app()->getWebsite($websiteId)->getStoreIds();
                    $orderStore = array_merge($orderStore, $storeId);
                }
            }
        }
        $orderStore = implode(',', array_unique($orderStore));
        if ($orderWebsites && !$orderStore) { // fix for empty websites
            $orderStore = '-1';
        }

        $sql = $db->select()
                  ->from(
                      array('o' => $this->getTable('sales/order')),
                      array(
                          'o.store_id', 'o.created_at', 'o.entity_id', 'o.increment_id', 'customer_id',
                          'CONCAT_WS(" ", o.customer_firstname, o.customer_middlename, o.customer_lastname) AS billing_name',
                          'o.customer_email as email'
                      )
                  )
                  ->where('   o.created_at >="' . $dateFrom . ' 00:00:00"
                    AND o.created_at <="' . $dateTo . ' 23:59:59"
                    AND o.status IN (' . $orderStatus . ')
                    ' . ($orderStore ? "AND o.store_id IN ($orderStore)" : "")
                  )
                  ->group('o.entity_id', 'o.customer_email')
                  ->limit(2000);

        if (intval(Mage::getStoreConfig('amreviews/conditions/min_order_subtotal')) > 0) {
            $sql->where('o.subtotal>= ' . intval(Mage::getStoreConfig('amreviews/conditions/min_order_subtotal')));
        }

        if (Mage::getStoreConfig('amreviews/conditions/customer_group') != '') {
            $sql->where('o.customer_group_id IN( ' . trim(Mage::getStoreConfig('amreviews/conditions/customer_group'), ',') . ')');
        }

        $rows = $db->fetchAll($sql);
        foreach ($rows as $r) {
            $orders[$r['entity_id']] = $r;
        }

        return $orders;
    }

    private function filterByBlacklist($orders)
    {
        $db = $this->_getReadAdapter();

        $sql = $db->select()
                  ->from($this->getTable('amreviews/blist'), 'LOWER(`email`)');

        $blackListed = $db->fetchCol($sql);

        foreach ($orders as $id => $order) {
            if (in_array(strtolower($order['email']), $blackListed)) {
                unset($orders[$id]);
            }
        }

        return $orders;
    }

    private function findProductsByOrders($orders)
    {
        $orderIDs = array_keys($orders);

        $db  = $this->_getReadAdapter();
        $sql = $db->select()
                  ->from(array('i' => $this->getTable('sales/order_item')), array('order_id', 'store_id', 'product_id', 'name', 'product_type'))
                  ->where('order_id IN (?)', $orderIDs)
                  ->where('parent_item_id IS NULL'); // only simple and configurable head items (excluding configurable child items)

        $categories = trim(Mage::getStoreConfig('amreviews/conditions/product_category'), ',');
        if ($categories) {
            $sql->joinLeft(
                array('cat' => $this->getTable('catalog/category_product')),
                'i.product_id=cat.product_id', 'cat.category_id'
            );
            $sql->where('cat.category_id  IN( ' . $categories . ')');
        }

        $rows = $db->fetchAll($sql);

        $products           = array();
        $orders_to_products = array();

        foreach ($rows as $r) {
            /* select unique products for each order (only if order have >1 product) */
            if (!isset($products[$r['product_id']])) {
                $products[$r['product_id']] = $r;
            }
            $orders_to_products[$r['order_id']][$r['product_id']] = $r['product_id'];
        }

        return array($products, $orders_to_products);
    }

    private function filterByProductCategory($orders, $orders_to_products)
    {
        foreach ($orders as $id => $item) {
            if (!isset($orders_to_products[$id])) {
                unset($orders[$id]);
            }
        }

        return $orders;
    }

    private function filterByHistoryOrders($orders)
    {
        $orderIDs = array_keys($orders);

        $db  = $this->_getReadAdapter();
        $sql = $db->select()
                  ->from($this->getMainTable(), 'order_id')
                  ->where('order_id IN ( ? )', $orderIDs);

        $oldOrders = $db->fetchCol($sql);
        $newOrders = array_diff($orderIDs, $oldOrders);

        foreach ($orders as $id => $order)
            if (!in_array($id, $newOrders))
                unset($orders[$id]);

        return $orders;
    }

    /*
     * remove orders that have no products in selected categories
     */

    private function filterByHistoryEmails($orders)
    {
        $orderEmails = array();
        $emails      = array();
        foreach ($orders as $order) {
            $orderEmails[] = $order['email'];
        }
        $orderEmails = array_unique($orderEmails);
        $orderIDs    = array_keys($orders);

        $db  = $this->_getReadAdapter();
        $sql = $db->select()
                  ->from($this->getMainTable(), 'email')
                  ->where('email IN ( ? )', $orderEmails);

        $oldOrders = $db->fetchCol($sql);

        foreach ($orders as $id => $order) {
            if (in_array($order['email'], $oldOrders)) {
                unset($orders[$id]);
                continue;
            }
            if (!isset($emails[$order['email']])) {
                $emails[$order['email']] = $order['email'];
            } else {
                unset($orders[$id]);
                continue;
            }
        }

        return $orders;
    }

    private function filterByProduct($productIds, $orders, $orders_to_products)
    {
        $productIds = array_keys($productIds);
        $where      = '(  (product_ids LIKE "%,' . implode(',%") OR (product_ids LIKE "%,', $productIds) . ',%") )';

        $db  = $this->_getReadAdapter();
        $sql = $db->select()
                  ->from($this->getMainTable(), 'product_ids')
                  ->where($where);;

        /*
         * remove orders with same products in previous orders sent
         * leave orders only with unique products
         */
        $old_data = $db->fetchCol($sql);
        $used_ids = array();
        $old_ids  = array();

        foreach ($old_data as $items) {
            $items   = explode(',', trim($items, ','));
            $old_ids = array_merge($items, $old_ids);
        }

        foreach ($orders_to_products as $oid => &$items) {
            if (!is_array($items) && count($items) <= 0) {
                unset($orders[$oid]);
                continue;
            }

            $items    = array_diff($items, $old_ids);
            $items    = array_diff($items, $used_ids);
            $used_ids = array_merge($used_ids, $items);

            if (!$items) {
                unset($orders[$oid]);
            }
        }

        return array($orders, $orders_to_products);
    }

    private function saveAll($candidates)
    {
        if (!$candidates) {
            return 0;
        }

        $db = $this->_getWriteAdapter();

        $sql = 'insert into ' . $this->getMainTable() . ' (
            purchased_at, email, name,
            order_id, product_ids, customer_id, store_id, increment_id,
            sent, sent_at, visited_at, visit_ip) values ';

        foreach ($candidates as $c) {
            $sql .= $db->quoteInto('(?,', $c['created_at']);
            $sql .= $db->quoteInto('?, ', $c['email']);
            $sql .= $db->quoteInto('?, ', $c['billing_name']);

            $sql .= $db->quoteInto('?, ', $c['order_id']);
            $sql .= $db->quoteInto('?, ', $c['product_ids']);
            $sql .= $db->quoteInto('?, ', $c['customer_id']);
            $sql .= $db->quoteInto('?, ', $c['store_id']);
            $sql .= $db->quoteInto('?, ', $c['increment_id']);

            $sql .= $db->quoteInto('?, ', 0);
            $sql .= $db->quoteInto('?, ', 0);
            $sql .= $db->quoteInto('?, ', 0);
            $sql .= $db->quoteInto('?),', 0);
        }

        $sql = substr($sql, 0, -1);
        $db->raw_query($sql);

        return count($candidates);
    }

    public function getProductImage($productId, $type = 'thumbnail', $storeId = 0, $imgWidth = 0, $imgHeight = 0)
    {
        $store      = Mage::app()->getStore();
        $imgWidth   = $imgWidth ? $imgWidth : Mage::getStoreConfig('amreviews/email/image_width', $store);
        $imgHeight  = $imgHeight ? $imgHeight : Mage::getStoreConfig('amreviews/email/image_height', $store);
        $curProduct = Mage::getModel('catalog/product')->setStoreId($storeId)->load($productId);

        /*
         * search for child image if this option enabled via admin panel
         */
        if (!Mage::getStoreConfig('amreviews/email/get_image_from_child')) {
            $image = Mage::helper('catalog/image')->init($curProduct, $type)->resize($imgWidth, $imgHeight);

            return $image;
        } else {
            $childIds = $this->getProductChildIds($productId);

            if (isset($childIds[0])) {
                $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($childIds[0]);
            } else {
                $product = $curProduct;
            }

            $image = Mage::helper('catalog/image')->init($product, $type)->resize($imgWidth, $imgHeight);

            return $image;
        }
    }

    private function getProductChildIds($productId)
    {
        $db  = $this->_getReadAdapter();
        $sql = $db->select()
                  ->from($this->getTable('sales/order_item'), array('order_id', 'product_id', 'name', 'product_type'))
                  ->where('parent_item_id = ?', $productId);

        $rows = $db->fetchAll($sql);

        return $rows;
    }

    public function removeOldCoupons()
    {
        $days = intVal(Mage::getStoreConfig('amreviews/coupon/remove_days'));
        if ($days <= 0)
            return;

        $rules = Mage::getResourceModel('salesrule/rule_collection')
                     ->addFieldToFilter('name', array('like' => 'Smart Review Reminder Coupon%'))
                     ->addFieldToFilter('from_date', array('lt' => date('Y-m-d', strtotime("-$days days"))));

        $errors = '';
        foreach ($rules as $rule) {
            try {
                $rule->delete();
            } catch (Exception $e) {
                $errors .= "\r\nError when deleting rule #" . $rule->getId() . ' : ' . $e->getMessage();
            }
        }
        Mage::log('Review Reminder Coupon Delete Errors:' . "\r\n" . $errors);
    }

    public function addCodeToInvite($inviteId, $code)
    {
        $db  = $this->_getReadAdapter();
        $res = $db->update($this->getTable('amreviews/invite'), array('coupon_code' => $code), $where = 'invite_id = ' . (int)$inviteId);

        return $res;
    }
}