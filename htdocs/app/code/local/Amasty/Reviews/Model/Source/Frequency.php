<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */

/**
 * @author Amasty
 */
class Amasty_Reviews_Model_Source_Frequency extends Mage_Core_Model_Config_Data
{
	public function toOptionArray()
	{
		$collection = array(
			'0'	=> 'once per customer',
			'1'	=> 'once per product',
			'2'	=> 'once per order',
		);
		$options = array();
	
		foreach($collection as $key => $label) {
			$options[] = array(
					'value' => $key,
					'label' => $label,
			);
		}
	
		return $options;
	}
}
