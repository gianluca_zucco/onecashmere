<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */

/**
 * @author Amasty
 */
class Amasty_Reviews_Model_Source_Import extends Mage_Core_Model_Config_Data
{
	public function _afterSave()
    {
        Mage::getResourceModel('amreviews/blist')->uploadAndImport($this);
    }
}