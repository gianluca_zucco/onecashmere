<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */

/**
 * @author Amasty
 */
class Amasty_Reviews_Model_Source_Categories extends Mage_Core_Model_Config_Data
{
    public function toOptionArray($addEmpty = true)
    {
        $collection = Mage::getModel('catalog/category')->getCollection();
        $collection->addAttributeToSelect('name')->addIsActiveFilter();//->setOrder('name','ASC');
        $options = array();

        $rootId = Mage::app()->getStore()->getRootCategoryId();
        $options = array_merge(
            array(array(
            'label' => Mage::helper('adminhtml')->__('-- Please Select --'),
            'value' => '')),
            $this->getTree($rootId)
        );

        return $options;
    }


    /**
     * Genarates tree of all categories
     *
     * @param int $rootId root category id
     * @return array sorted list category_id=>title
     */
    protected function getTree($rootId)
    {
        $tree = array();

        $collection = Mage::getModel('catalog/category')
            ->getCollection()->addNameToResult();

        $pos = array();
        foreach ($collection as $cat){
            $path = explode('/', $cat->getPath());
            if ((!$rootId || in_array($rootId, $path)) && $cat->getLevel() && $cat->getName()){
                $tree[$cat->getId()] = array(
                    'label' => str_repeat('--', $cat->getLevel()) . $cat->getName(),
                    'value' => $cat->getId(),
                    'path'  => $path,
                );
            }
            $pos[$cat->getId()] = $cat->getPosition();
        }

        foreach ($tree as $catId => $cat){
            $order = array();
            foreach ($cat['path'] as $id){
                $order[] = isset($pos[$id]) ? $pos[$id] : 0;
            }
            $tree[$catId]['order'] = $order;
        }

        usort($tree, array($this, 'compare'));

        return $tree;
    }

    /**
     * Compares category data. Must be public as used as a callback value
     *
     * @param array $a
     * @param array $b
     * @return int 0, 1 , or -1
     */
    public function compare($a, $b)
    {
        foreach ($a['path'] as $i => $id){
            if (!isset($b['path'][$i])){
                // B path is shorther then A, and values before were equal
                return 1;
            }
            if ($id != $b['path'][$i]){
                // compare category positions at the same level
                return ($a['order'][$i] < $b['order'][$i]) ? -1 : 1;
            }
        }
        // B path is longer or equal then A, and values before were equal
        return ($a['value'] == $b['value']) ? 0 : -1;
    }


}