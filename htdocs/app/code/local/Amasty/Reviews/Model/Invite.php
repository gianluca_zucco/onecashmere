<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Model_Invite extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('amreviews/invite');
    }

    public function massDelete($ids)
    {
        return $this->getResource()->massDelete($ids);
    }

    public function generate($dateTo, $dateFrom)
    {
        return $this->getResource()->generate($dateTo, $dateFrom);
    }

    public function send()
    {
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);
        $storeId = $this->getStoreId();


        $productsItems = $this->getProductIds();
        $productsItems = explode(',', trim($productsItems, ','));
        $products      = array();
        foreach ($productsItems as $item) {
            if ($item > 0) {
                $product    = Mage::getModel('catalog/product')->setStoreId($storeId)->load($item);
                $products[] = array(
                    'name'        => $product->getName(),
                    'description' => $product->getDescription(),
                    'id'          => $product->getEntityId(),
                    'url'         => $this->getProxyUrl($this->getId(), $product->getEntityId(), $storeId, $this->getCustomerId()),
                    'store_id'    => $storeId,
                );
            }
        }
        $this->setProducts($products);


        $tpl = Mage::getModel('core/email_template');
        $tpl->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
            ->sendTransactional(
                Mage::getStoreConfig('amreviews/email/template', $storeId),
                Mage::getStoreConfig('amreviews/email/sender', $storeId),
                $this->getEmail(),
                $this->getName(),
                array(
                    'name'        => $this->getName(),
                    'visit_ip'    => $this->getVisitIp(),
                    'store_id'    => $this->getStoreId(),
                    'customer_id' => $this->getCustomerId(),
                    'order_id'    => $this->getOrderId(), // increment id
                    'coupon_days' => Mage::getStoreConfig('amreviews/coupon/coupon_days', $storeId),
                    'reminder'    => $this,
                )
            );

        $tpl = null;
        $translate->setTranslateInline(true);

        $this->unsProducts();
        $this->setSent(1);
        $this->setSentAt(date('Y-m-d G:i:s'));
        $this->save();

        return $this;
    }

    public function getProxyUrl($invite_id, $product_id, $store_id, $customer_id = 0)
    {
        // get URL params as encoded plain string
        $code = $this->encodeParam($invite_id, $product_id, $store_id, $customer_id);

        //get autoLogin string if enabled
        $autoLogin = '';
        if ($customer_id && Mage::getStoreConfig('amreviews/email/autologin')) {
            $autoLogin     = '&autologin=';
            $key           = $customer_id . $invite_id . $product_id . $store_id;
            $autoLoginStr  = $key.'|'.time();
            $autoLoginData = Mage::helper('amreviews')->encrypt_decrypt('encrypt', $autoLoginStr, $key);
            $autoLogin .= $autoLoginData;
        }

        // finally, real URL will be like
        $baseUrl = Mage::app()->getStore($store_id)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
        $url     = trim($baseUrl, ' /') . '/reminder/?code=' . urlencode($code) . $autoLogin;

        return $url;
    }

    public function encodeParam($invite_id, $product_id, $storeId, $customerId = 0)
    {
        return base64_encode($invite_id . ',' . $product_id . ',' . $storeId . ',' . $customerId);
    }

    public function sendTest($email, $order)
    {
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        $storeId = $order->getStoreId();
        $store   = Mage::app()->getStore($storeId);

        $this->setEmail($email);
        $this->setName('TEST EMAIL SENDING');
        $this->setSent(0);
        $this->save();

        $products = array();
        foreach ($order->getItemsCollection() as $item) {
            if ($item->getParentItemId() > 0) {
                continue;
            }
            $products[] = array(
                'name'        => $item->getName(),
                'description' => Mage::getModel('catalog/product')->load($item->getProductId())->getDescription(),
                'id'          => $item->getProductId(),
                'url'         => $this->getProxyUrl($this->getId(), $item->getProductId(), $storeId, $order->getCustomerId()),
                'store_id'    => $storeId,
            );
        }
        $this->setProducts($products);


        $tpl = Mage::getModel('core/email_template');
        $tpl->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
            ->sendTransactional(
                Mage::getStoreConfig('amreviews/email/template', $store),
                Mage::getStoreConfig('amreviews/email/sender', $store),
                $email,
                $order->getCustomerName(),
                array(
                    'name'        => $order->getCustomerName(),
                    'visit_ip'    => $order->getVisitIp(),
                    'store_id'    => $storeId,
                    'customer_id' => $order->getCustomerId(),
                    'order_id'    => $order->getEntityId(), // increment id
                    'coupon_days' => Mage::getStoreConfig('amreviews/coupon/coupon_days', $store),
                    'reminder'    => $this,
                )
            );
        $tpl = null;
        $translate->setTranslateInline(true);

        $this->unsProducts();
        $this->setSent(1);
        $this->setSentAt(date('Y-m-d G:i:s'));
        $this->save();

        return $this;
    }

    public function decodeParam($param)
    {
        $code = Mage::helper('core')->decrypt($param);
        if (!strpos($code, ',')) {
            list($invite_id, $product_id, $storeId, $customer) = explode(',', base64_decode($param), 4);
        } else {
            $storeId  = 0;
            $customer = 0;
            list($invite_id, $product_id) = explode(',', $code, 2);
            if (!$product_id || $product_id <= 0) {
                list($invite_id, $product_id, $storeId, $customer) = explode(',', base64_decode($param), 4);
            }
        }

        return array($invite_id, $product_id, $storeId, $customer);
    }

    public function coupon()
    {
        if (Mage::getStoreConfig('amreviews/coupon/enabled')) {
            if ($this->getCouponCode()) {
                return $this->getCouponCode();
            }

            $storeId                   = Mage::app()->getStore()->getStoreId();
            $couponData                = array();
            $couponData['name']        = 'Smart Review Reminder Coupon ' . date('Y-m-d');
            $couponData['is_active']   = Mage::getStoreConfig('amreviews/coupon/need_to_review', $storeId) ? 0 : 1;
            $couponData['website_ids'] = array_keys(Mage::app()->getWebsites(true));
            $couponData['coupon_type'] = 2;  // for magento 1.4.1.1
            $couponData['coupon_code'] = strtoupper(uniqid());

            $maxUses                         = intVal(Mage::getStoreConfig('amreviews/coupon/coupon_uses'));
            $usesPerCustomer                 = intVal(Mage::getStoreConfig('amreviews/coupon/uses_per_customer'));
            $couponData['uses_per_coupon']   = $maxUses;
            $couponData['uses_per_customer'] = $usesPerCustomer;
            $couponData['from_date']         = ''; //current date

            $days                  = intVal(Mage::getStoreConfig('amreviews/coupon/coupon_days'));
            $date                  = date('Y-m-d', time() + $days * 24 * 3600);
            $couponData['to_date'] = $date;

            $couponData['simple_action']   = Mage::getStoreConfig('amreviews/coupon/coupon_type');
            $couponData['discount_amount'] = Mage::getStoreConfig('amreviews/coupon/coupon_amount');

            if ('ampromo_cart' == $couponData['simple_action']) {
                $couponData['promo_sku']       = $couponData['discount_amount'];
                $couponData['discount_amount'] = 0;
            }

            $couponData['conditions'] = array(
                '1'    => array(
                    'type'       => 'salesrule/rule_condition_combine',
                    'aggregator' => 'all',
                    'value'      => 1,
                    'new_child'  => '',
                ),
                '1--1' => array(
                    'type'      => 'salesrule/rule_condition_address',
                    'attribute' => 'base_subtotal',
                    'operator'  => '>=',
                    'value'     => floatVal(Mage::getStoreConfig('amreviews/coupon/min_order')),
                ),
            );

            $couponData['actions'] = array(
                1 => array(
                    'type'       => 'salesrule/rule_condition_product_combine',
                    'aggregator' => 'all',
                    'value'      => 1,
                    'new_child'  => '',
                )
            );

            //compatibility with aitoc's individ promo module
            $couponData['customer_individ_ids'] = array();

            //create for all customer groups
            $couponData['customer_group_ids'] = array();
            if (!Mage::getStoreConfig('amreviews/coupon/customer_group')) {
                $customerGroups = Mage::getResourceModel('customer/group_collection')
                                      ->load();

                $found = false;
                foreach ($customerGroups as $group) {
                    if (0 == $group->getId()) {
                        $found = true;
                    }
                    $couponData['customer_group_ids'][] = $group->getId();
                }
                if (!$found) {
                    $couponData['customer_group_ids'][] = 0;
                }
            } else {
                $groups                           = Mage::getStoreConfig('amreviews/coupon/customer_group');
                $couponData['customer_group_ids'] = explode(',', $groups);
            }

            try {
                Mage::getModel('salesrule/rule')
                    ->loadPost($couponData)
                    ->save();
            } catch (Exception $e) {
                $couponData['coupon_code'] = '';
            }

            // add coupon data in Reviews_Invite table
            $this->setCouponCode($couponData['coupon_code']);
            $this->getResource()->addCodeToInvite($this->getInviteId(), $couponData['coupon_code']);

            return $couponData['coupon_code'];

        } else {
            return false;
        }

    }

    public function unsubscribeLink()
    {
        $email = $this->getEmail();
        $link  = trim(Mage::getBaseUrl(), ' /') . '/reminder/unsubscribe/?code=' . urlencode($this->encodeParam(urlencode($email), '', '', 0));

        return $link;
    }

    public function removeOldCoupons()
    {
        return $this->getResource()->removeOldCoupons();
    }

    public function insertProductsGrid($imgWidth = 0, $imgHeight = 0, $descLenght = 0)
    {
        $products = $this->getProducts();
        $table    = '';

        if (is_array($products) && count($products) > 0) {
            $table = '<table style="border: 1px solid black; font-size: 10px">' . '';
            foreach ($products as $product) {
                $descLenght = $descLenght ? $descLenght : Mage::getStoreConfig('amreviews/email/description_lenght', $product['store_id']);
                if (strlen($product['description']) > $descLenght) {
                    $product['description'] = substr($product['description'], 0, $descLenght) . '...';
                }
                $table .= '<tr>';
                $table .= ' <td><a href="' . $product['url'] . '" style="color:#1E7EC8;"><img src="' . $this->image($product['id'], 'thumbnail', $imgWidth, $imgHeight) . '" /></a></td>';
                $table .= ' <td><h2><a href="' . $product['url'] . '" style="color:#1E7EC8;">' . $product['name'] . '</a></h2>' . $product['description'] . '</td>';
                $table .= '</tr>';
            }
            $table .= '</table>';
        }

        return $table;
    }

    public function image($product_id, $type = 'thumbnail', $width = 0, $height = 0)
    {
        $storeId = Mage::app()->getStore()->getStoreId();

        return $this->getResource()->getProductImage($product_id, $type, $storeId, $width, $height);
    }

    public function needReview()
    {
        $storeId = Mage::app()->getStore()->getStoreId();

        return Mage::getStoreConfig('amreviews/coupon/need_to_review', $storeId) ? true : false;
    }

    public function getDiscount()
    {
        $storeId      = Mage::app()->getStore()->getStoreId();
        $discount     = Mage::getStoreConfig('amreviews/coupon/coupon_amount', $storeId);
        $discountType = Mage::getStoreConfig('amreviews/coupon/coupon_type', $storeId);
        if ($discountType == 'by_percent') {
            $discount .= '% ';
        } else if ($discountType == 'by_fixed') {
            $discount .= ' ' . Mage::app()->getStore()->getCurrentCurrencyCode() . ' (for single item) ';
        } else if ($discountType == 'cart_fixed') {
            $discount .= ' ' . Mage::app()->getStore()->getCurrentCurrencyCode() . ' (for whole cart) ';
        } else if ($discountType == 'ampromo_cart') {
            $discount_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $discount);
            if ($discount_product->getData()) {
                $discount = 'a free product <a href="' . $discount_product->getProductUrl(true) . '">"' . $discount_product->getName() . '"</a> ';
            }
        }

        return $discount;
    }

}