<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */ 
class Amasty_Reviews_Model_Blist extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('amreviews/blist');
    }

    public function addEmail($email)
    {
        return $this->getResource()->addEmail($email);
    }
}