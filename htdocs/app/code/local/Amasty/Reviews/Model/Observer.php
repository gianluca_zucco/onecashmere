<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Model_Observer
{
    public function generate()
    {
        $afterDays = Mage::getStoreConfig('amreviews/email/send_after_days');
        $tillDays  = Mage::getStoreConfig('amreviews/email/generate_for_days');
        $dateFrom  = date('Y-m-d', strtotime("-$tillDays days"));
        $dateTo    = date('Y-m-d', strtotime("-$afterDays days"));
        Mage::getModel('amreviews/invite')->generate($dateFrom, $dateTo);
        Mage::getModel('amreviews/invite')->removeOldCoupons();
    }

    public function send()
    {
        $sendBy   = Mage::getStoreConfig('amreviews/email/send_by_count');
        $autosend = Mage::getStoreConfig('amreviews/email/autosend');
        if (!$autosend) {
            Mage::getSingleton('adminhtml/session')->addNotice('Autosend option is not enabled!');

            return false;
        }

        $collection = Mage::getModel('amreviews/invite')
                          ->getCollection()
                          ->addFieldToFilter('sent', 0)
                          ->setPageSize($sendBy ? $sendBy : 100)
                          ->setCurPage(1);

        foreach ($collection as $invite) {
            $invite->send();
        }
    }

    public function updateReviewCoupon(Varien_Event_Observer $observer)
    {
        $review = $observer->getObject();

        // only if review status is Approved and it was NOT approved
        // e.g. check approving review action
        if ($review && $review->getStatusId() == '1' && $review->getOrigData('status_id') != '1') {
            // get variables with IDs
            $inviteId   = Mage::getModel('amreviews/reviews')->load($review->getId(), 'review_id')->getInviteId();
            $productId  = $review->getEntityPkValue();
            $customerId = $review->getCustomerId();

            // only when review have product and defined customerID or inner table "invite->review" association
            if (($customerId || $inviteId) && $productId) {
                $invite = array();
                if ($customerId) {
                    // get all review invites with such combination of
                    // customer ID and product ID
                    $invite = Mage::getModel('amreviews/invite')
                                  ->getCollection()
                                  ->addFilter('customer_id', $customerId)
                                  ->addFilter('product_ids', array('like' => "%,$productId,%"), 'public')
                                  ->load();
                } else if ($inviteId) {
                    $inviteToReview = Mage::getModel('amreviews/invite')->load($inviteId);
                    if ($inviteToReview) {
                        $invite[] = $inviteToReview;
                    }
                }
                // if found any invite
                if ($invite) {
                    foreach ($invite as $item) {
                        $couponCode = $item->getCouponCode();
                        $coupon     = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
                        if ($coupon) {
                            // load coupon rule
                            $ruleId = $coupon->getRuleId();
                            $rule   = Mage::getModel('salesrule/rule')->load($ruleId);

                            // if rule loaded - process coupon+rule activation
                            if ($ruleId && $rule) {
                                // add delay to coupon from current date
                                $daysAdd = ' + ' . (int)Mage::getStoreConfig('amreviews/coupon/notify_customer') . ' days';
                                $expDate = date('Y-m-d', strtotime($daysAdd)) . ' 23:59:59';
                                $coupon->setExpirationDate($expDate);
                                $coupon->save();

                                // set cuopon rule as active
                                $rule->setIsActive(true);
                                $rule->save();

                                // send email with notification
                                if (Mage::getStoreConfig('amreviews/coupon/notify_customer')) {
                                    $translate = Mage::getSingleton('core/translate');
                                    $translate->setTranslateInline(false);

                                    $storeId = $review->getStoreId();
                                    $store   = Mage::app()->getStore($storeId);

                                    $customer = Mage::getModel('customer/customer');
                                    if ($customerId) {
                                        $customer->load($customerId);
                                        $customer_name  = $customer->getName();
                                        $customer_email = $customer->getEmail();
                                    } else {
                                        $customer_name  = $item->getEmail();
                                        $customer_email = $item->getEmail();
                                    }
                                    $product = Mage::getModel('catalog/product')->load($productId);

                                    $tpl = Mage::getModel('core/email_template');
                                    $tpl->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
                                        ->sendTransactional(
                                            Mage::getStoreConfig('amreviews/coupon/notify_customer_template', $store),
                                            Mage::getStoreConfig('amreviews/email/sender', $store),
                                            $customer_email,
                                            $customer_name,
                                            array(
                                                'name'         => $customer_name,
                                                'store_id'     => $storeId,
                                                'customer_id'  => $customerId,
                                                'coupon_days'  => Mage::getStoreConfig('amreviews/coupon/coupon_days', $store),
                                                'coupon_code'  => $couponCode,
                                                'product_name' => $product->getName(),
                                            )
                                        );
                                    $tpl = null;
                                    $translate->setTranslateInline(true);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $this;
    }

    public function attachReviewToInvite(Varien_Event_Observer $observer)
    {
        $review     = $observer->getObject();
        $module     = Mage::app()->getRequest()->getModuleName();
        $controller = Mage::app()->getRequest()->getControllerName();

        if ($review && $module == 'review' && $controller == 'product') {
            // try get saved review invite ID to indeficate user if guest
            $inviteId = Mage::getSingleton('core/session')->getReviewInviteId();
            $reviewId = $review->getId();
            if ($inviteId && $reviewId) {
                // attach current review ID info to review invite ID
                $reviewToInvite = Mage::getModel('amreviews/reviews')->load($reviewId, 'review_id');
                if (!$reviewToInvite->getData()) {
                    $reviewToInvite->setReviewId($review->getId());
                    $reviewToInvite->setInviteId($inviteId);
                    $reviewToInvite->save();
                }
            }
        }
    }
}