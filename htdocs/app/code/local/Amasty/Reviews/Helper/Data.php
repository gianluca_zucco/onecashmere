<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param $action   string  params: encrypt || decrypt
     * @param $string   string  target string to encode\decode
     * @param $key
     *
     * @return bool|string
     */
    public function encrypt_decrypt($action, $string, $key)
    {
        $output         = false;
        $encrypt_method = "AES-256-CBC";

        // hash
        $key = hash('sha256', $key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $key), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }
}