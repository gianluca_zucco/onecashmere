<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */ 
class Amasty_Reviews_UnsubscribeController extends Mage_Core_Controller_Front_Action
{
    public function indexAction(){
        $code = $this->getRequest()->getQuery('code');
        if (!$code){
            Mage::getSingleton('customer/session')->addError('No any code entered. Check the link, please.');
            header('Location: / ');
            die();
        }

        $model = Mage::getModel('amreviews/invite');
        list($email,$tmp1,$tmp2,$tmp3) = $model->decodeParam($code);
        $email = urldecode($email);
        if (!$email) {
            Mage::getSingleton('customer/session')->addError('Wrong code entered. Check the link, please.');
            header('Location: / ');
            die();
        }

        ;
        if (!$res = Mage::getModel('amreviews/blist')->addEmail($email)){
            Mage::getSingleton('customer/session')->addError("Sorry, but something went wrong. Contact our support, please.");
            header('Location: / ');
            die();
        } else {
            Mage::getSingleton('customer/session')->addSuccess('You were successfully insubscribed.');
            header('Location: / ');
            die();
        }
    }
}