<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $code = $this->getRequest()->getQuery('code');
        if (!$code) {
            Mage::log('Smart Reviews Reminder: no any code entered');
            Mage::getSingleton('customer/session')->addError('No any code entered. Check the link, please.');
            header('Location: / ');
            exit;
        }

        $model = Mage::getModel('amreviews/invite');
        list($invite_id, $product_id, $store_id, $customerId) = $model->decodeParam($code);
        if (!$invite_id) {
            Mage::log('Smart Reviews Reminder: wrong code entered - "' . print_r($code) . '"');
            Mage::getSingleton('customer/session')->addError('Wrong code entered. Check the link, please.');
            header('Location: / ');
        }

        $model->load($invite_id);
        if (!$model->getId()) {
            Mage::log('Smart Reviews Reminder: no such invite like - "' . print_r($invite_id) . '"');
            Mage::getSingleton('customer/session')->addError("Sorry, but such product wasn't found on site.");
            header('Location: / ');
            exit;
        }

        $visitedAt = strtotime($model->getVisitedAt());
        if ($visitedAt < 0) {
            $model->setVisitedAt(date('Y-m-d H:i:s'));
            $model->setVisitIp($this->getRequest()->getClientIp());
            $model->save();
        }

        // save data about review invite ID
        Mage::getSingleton('core/session')->setReviewInviteId($invite_id);

        // check if user can be logged in
        $autoLogin = $this->getRequest()->getQuery('autologin');
        if (Mage::getStoreConfig('amreviews/email/autologin') && $autoLogin) {
            $key           = $customerId . $invite_id . $product_id . $store_id;
            $autoLoginData = explode('|', Mage::helper('amreviews')->encrypt_decrypt('decrypt', $autoLogin, $key));
            if ($autoLoginData[0] == $key) {
                $expireDays = '-'.Mage::getStoreConfig('amreviews/email/autologin_expire').' days';
                if ($autoLoginData[1] >= strtotime($expireDays)){
                        $customer = Mage::getModel('customer/customer')->load($customerId);
                        if ($customer->getId()){
                            Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($customer);
                            Mage::getSingleton('customer/session')->renewSession();
                        }
                }
            }
        }

        // @todo check if seo review module is active and "add reviews on product page" is set to Yes
        //$url        = Mage::getUrl('catalog/product/view', array('id' => $product_id, '_store' => $store_id, '_store_to_url' => true));
		$url =  Mage::getModel('catalog/product')->load($product_id)->getProductUrl();
        $params     = Mage::getStoreConfig('amreviews/google_analytics');
        $url_params = array();
        if (is_array($params) && count($params) > 0) {
            foreach ($params as $k => $v) {
                if ($v) {
                    $url_params[] = "$k=" . urlencode($v);
                }
            }
            $url_params = '?' . implode('&', $url_params);
            if ($url_params != '?') {
                $url .= $url_params;
            }
        }

        header('Location: ' . $url);
        exit;
    }

}