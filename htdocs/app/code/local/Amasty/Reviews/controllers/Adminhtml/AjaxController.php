<?php 
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Adminhtml_AjaxController extends Mage_Adminhtml_Controller_Action
{
	public function sendTestEmailAction()
    {
		$request	= Mage::app()->getRequest();
		$email		= trim($request->getParam('test_recipient_email'));
		$orderId	= trim($request->getParam('test_order_id'));
		if (!$email || !$orderId){
			return false;
		}

        $order   = Mage::getModel('sales/order')->loadByIncrementId($orderId);
        $mailing = Mage::getModel('amreviews/invite')->sendTest($email, $order);

        $msg = Mage::helper('amreviews')->__('Test email has been successfully sent');
        Mage::getSingleton('adminhtml/session')->addSuccess($msg);

        $this->_redirect('adminhtml/system_config/edit/section/amreviews');
	}
}