<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */
class Amasty_Reviews_Adminhtml_QueueController extends Mage_Adminhtml_Controller_Action
{
    protected function _setActiveMenu($menuPath)
    {
        $this->getLayout()->getBlock('menu')->setActive($menuPath);
        $this->_title($this->__('Customers'))->_title($this->__('Smart Review Reminder'));
        return $this;
    } 
    
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('catalog/reviews_ratings/amreviews/queue');
        $this->_addContent($this->getLayout()->createBlock('amreviews/adminhtml_queue'));
        $this->renderLayout();
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
             Mage::getSingleton('adminhtml/session')->addError(Mage::helper('amreviews')->__('Please select records'));
             $this->_redirect('*/*/');
             return;
        }
         
        try {
            Mage::getModel('amreviews/invite')->massDelete($ids);
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('adminhtml')->__(
                    'Total of %d record(s) were successfully deleted', count($ids)
                )
            );
        } 
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        
        $this->_redirect('*/*/');
        return;
    }
    public function massSendAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
             Mage::getSingleton('adminhtml/session')->addError(Mage::helper('amreviews')->__('Please select records'));
             $this->_redirect('*/*/');
             return;
        }

        foreach($ids as $id) {
            try {
                Mage::getModel('amreviews/invite')->load($id)->send();
                            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('adminhtml')->__(
                'Total of %d record(s) were successfully sended', count($ids)
            )
        );

        $this->_redirect('*/*/');
        return;
    }

    public function newAction()
    {
        $model = Mage::getModel('amreviews/invite');

        $afterDays	= Mage::getStoreConfig('amreviews/email/send_after_days');
        $tillDays	= Mage::getStoreConfig('amreviews/email/generate_for_days');
        $dateFrom	= date('Y-m-d', strtotime("-$tillDays days"));
        $dateTo  	= date('Y-m-d', strtotime("-$afterDays days"));

        $cnt = $model->generate($dateFrom, $dateTo);
        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('amreviews')->__('Total of %d record(s) were successfully generated', $cnt)
        );

        $this->_redirect('*/*/');
        return;
    }

    public function sendAction()
    {
        $id = $this->getRequest()->getParam('invite_id');
        $model = Mage::getModel('amreviews/invite')->load($id);

        if ($model->getId()){
            $model->send();
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('amreviews')->__('Invite has been sent to %s', $model->getEmail())
            );
        }
        else {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('amreviews')->__('Can not find invite #`%d`', $id)
            );

        }

        $this->_redirect('*/*/');
        return;
    }
}