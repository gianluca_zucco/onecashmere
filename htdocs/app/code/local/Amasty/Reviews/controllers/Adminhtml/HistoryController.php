<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */ 
class Amasty_Reviews_Adminhtml_HistoryController extends Mage_Adminhtml_Controller_Action
{
    protected function _setActiveMenu($menuPath)
    {
        $this->getLayout()->getBlock('menu')->setActive($menuPath);
        $this->_title($this->__('Customers'))->_title($this->__('Smart Review Reminder'));
        return $this;
    } 
    
    public function indexAction()
    {
	    $this->loadLayout(); 
        $this->_setActiveMenu('catalog/reviews_ratings/amreviews/history');
        $this->_addContent($this->getLayout()->createBlock('amreviews/adminhtml_history')); 	    
 	    $this->renderLayout();
    }
}