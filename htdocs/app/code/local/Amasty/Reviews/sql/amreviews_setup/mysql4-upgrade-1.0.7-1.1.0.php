<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */

$this->startSetup();

$this->run("

ALTER TABLE `{$this->getTable('amreviews/invite')}` ADD `coupon_code` VARCHAR(255) NOT NULL ;

CREATE TABLE `{$this->getTable('amreviews/reviews')}` (
  `id`             int(10) unsigned NOT NULL auto_increment,
  `review_id`      varchar(255) NOT NULL,
  `invite_id`      varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `review_id` (`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

"
);

$this->endSetup(); 