<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */

$this->startSetup();

$this->run(''."ALTER TABLE `{$this->getTable('amreviews/invite')}` ADD `increment_id` VARCHAR(25) NOT NULL ;");

$this->endSetup(); 