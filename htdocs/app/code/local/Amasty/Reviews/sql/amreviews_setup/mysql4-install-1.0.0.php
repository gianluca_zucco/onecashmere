<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */  
$this->startSetup();

$this->run("

CREATE TABLE `{$this->getTable('amreviews/invite')}` (
  `invite_id`     mediumint(8) unsigned NOT NULL auto_increment,

  `order_id`      mediumint(8) unsigned NOT NULL,
  `product_id`    mediumint(8)  unsigned NOT NULL,
  `customer_id`   mediumint(8) unsigned NOT NULL,

  `store_id`      smallint(5) unsigned NOT NULL,

  `sent`          tinyint(1) unsigned NOT NULL,
  `purchased_at`  datetime NOT NULL,
  `sent_at`       datetime DEFAULT NULL,
  `visited_at`    datetime DEFAULT NULL,

  `email`      varchar(255) NOT NULL,
  `name`       varchar(255) NOT NULL,

  `url`        varchar(255) NOT NULL,
  `visit_ip`   varchar(16) NOT NULL,

  `product`        varchar(255) NOT NULL,
  `product_desc`   text,

  PRIMARY KEY  (`invite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{$this->getTable('amreviews/blist')}` (
  `blist_id`  mediumint(8) unsigned NOT NULL auto_increment,
  `email`      varchar(255) NOT NULL,
  PRIMARY KEY  (`blist_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");


$this->endSetup(); 