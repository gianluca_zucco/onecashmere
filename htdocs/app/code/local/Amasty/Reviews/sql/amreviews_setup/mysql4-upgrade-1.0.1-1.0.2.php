<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Reviews
 */

$this->startSetup();

$this->run("

ALTER TABLE `{$this->getTable('amreviews/invite')}` DROP `product_id`;
ALTER TABLE `{$this->getTable('amreviews/invite')}` ADD `product_ids` VARCHAR(255) NOT NULL ;

");

$this->endSetup(); 