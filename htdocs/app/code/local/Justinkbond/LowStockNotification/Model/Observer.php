<?php

use Varien_Event_Observer as Event;

class Justinkbond_LowStockNotification_Model_Observer
{
    public function lowStockReport(Event $event)
    {
        /** @var Mage_CatalogInventory_Model_Stock_Item $stockItem */
        $stockItem = $event->getItem();

        if ($stockItem->getQty() < $stockItem->getNotifyStockQty() && !Mage::app()->getStore()->isAdmin()) {
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product')->load($stockItem->getProductId());
            $subject = $stockItem->getQty() ? "[Notice] Low Stock Notification" : "[Notice] Out of Stock Notification";

            if ($stockItem->getQty() > 0) {
                $body[] = "{$product->getName()} :: {$product->getSku()} is low on stock!";
                $body[] = "Current Qty: {$stockItem->getQty()}";
                $body[] = "Low Stock Date: {$stockItem->getLowStockDate()}";
            } else {
                $body[] = "{$product->getName()} :: {$product->getSku()} is out of stock!";
                $body[] = "Out of Stock Date: {$stockItem->getLowStockDate()}";
            }
            $this->sendEmail($subject, implode(PHP_EOL, $body));
        }
    }

    private function sendEmail(string $subject, string $body)
    {
        /** @var Mage_Core_Model_Email $mail */
        $mail = Mage::getModel('core/email');
        $mail->setData([
            'to_name' => Mage::getStoreConfig('trans_email/ident_general/name'),
            'to_email' => 'ordini@onecashmere.com',
            'from_name' => Mage::getStoreConfig('trans_email/ident_general/name'),
            'from_email' => Mage::getStoreConfig('trans_email/ident_general/email'),
            'subject' => $subject,
            'body' => $body,
            'type' => 'text'
        ]);
        $mail->send();
    }
}
