<?php

class W3Themes_Newproductslider_Block_Newproductslider extends Mage_Catalog_Block_Product_Abstract
{
    public function getNewproductslider()
    {
        if (!$this->hasData('newproductslider')) {
            $this->setData('newproductslider', Mage::registry('newproductslider'));
        }
        return $this->getData('newproductslider');
    }

    public function getProducts()
    {
        if (!$this->getProductCollection()) {
            $todayDate = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
            $products = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->addAttributeToFilter('visibility', array(
                    'in' => array(
                        Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                        Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
                    )
                ))
                ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->addAttributeToFilter('news_from_date', array('date' => true, 'to' => $todayDate))
                ->addAttributeToFilter(array(
                    array('attribute' => 'news_to_date', 'date' => true, 'from' => $todayDate),
                    array('attribute' => 'news_to_date', 'is' => new Zend_Db_Expr('null'))
                ), '')
                ->addAttributeToSort('news_from_date', 'desc');
            $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
            $this->setProductCollection($products);
        }
    }

    public function getConfig($att)
    {
        $config = Mage::getStoreConfig('newproductslider');
        if (isset($config['newproductslider_config'])) {
            $value = $config['newproductslider_config'][$att];
            return $value;
        } else {
            throw new Exception($att . ' value not set');
        }
    }
}
