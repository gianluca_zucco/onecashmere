<?php
/**
 * @project     SocialLogin
 * @package     W3Themes_SocialLogin
 * @author      W3Themes
 * @email       support@w3themes.net
 */

class W3Themes_SocialLogin_FixController extends Mage_Core_Controller_Front_Action {

    public function indexAction(){
        $this->loadLayout();
//        $this->getLayout()->getBlock('root')->setTemplate('w3_sociallogin/fix.phtml');
        $this->renderLayout();
        return $this;
    }

    public function connectAction(){

        $attributeModel = Mage::getModel('eav/entity_attribute');
        $fid = $attributeModel->getIdByCode('customer', 'w3_sociallogin_fid');
        $ftoken = $attributeModel->getIdByCode('customer', 'w3_sociallogin_ftoken');

        $gid = $attributeModel->getIdByCode('customer', 'w3_sociallogin_gid');
        $gtoken = $attributeModel->getIdByCode('customer', 'w3_sociallogin_gtoken');

        $tid = $attributeModel->getIdByCode('customer', 'w3_sociallogin_tid');
        $ttoken = $attributeModel->getIdByCode('customer', 'w3_sociallogin_ttoken');


        if($fid == false || $ftoken == false ||
            $gid == false || $gtoken == false ||
            $tid == false || $ttoken  == false
        ){

            $setup = Mage::getModel('customer/entity_setup','core_setup');
            if($fid == false){
                echo 'w3_sociallogin_fid not exits <br />';
                $setup->addAttribute('customer', 'w3_sociallogin_fid', array(
                    'type' => 'text',
                    'visible' => 0,
                    'required' => 0,
                    'user_defined' => 0,
                ));
                echo 'w3_sociallogin_fid setup ok<br />';
            }
            if($ftoken == false){
                echo 'w3_sociallogin_ftoken not exits<br />';
                $setup->addAttribute('customer', 'w3_sociallogin_ftoken', array(
                    'type' => 'text',
                    'visible' => 0,
                    'required' => 0,
                    'user_defined' => 0,
                ));
                echo 'w3_sociallogin_ftoken setup ok<br />';
            }
            if($gid == false){
                echo 'w3_sociallogin_gid not exits<br />';
                $setup->addAttribute('customer', 'w3_sociallogin_gid', array(
                    'type' => 'text',
                    'visible' => 0,
                    'required' => 0,
                    'user_defined' => 0,
                ));
                echo 'w3_sociallogin_gid setup ok<br />';
            }
            if($gtoken == false){
                echo 'w3_sociallogin_gtoken not exits<br />';
                $setup->addAttribute('customer', 'w3_sociallogin_gtoken', array(
                    'type' => 'text',
                    'visible' => 0,
                    'required' => 0,
                    'user_defined' => 0,
                ));
                echo 'w3_sociallogin_gtoken setup ok<br />';
            }
            if($tid == false){
                echo 'w3_sociallogin_tid not exits<br />';
                $setup->addAttribute('customer', 'w3_sociallogin_tid', array(
                    'type' => 'text',
                    'visible' => 0,
                    'required' => 0,
                    'user_defined' => 0,
                ));
                echo 'w3_sociallogin_tid setup ok<br />';
            }
            if($ttoken == false){
                echo 'w3_sociallogin_ttoken not exits<br />';
                $setup->addAttribute('customer', 'w3_sociallogin_ttoken', array(
                    'type' => 'text',
                    'visible' => 0,
                    'required' => 0,
                    'user_defined' => 0,
                ));
                echo 'w3_sociallogin_ttoken setup ok<br />';
            }

            if (version_compare(Mage::getVersion(), '1.6.0', '<='))
            {
                $customer = Mage::getModel('customer/customer');
                $attrSetId = $customer->getResource()->getEntityType()->getDefaultAttributeSetId();
                $setup->addAttributeToSet('customer', $attrSetId, 'General', 'w3_sociallogin_fid');
            }
            if (version_compare(Mage::getVersion(), '1.4.2', '>='))
            {
                Mage::getSingleton('eav/config')
                    ->getAttribute('customer', 'w3_sociallogin_fid')
                    ->save();
            }

            echo "Setup complete<br />";
        } else {
            echo 'All attr exits. Nothing to do.';
        }
    }
}