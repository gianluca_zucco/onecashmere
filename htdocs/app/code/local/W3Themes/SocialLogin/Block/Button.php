<?php
/**
 * @project     SocialLogin
 * @package     W3Themes_SocialLogin
 * @author      W3Themes
 * @email       support@w3themes.net
 */

class W3Themes_SocialLogin_Block_Button extends Mage_Core_Block_Template{

    protected $_buttons;

    protected function _construct(){
        parent::_construct();
        $this->_addButtons();
        $this->setTemplate('w3_sociallogin/button.phtml');
    }

    protected function _addButtons(){
        $this->_addButton(new W3Themes_SocialLogin_Block_Button_Type_Facebook());
        $this->_addButton(new W3Themes_SocialLogin_Block_Button_Type_Google());
        $this->_addButton(new W3Themes_SocialLogin_Block_Button_Type_Twitter());
    }

    protected function _addButton(W3Themes_SocialLogin_Block_Button_Type $button){
        $this->_buttons[] = $button;
    }

    protected function getButtons(){
        return $this->_buttons;
    }

}