<?php
/**
 * @project     SocialLogin
 * @package     W3Themes_SocialLogin
 * @author      W3Themes
 * @email       support@w3themes.net
 */

class W3Themes_SocialLogin_Block_Button_Type_Google extends W3Themes_SocialLogin_Block_Button_Type{

    protected $_class = 'ico-go';
    protected $_title = 'Google';
    protected $_name = 'google';
    protected $_width = 700;
    protected $_height = 500;
    protected $_disconnect = 'w3_sociallogin/google/disconnect';

    public function __construct($name = null, $class = null,$title=null){
        parent::__construct();

        $this->client = Mage::getSingleton('w3_sociallogin/google_client');
        if(!($this->client->isEnabled())) {
            return;
        }

        $this->userInfo = Mage::registry('w3_sociallogin_google_userinfo');

//        // CSRF protection
//        Mage::getSingleton('core/session')->setGoogleCsrf($csrf = md5(uniqid(rand(), TRUE)));
//        $this->client->setState($csrf);

        if(!($redirect = Mage::getSingleton('customer/session')->getBeforeAuthUrl())) {
            $redirect = Mage::helper('core/url')->getCurrentUrl();
        }

        // Redirect uri
        Mage::getSingleton('core/session')->setGoogleRedirect($redirect);
    }

}