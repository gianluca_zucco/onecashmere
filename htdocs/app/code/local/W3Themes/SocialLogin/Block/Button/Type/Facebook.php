<?php
/**
 * @project     SocialLogin
 * @package     W3Themes_SocialLogin
 * @author      W3Themes
 * @email       support@w3themes.net
 */

class W3Themes_SocialLogin_Block_Button_Type_Facebook extends W3Themes_SocialLogin_Block_Button_Type{

    protected $_class = 'ico-fb';
    protected $_title = 'Facebook';
    protected $_name = 'facebook';
    protected $_width = 500;
    protected $_height = 270;
    protected $_disconnect = 'w3_sociallogin/facebook/disconnect';

    public function __construct($name = null, $class = null,$title=null){
        parent::__construct();

        $this->client = Mage::getSingleton('w3_sociallogin/facebook_client');
        if(!($this->client->isEnabled())) {
            return;
        }

        $this->userInfo = Mage::registry('w3_sociallogin_facebook_userinfo');

        // CSRF protection
        //Mage::getSingleton('core/session')->setFacebookCsrf($csrf = md5(uniqid(rand(), TRUE)));
        //$this->client->setState($csrf);

        if(!($redirect = Mage::getSingleton('customer/session')->getBeforeAuthUrl())) {
            $redirect = Mage::helper('core/url')->getCurrentUrl();
        }

        // Redirect uri
        Mage::getSingleton('core/session')->setFacebookRedirect($redirect);

    }

}