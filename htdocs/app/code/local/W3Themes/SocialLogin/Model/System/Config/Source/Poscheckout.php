<?php

/**
 * @project     SocialLogin
 * @package     W3Themes_SocialLogin
 * @author      W3Themes
 * @email       support@w3themes.net
 */
class W3Themes_SocialLogin_Model_System_Config_Source_Poscheckout {

    public function toOptionArray() {
        return array(
            array('value' => '1', 'label' => Mage::helper('adminhtml')->__('Top')),
            array('value' => 'bottom', 'label' => Mage::helper('adminhtml')->__('Bottom')),
            array('value' => '0', 'label' => Mage::helper('adminhtml')->__("Don't Show")),
        );
    }

}
