<?php
/*------------------------------------------------------------------------
# Websites: http://www.plazathemes.com/
-------------------------------------------------------------------------*/ 
class W3Themes_Themeoptions_Model_Config_Header
{

    public function toOptionArray()
    {
        return array(
        	array('value'=>'type1', 'label'=>Mage::helper('adminhtml')->__('Type 1')),
            array('value'=>'type2', 'label'=>Mage::helper('adminhtml')->__('Type 2')),         
        );
    }

}
