<?php
/*------------------------------------------------------------------------
# Websites: http://www.plazathemes.com/
-------------------------------------------------------------------------*/ 
class W3Themes_Themeoptions_Model_Config_Fixedheader
{

    public function toOptionArray()
    {
        return array(
        	array('value'=>'disable', 'label'=>Mage::helper('adminhtml')->__('Disable')),
            array('value'=>'navonly', 'label'=>Mage::helper('adminhtml')->__('Navbar Only')),
            array('value'=>'headeronly', 'label'=>Mage::helper('adminhtml')->__('Header Only')), 
            array('value'=>'navheader', 'label'=>Mage::helper('adminhtml')->__('Both Nav & Header'))           
        );
    }

}
