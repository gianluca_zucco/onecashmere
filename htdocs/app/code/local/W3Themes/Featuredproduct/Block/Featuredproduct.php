<?php

class W3Themes_Featuredproduct_Block_Featuredproduct extends Mage_Catalog_Block_Product_Abstract
{
    protected $_collection;

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $child = $this->getLayout()->createBlock('onecashmere_catalog/product_list_item', 'product.renderer');
        $this->append($child);
    }

    public function getFeaturedproduct()
    {
        if (!$this->hasData('featuredproduct')) {
            $this->setData('featuredproduct', Mage::registry('featuredproduct'));
        }
        return $this->getData('featuredproduct');
    }

    public function getProducts()
    {
        if (!$this->getProductCollection()) {
            $products = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addAttributeToFilter('visibility', [
                    'in' => Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds()
                ])
                ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->setOrder($this->getConfig('sort'), $this->getConfig('direction'))
                ->addAttributeToFilter("featured", 1);

            $products->setPageSize(10)->setCurPage(1);
            $this->setProductCollection($products);
        }
    }

    /**
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection(): Mage_Catalog_Model_Resource_Product_Collection
    {
        if (!isset($this->_collection)) {
            $products = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addAttributeToFilter('visibility', [
                    'in' => Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds()
                ])
                ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->addMinimalPrice()
                ->addUrlRewrite()
                ->addTaxPercents()
                ->addStoreFilter()
                ->setOrder($this->getConfig('sort'), $this->getConfig('direction'))
                ->addAttributeToFilter("featured", 1);

            if ($this->getConfig('review') ) {
                Mage::getModel('review/review')->appendSummary($products);
            }

            $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
            Mage::dispatchEvent('catalog_block_product_list_collection', ['collection' => $products]);
            $this->_collection = $products;
        }
        return $this->_collection;
    }

    /**
     * @param $att
     * @return mixed
     * @throws Exception
     */
    public function getConfig($att)
    {
        $config = Mage::getStoreConfig('featuredproduct');
        if (isset($config['featuredproduct_config'])) {
            $value = $config['featuredproduct_config'][$att];
            return $value;
        } else {
            throw new Exception($att . ' value not set');
        }
    }
}
