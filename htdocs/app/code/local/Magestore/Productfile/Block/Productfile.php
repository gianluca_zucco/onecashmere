<?php

use Magestore_Productfile_Model_Productfile as File;

class Magestore_Productfile_Block_Productfile extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * @return Magestore_Productfile_Model_Productfile[]
     */
    public function getProductFiles(): array
    {
        $items = Mage::getModel('productfile/productfile')->getProductFiles()->getItems();
        return array_filter($items, function(File $file) {
            return $file->getStatus();
        });
    }

    public function getProduct(): Magestore_Productfile_Model_Mysql4_Fproduct_Collection
    {
        return Mage::getModel('productfile/fproduct')
            ->getCollection()
            ->addFieldToFilter('product_id', Mage::registry('current_product')->getId());
    }

    public function getIcon(File $file): string
    {
        $src = Mage::getBaseUrl('media');
        switch ($file->getExtension()) {
            case "application/zip":
            case "application/x-rar-compressed":
                $src .= 'icons/icon_package_get.gif';
                break;
            case "image/jpeg":
            case "image/gif":
            case "image/jpg":
            case "image/png":
                $src .= 'icons/image.gif';
                break;
            case "application/pdf":
                $src .= 'icons/file_acrobat.gif';
                break;
            case "application/msword":
            case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                $src .= 'icons/ms_word_icon.jpg';
                break;
            case "application/vnd.ms-excel":
            case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                $src .= 'icons/excel.jpg';
                break;
            case "application/vnd.ms-powerpoint":
            case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                $src .= 'icons/powerpoint.jpg';
                break;
            default:
                $src .= 'icons/icon_attachment.gif';
        }
        return $src;
    }

    public function isPdf(File $file): bool
    {
        return $file->getExtension() === 'application/pdf';
    }

    public function isWord(File $file): bool
    {
        return $file->getExtension() === 'application/msword';
    }

    public function getDownloadUrl(File $file): string
    {
        $string = $file->getFileName();
        return implode('/', [
            Mage::getBaseUrl('media') . 'productfile',
            $string[0],
            $string[1],
            strtolower($file->getFileName())
        ]);
    }

    public function getDisplaySize(File $file): string
    {
        return sprintf('(%1$s)', Mage::helper('productfile')->Format_size($file->getSize()));
    }
}
