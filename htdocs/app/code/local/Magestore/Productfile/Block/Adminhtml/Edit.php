<?php

class Magestore_Productfile_Block_Adminhtml_Productfile_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'productfile';
        $this->_controller = 'adminhtml_productfile';
        
        $this->_updateButton('delete', 'label', Mage::helper('productfile')->__('Delete File'));
		$this->_removeButton('back');

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('productfile_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'productfile_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'productfile_content');
                }
            }
        ";
		
    }

    public function getHeaderText()
    {
        if( Mage::registry('productfile_data') && Mage::registry('productfile_data')->getId() ) {
            return Mage::helper('productfile')->__("Edit File '%s'", $this->htmlEscape(Mage::registry('productfile_data')->getTitle()));
        } else {
            return Mage::helper('productfile')->__('Add File');
        }
    }
}