<?php
class Magestore_Productfile_Block_Adminhtml_Productfile extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_productfile';
    $this->_blockGroup = 'productfile';
    $this->_headerText = Mage::helper('productfile')->__('File Manager');
    $this->_addButtonLabel = Mage::helper('productfile')->__('Add File');
    parent::__construct();
  }
  
  
}