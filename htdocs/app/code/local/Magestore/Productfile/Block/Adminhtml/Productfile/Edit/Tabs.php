<?php

class Magestore_Productfile_Block_Adminhtml_Productfile_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('productfile_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('productfile')->__('File Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('productfile')->__('File Information'),
          'title'     => Mage::helper('productfile')->__('File Information'),
          'content'   => $this->getLayout()->createBlock('productfile/adminhtml_productfile_edit_tab_form')->toHtml(),
      ));
	  
	  $this->addTab('product_section',array(
		  'label'	  => Mage::helper('productfile')->__('Related Products'),
		  'url'		  => $this->getUrl('*/*/product',array('_current'=>true,'id'=>$this->getRequest()->getParam('id'))),
		  'class'     => 'ajax',
	  ));
     
      return parent::_beforeToHtml();
  }
}