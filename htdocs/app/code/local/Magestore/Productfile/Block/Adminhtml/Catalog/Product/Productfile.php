<?php
class Magestore_Productfile_Block_Adminhtml_Catalog_Product_Productfile 
extends Mage_Adminhtml_Block_Widget_Grid
implements Mage_Adminhtml_Block_Widget_Tab_Interface 
{
	public function getTabLabel()	{
		return Mage::helper('productfile')->__('Attached Files');
	}

	public function getTabTitle() {
		return Mage::helper('productfile')->__('Attached Files');
	}
	
	public function canShowTab()	{
		
			return true;
		
	}
	
	public function isHidden()	{
		
			return false;
	}	
	
	public function getTabClass()
	{
		return 'ajax notloaded';
	}
	
	public function getTabUrl()
	{
		return $this->getUrl('productfile/adminhtml_productfile/productfile', array('_current'=>true));
	}
}