<?php
class Magestore_Productfile_IndexController extends Mage_Core_Controller_Front_Action
{
    public function viewAction()
    {
    	if(!Mage::helper('magenotification')->checkLicenseKeyFrontController($this)){return;}			
		$this->loadLayout();     
		$this->renderLayout();
    }
}