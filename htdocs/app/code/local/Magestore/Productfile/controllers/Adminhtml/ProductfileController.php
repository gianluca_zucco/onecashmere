<?php

class Magestore_Productfile_Adminhtml_ProductfileController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('productfile/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		if(!Mage::helper('magenotification')->checkLicenseKeyAdminController($this)){return;}
		$this->_initAction()
			->renderLayout();
	}
	
	public function productAction()
	{
		$this->loadLayout();
		$this->getLayout()->getBlock('productfile.edit.tab.product')
			->setProducts($this->getRequest()->getPost('fproducts',null));
		$this->renderLayout();
	
	}
	
	public function productGridAction()
	{
		$this->loadLayout();
		$this->getLayout()->getBlock('productfile.edit.tab.product')
			->setProducts($this->getRequest()->getPost('productfile',null));
		$this->renderLayout();
	
	}
	
	public function productfileAction()
	{
		//Mage::getSingleton('core/session')->setData('filter',true);
		$this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.grid')
            ->setProductfiles($this->getRequest()->getPost('productfile', null));
        $this->renderLayout();
	
	}
	
	public function productfileGridAction()
	{
		//Mage::getSingleton('core/session')->setData('filter',false);
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.grid')
            ->setProductfiles($this->getRequest()->getPost('productfile', null));
        $this->renderLayout();
	
	}
	
	//tab in product
	public function fileAction() {
		$this->loadLayout();
		$this->_setActiveMenu('productfile/items');

		$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

		$this->_addContent($this->getLayout()->createBlock('productfile/adminhtml_file')->setTemplate('productfile/catalog/product/uploader.phtml'));

		$this->renderLayout();
	}

		
	public function editAction() {
		if(!Mage::helper('magenotification')->checkLicenseKeyAdminController($this)){return;}
		$id     = $this->getRequest()->getParam('id');
		$store     = $this->getRequest()->getParam('store');
		$model  = Mage::getModel('productfile/productfile')->setStoreId($store)->load($id);
		    	
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('productfile_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('productfile/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('productfile/adminhtml_productfile_edit'))
				->_addLeft($this->getLayout()->createBlock('productfile/adminhtml_productfile_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('productfile')->__('File does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	
 
	public function newAction() {
		$this->editAction();
	}
 
	public function saveAction() {		
		$store = $this->getRequest()->getParam('store');		
		if ($data = $this->getRequest()->getPost()) {	
			//Zend_debug::dump($data);die();
			if (isset($data['file_status']) && $data['file_status'] != NULL){
				$data['status'] = $data['file_status'];
			}					
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		//$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','doc','docx','rar','zip','mp3','txt','pdf','wmv','flv','mp4','avi','xlsx','xls'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					//$uploader->setFilesDispersion(false);
					$uploader->setFilesDispersion(true);
					//$path = Mage::getBasedDir('media') . DS .'productfile'. DS . $this->getRequest()->getParam('id') . DS ;
					$path = Mage::getBaseDir('media') . DS .'productfile' ;
					$uploader->save($path, $_FILES['filename']['name'] );	
					$result = $uploader->save($path, $_FILES['filename']['name'] );	
					
					
				} catch (Exception $e) {
		      
		        }
				
				//$data['filepath'] = $result['path'];
				$data['filename'] = $_FILES['filename']['name'];
				$data['filesize'] = $_FILES['filename']['size'];
				$data['fileextension'] = $_FILES['filename']['type'];
				//var_dump($data['filepath']);die();
				
			
			}
			//get productfileId
			//$fileIds = array();		
				
			//get productId	
			if(isset($data['productfile_products']))
			{
				$productIds = array();
				parse_str($data['productfile_products'],$productIds);
				$productIds = array_keys($productIds);
			}
			else {
				$productIds = array(0);
			}
	  		//pid for tab in manage products	
			$pid     = $this->getRequest()->getParam('pid');
			if($pid )
			{
				$productIds = array($pid);
			}					
			
			$model = Mage::getModel('productfile/productfile');					
			$model->setData($data)
					->setStoreId($store)				
					->setId($this->getRequest()->getParam('id'));						
			try {
				
				$model->save();	
					
				Mage::helper('productfile')->assignProducts($model,$productIds);
								
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('productfile')->__('File was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId(),'store'=>$store));
					return;
				}
				
				//uploader
				if($pid){
					$this->_redirect('*/*/file',array('pid'=>$pid,'id'=>$model->getId(), 'store'=>$store));
					return;
				}
				
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
			
			
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('productfile')->__('Unable to find file to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('productfile/productfile');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('File was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}
	
	public function deleteAttachedFileAction() {
		if( $this->getRequest()->getParam('id') > 0 && $this->getRequest()->getParam('pid') > 0) {
			try {
				$model = Mage::getModel('productfile/productfile');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				$this->loadLayout();
				$this->_addContent($this->getLayout()->createBlock('core/template')
					 ->setTemplate('productfile/catalog/product/deleteAttachedFile.phtml'));
				$this->renderLayout();
				

			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/file', array('id' => $this->getRequest()->getParam('id')));
			}
		}
	
	}
	

    public function massDeleteAction() {
        $productfileIds = $this->getRequest()->getParam('productfile');
        if(!is_array($productfileIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select file(s)'));
        } else {
            try {
                foreach ($productfileIds as $productfileId) {
                    $productfile = Mage::getModel('productfile/productfile')->load($productfileId);
                    $productfile->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d file(s) were successfully deleted', count($productfileIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
		$storeId = $this->getRequest()->getParam('store');
		//Zend_debug::dump($this->getRequest()->getParam('store'));die();
        $productfileIds = $this->getRequest()->getParam('productfile');
        if(!is_array($productfileIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select file(s)'));
        } else {
			if (!$storeId || $storeId == 0){
				try {
					foreach ($productfileIds as $productfileId) {
						$productfile = Mage::getSingleton('productfile/productfile')
							->load($productfileId)
							->setStatus($this->getRequest()->getParam('status'))
							->setIsMassupdate(true)
							->save();
					}
					$this->_getSession()->addSuccess(
						$this->__('Total of %d file(s) were successfully updated', count($productfileIds))
					);
				} catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
				}			
			  $this->_redirect('*/*/index/');
			}
			else{
				foreach ($productfileIds as $productfileId) {
					$valueCollection = Mage::getModel('productfile/value')->getCollection()
								->addFieldToFilter('productfile_id', $productfileId)
								->addFieldToFilter('store_id', $storeId)
								->addFieldToFilter('attribute_code', 'status');
					$item = $valueCollection->getFirstItem();
					$item->setProductfileId($productfileId);
					$item->setStoreId($storeId);
					$item->setAttributeCode('status');
					$item->setValue($this->getRequest()->getParam('status'));
					$item->save();					
				}
				$this->_redirect('*/*/index/', array('store' => $storeId));
			}            
        }		
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'productfile.csv';
        $content    = $this->getLayout()->createBlock('productfile/adminhtml_productfile_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'productfile.xml';
        $content    = $this->getLayout()->createBlock('productfile/adminhtml_productfile_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}