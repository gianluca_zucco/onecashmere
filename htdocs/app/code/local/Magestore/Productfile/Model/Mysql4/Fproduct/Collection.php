<?php

class Magestore_Productfile_Model_Mysql4_Fproduct_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('productfile/fproduct');
    }
}