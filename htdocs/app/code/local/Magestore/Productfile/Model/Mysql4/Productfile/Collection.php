<?php

class Magestore_Productfile_Model_Mysql4_Productfile_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	protected $_store_id = null;
    protected $_addedTable = array();
	
	
    public function _construct()
    {
        parent::_construct();		
		if ($storeId = Mage::app()->getStore()->getId()) {
            $this->setStoreId($storeId);
        }
		//var_dump($storeId);die();
        $this->_init('productfile/productfile');
    }
	
	public function setStoreId($value){
		$this->_store_id = $value;
		return $this;
	}
	
	public function getStoreId(){
		return $this->_store_id;
	}
	
	protected function _afterLoad(){
    	parent::_afterLoad();		
    	if ($storeId = $this->getStoreId()) {
		//die('111111111111');
            foreach ($this->_items as $item){
                $item->setStoreId($storeId)->loadProductFileValue();
            }
        }
    	return $this;
    }
    
    public function addFieldToFilter($field, $condition=null) {
        $attributes = array(
            'title',
            //'filename',
           // 'fileextension',
            'content',           
			'status',
        );
        $storeId = $this->getStoreId();
        if (in_array($field, $attributes) && $storeId) {
            if (!in_array($field, $this->_addedTable)) {
                $this->getSelect()
                    ->joinLeft(array($field => $this->getTable('productfile/value')),
                        "main_table.productfile_id = $field.productfile_id" .
                        " AND $field.store_id = $storeId" .
                        " AND $field.attribute_code = '$field'",
                        array()
                    );
                $this->_addedTable[] = $field;
            }
            return parent::addFieldToFilter("IF($field.value IS NULL, main_table.$field, $field.value)", $condition);
        }
        if ($field == 'productfile_id') {
            $field = 'main_table.productfile_id';
        }
        return parent::addFieldToFilter($field, $condition);
    }
}