<?php

class Magestore_Productfile_Model_Mysql4_Productfile extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the productfile_id refers to the key field in your database table.
        $this->_init('productfile/productfile', 'productfile_id');
    }
}