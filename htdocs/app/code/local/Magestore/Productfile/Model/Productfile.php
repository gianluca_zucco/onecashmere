<?php

use Magestore_Productfile_Model_Mysql4_Productfile_Collection as Collection;

class Magestore_Productfile_Model_Productfile extends Mage_Core_Model_Abstract
{
    protected $_store_id = null;

    public function _construct()
    {
        parent::_construct();
        if ($storeId = Mage::app()->getStore()->getId()) {
            $this->setStoreId($storeId);
        }
        $this->_init('productfile/productfile');
    }

    public function setStoreId($value)
    {
        $this->_store_id = $value;
        return $this;
    }

    public function getStoreId()
    {
        return $this->_store_id;
    }

    public function getAttributes(): array
    {
        return ['title','content','status',];
    }

    public function load($id, $field = null)
    {
        parent::load($id, $field);
        if ($this->getStoreId()) {

            $this->loadProductFileValue();
        }
        return $this;
    }

    public function loadProductFileValue($storeId = null)
    {
        if (!$storeId) {
            $storeId = $this->getStoreId();
        }
        if (!$storeId) {
            return $this;
        }
        $storeValues = Mage::getModel('productfile/value')->getCollection()
            ->addFieldToFilter('productfile_id', $this->getId())
            ->addFieldToFilter('store_id', $storeId);
        foreach ($storeValues as $value) {
            $this->setData($value->getAttributeCode() . '_in_store', true);
            $this->setData($value->getAttributeCode(), $value->getValue());
        }
        return $this;
    }

    protected function _beforeSave()
    {
        if ($storeId = $this->getStoreId()) {
            $defaultProductFile = Mage::getModel('productfile/productfile')->load($this->getId());
            $productFileAttributes = $this->getAttributes();
            foreach ($productFileAttributes as $attribute) {

                if ($this->getData($attribute . '_default')) {
                    $this->setData($attribute . '_in_store', false);
                } else {
                    $this->setData($attribute . '_in_store', true);
                    $this->setData($attribute . '_value', $this->getData($attribute));
                }
                $this->setData($attribute, $defaultProductFile->getData($attribute));
            }
        }
        return parent::_beforeSave();
    }

    protected function _afterSave()
    {
        if ($storeId = $this->getStoreId()) {
            $defaultProductFile = $this->getAttributes();
            foreach ($defaultProductFile as $attribute) {
                $attributeValue = Mage::getModel('productfile/value')
                    ->loadAttributeValue($this->getId(), $storeId, $attribute);
                if ($this->getData($attribute . '_in_store')) {
                    try {
                        $attributeValue->setValue($this->getData($attribute . '_value'))->save();
                    } catch (Exception $e) {

                    }
                } elseif ($attributeValue && $attributeValue->getId()) {
                    try {
                        $attributeValue->delete();
                    } catch (Exception $e) {

                    }
                }
            }
        }
        return parent::_afterSave();
    }

    public function getProductIds()
    {
        $productIds = array();
        $collection = Mage::getResourceModel('productfile/fproduct_collection')->addFieldToFilter('productfile_id',
            $this->getId());
        if (count($collection)) {
            foreach ($collection as $item) {
                $productIds[] = $item->getProductId();
            }
            return $productIds;
        }
    }

    public function getProductFiles(): Collection
    {
        $collection = Mage::getResourceModel('productfile/productfile_collection')
            ->join('fproduct', 'fproduct.productfile_id = main_table.productfile_id', 'product_id');

        $collection->getSelect()->where('status = ?', 1);
        $collection->getSelect()->where('product_id = ?', Mage::registry('current_product')->getId());
        $collection->setOrder('title', 'ASC');
        return $collection;
    }

    public function getStatus(): bool
    {
        return !!$this->getData('status');
    }

    public function getTitle(): string
    {
        return $this->getData('title');
    }

    public function getFileName(): string
    {
        return $this->getData('filename');
    }

    public function getContent(): string
    {
        return $this->getData('content');
    }

    public function getExtension(): string
    {
        return $this->getData('fileextension');
    }

    public function getSize(): string
    {
        return $this->getData('filesize');
    }
}
