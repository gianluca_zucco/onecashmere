<?php

class Magestore_Productfile_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected $_productfile;	
	
	public function assignProducts($productfile,$productIds)
	{
		
		$Fproduct = Mage::getModel('productfile/fproduct');
		
		if($productIds == array(0))
			return $this;			
		
		if(!count($productIds))
			$productIds = array(0);	
	//	var_dump($productIds);die();	
		foreach($productIds as $key=>$productId)
		{
			if($productId)
			{
				$Fproduct->loadByFilePrd($productfile->getId(),$productId);
				$Fproduct->setProductfileId($productfile->getId());
				$Fproduct->setProductId($productId);
				$Fproduct->save();
				$Fproduct->setId(null);
			}else{
				unset($productIds[$key]);
			}
		}
		
		if(!count($productIds))
			$productIds = array(0);
		
		$collection = Mage::getResourceModel('productfile/fproduct_collection')
						->addFieldToFilter('product_id',array('nin'=>$productIds))
						->addFieldToFilter('productfile_id',$productfile->getId());
		
		if(count($collection))
		foreach($collection as $item)
			$item->delete();
		
		return $this;	
			
	}
	
	public function assignProductfiles($productfiles,$productId)
	{
		$productfileIds = array(0);
		$Fproduct = Mage::getModel('productfile/fproduct');
		
		if($productId)
		{
			if (count($productfiles))
			{
			foreach($productfiles as $productfile)
			{
				$Fproduct->loadByFilePrd($productfile->getId(),$productId);
				$Fproduct->setProductfileId($productfile->getId());
				$Fproduct->setProductId($productId);
				$Fproduct->save();
				$Fproduct->setId(null);
				$productfileIds[] = $productfile->getId();
			}
			}
		}
		
		$collection = Mage::getResourceModel('productfile/fproduct_collection')
						->addFieldToFilter('productfile_id',array('nin'=>$productfileIds))
						->addFieldToFilter('product_id',$productId);
		
		if(count($collection))
		foreach($collection as $item)
			$item->delete();
		
		return $this;	
			
	}
	
	public function getProductfileUrl()
	{
		$url = $this->_getUrl("productfile/index/view",array());
		return $url;
	
	}
	
	function Format_size($size) 
	{
      $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
      if ($size == 0) { return('n/a'); } else {
      return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]); }
	}
	
	//NEW
	
	public function init($productfile)
	{
		$this->_productfile = $productfile;
		return $this;	
	}
	
}